FROM openjdk:17-slim
WORKDIR /opt/app
COPY build/libs/dnd-service-*.jar app.jar
COPY prod/application.properties application.properties
ENTRYPOINT ["java","-jar","app.jar"]