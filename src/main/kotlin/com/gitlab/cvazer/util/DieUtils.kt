package com.gitlab.cvazer.util

import java.util.concurrent.ThreadLocalRandom

enum class StdDie(val die: Die) {
    D4(Die(4)),
    D6(Die(6)),
    D8(Die(8)),
    D10(Die(10)),
    D12(Die(12)),
    D20(Die(20)),
    D100(Die(100))
}

data class Die(val sides: Int) {

    constructor(src: String): this(
            src.apply { if (!"^[dD]([1-9]+[0-9]*)$".toRegex().matches(src))
                throw RuntimeException("Can't parse $src") }
                    .let { "^[dD]([1-9]+[0-9]*)$".toRegex().find(src)!!.groupValues[1].toInt() })

    fun roll() = roll(1)
    fun roll(count: Int) = mutableListOf<Int>()
            .apply { repeat(count) { add(ThreadLocalRandom.current().nextInt(1, sides+1)) } }
            .sum()
}

//    constructor(src: String): this(
//            src.map { if (src.matches("^$".toRegex())) }
//            src.matches("^[1-9]+[0-9]*[dD][1-9]+[0-9]*$".toRegex())
//    )
//
//    fun roll() = dies.map { it.key.roll(it.value) }.sum()


