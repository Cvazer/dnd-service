package com.gitlab.cvazer.util

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future

private val executorService: ExecutorService = Executors
    .newWorkStealingPool(Runtime.getRuntime().availableProcessors()*2)

fun <R> Any.async(block: () -> R): Future<R> = executorService
    .submit<R> { try { block.invoke() } catch (e: Exception) { log().error(e.message, e); throw e } }