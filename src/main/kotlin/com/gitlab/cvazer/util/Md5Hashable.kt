package com.gitlab.cvazer.util

interface Md5Hashable {
    fun md5(): String
}