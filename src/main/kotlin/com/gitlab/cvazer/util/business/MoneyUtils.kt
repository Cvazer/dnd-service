package com.gitlab.cvazer.util.business

data class Money(
        var cp: Int = 0,
        var sp: Int = 0,
        var gp: Int = 0,
        var pp: Int = 0
) {
    constructor(units: Long): this(
            pp = getValue(units)["pp"] ?: 0,
            gp = getValue(units)["gp"] ?: 0,
            sp = getValue(units)["sp"] ?: 0,
            cp = getValue(units)["cp"] ?: 0
    )

    fun getUnits(): Long = ((pp * 100_000) + (gp * 100) + (sp * 10) + cp).toLong()

    operator fun plus(increment: Money): Money { return Money(this.getUnits() + increment.getUnits()) }
    operator fun minus(increment: Money): Money { return Money(this.getUnits() - increment.getUnits()) }

    operator fun plusAssign(increment: Money) {
        val new = Money(this.getUnits() + increment.getUnits())
        this.cp = new.cp
        this.sp = new.sp
        this.gp = new.gp
        this.pp = new.pp
    }

    operator fun minusAssign(increment: Money) {
        val new = Money(this.getUnits() - increment.getUnits())
        this.cp = new.cp
        this.sp = new.sp
        this.gp = new.gp
        this.pp = new.pp
    }

    companion object {
        fun parse(src: String): Money{
            val regex = """([1-9]+[0-9]*)([PGSC])""".toRegex()
            if (!regex.containsMatchIn(src.uppercase()
                    .replace(" ", ""))) throw RuntimeException("Неверный формат [$src]")
            val sum = regex.findAll(src.uppercase()).map {
                val (count, coin) = it.destructured
                return@map when(coin) {
                    "C" -> count.toLong()
                    "S" -> count.toLong() * 10
                    "G" -> count.toLong() * 100
                    "P" -> count.toLong() * 100000
                    else -> throw RuntimeException("Invalid coin nominal")
                }
            }.sum()
            return Money(sum)
        }
    }
}

private fun getValue(units: Long): Map<String, Int> {
    val pp = units/100_000
    val gp = (units - pp * 100_000)/100
    val sp = (units - (pp * 100_000) - (gp * 100))/10
    val cp = (units - (pp * 100_000) - (gp * 100) - (sp * 10))
    return mapOf(
            Pair("pp", pp.toInt()), Pair("gp", gp.toInt()),
            Pair("sp", sp.toInt()), Pair("cp", cp.toInt())
    )
}

