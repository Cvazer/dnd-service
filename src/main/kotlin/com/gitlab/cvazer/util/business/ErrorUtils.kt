package com.gitlab.cvazer.util.business

import com.gitlab.cvazer.util.etc.EntityNotFoundException
import org.springframework.data.jpa.repository.JpaRepository

fun <T, I> JpaRepository<T,I>.getOrError(id: I): T {
    return this.findById(id!!)
            .orElseThrow { return@orElseThrow EntityNotFoundException(this::class.java.simpleName, id.toString()) }
}