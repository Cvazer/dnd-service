package com.gitlab.cvazer.util.etc

// Scrapping exceptions
class ScrapFetchingException(message: String?, cause: Throwable?)
    : ScrapProcessingException(message, cause)

class ScrapConversionException(message: String?, cause: Throwable?)
    : ScrapProcessingException(message, cause)

open class ScrapProcessingException(message: String?, cause: Throwable? = null)
    : Throwable(message, cause) {
        constructor(e: Exception): this(e.message, e)
    }

// Dao exceptions
class EntityNotFoundException(domain: String, id: String)
    : RuntimeException("В домене [$domain] нет записи с ключом [$id]")

// Service exceptions
class AccountNotFoundException(key: String)
    : RuntimeException("Нет аккаунта с ключем [$key]")