package com.gitlab.cvazer.util

import com.fasterxml.jackson.annotation.JsonFilter
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.convertValue
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.gitlab.cvazer.Application
import com.gitlab.cvazer.model.icu.ICUDateTime
import com.gitlab.cvazer.model.icu.ICUDateTimeJacksonAdapter
import com.gitlab.cvazer.web.comms.ErrorInfo
import com.gitlab.cvazer.web.comms.ObjectServiceResponse
import com.gitlab.cvazer.web.comms.ServiceResponse
import org.hibernate.Hibernate
import org.hibernate.proxy.HibernateProxy
import org.slf4j.Logger
import org.slf4j.LoggerFactory.getLogger
import java.lang.RuntimeException
import java.util.*
import javax.validation.ConstraintViolation
import javax.validation.Validation
import kotlin.reflect.KMutableProperty
import kotlin.reflect.KProperty


inline fun <reified T> Any.getAs(): T {
    return Lang.mapper.convertValue(this)
}

fun <T> T.validate(): MutableSet<ConstraintViolation<T>> {
    return Validation.buildDefaultValidatorFactory().validator.validate(this)
}

fun <T> T.isValid(): Boolean {
    return this.validate().isEmpty()
}

fun <T> T.checkValid() {
    this.validate().takeIf { it.isNotEmpty() }?.apply { throw RuntimeException(this.first().message) }
}


inline fun <reified T> String.readAs(): T {
    return Lang.mapper.readValue(this)
}

inline fun <reified T> Any.nullify(field: String): T {
    val map = this.getAs<MutableMap<String, Any?>>()
    map[field] = null
    return this.getAs()
}

fun Any.dtoMap(): MutableMap<String, Any?> = this.getAs()
fun MutableMap<String, Any?>.cut(vararg fields: String): MutableMap<String, Any?> {
    return this.apply { fields.forEach { this.remove(it) } }
}

fun MutableMap<String, Any?>.cutAllExcept(vararg fields: String): MutableMap<String, Any?> {
    return mutableMapOf<String, Any?>().apply { fields.forEach { this[it] = this@cutAllExcept[it] } }
}

fun MutableMap<String, Any?>.paste(vararg pairs: Pair<String, Any?>): MutableMap<String, Any?> {
    return this.apply { pairs.forEach { this[it.first] = it.second } }
}

@Suppress("UNCHECKED_CAST")
fun <T> MutableMap<String, Any?>.transform(field: String, block: (value: T) -> Any?): MutableMap<String, Any?> {
    return this.apply { this[field] = block(this[field] as T) }
}

@Suppress("UNCHECKED_CAST")
fun <T> MutableMap<String, Any?>.transformIfNotNull(field: String, block: (value: T) -> Any?): MutableMap<String, Any?> {
    return this.apply { this[field] = this[field]?.let { block(it as T) } }
}

fun <T> String.readAs(clazz: Class<T>): T {
    return Lang.mapper.readValue(this, clazz)
}

inline fun <reified T> T.jsonString(): String {
    return Lang.mapper.writeValueAsString(Hibernate.unproxy(this))
}

inline fun <reified T> Any.getBean(): T {
    return Application.context.getBean(T::class.java)
}

fun Any.getBean(name: String): Any {
    return Application.context.getBean(name)
}

fun Any.serviceResponse(): ServiceResponse<*> {
    if (this is ErrorInfo) return ObjectServiceResponse(this, null)
    if (this is ServiceResponse<*>) return this
    return ObjectServiceResponse(ErrorInfo.ok(), this.unwrap())
}

inline fun <reified T> T.log(): Logger = getLogger(T::class.java)

inline fun <reified T> T.unwrap(): T {
    Hibernate.initialize(this)
    T::class.members.filterIsInstance<KProperty<*>>().forEach { prop ->
        val value = prop.getter.call(this)
        if (value !is HibernateProxy) return@forEach
        if (prop !is KMutableProperty<*>) return@forEach
        prop.setter.call(this, Hibernate.unproxy(value))
    }
    return Hibernate.unproxy(this, T::class.java)
}

object Lang {
    val mapper: ObjectMapper = ObjectMapper()
        .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
        .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        .setSerializationInclusion(JsonInclude.Include.NON_NULL)
        .registerModule(SimpleModule().apply {
            this.addSerializer(ICUDateTime::class.java, ICUDateTimeJacksonAdapter.ICUDateTimeJacksonSerializer())
            this.addDeserializer(ICUDateTime::class.java, ICUDateTimeJacksonAdapter.ICUDateTimeJacksonDeserializer())
        })
        .registerModule(JavaTimeModule())
        .registerModule(KotlinModule.Builder().build())
        .addMixIn(Any::class.java, DynamicMixIn::class.java)
        .registerKotlinModule()
        .setFilterProvider(SimpleFilterProvider()
            .addFilter("dynamicFilter",
                SimpleBeanPropertyFilter
                    .serializeAllExcept("hibernateLazyInitializer", "handler")))

    @JsonFilter("dynamicFilter")
    class DynamicMixIn
}

fun <R> Any.doIf(block: () -> R, predicate: () -> Boolean) {
    if (predicate.invoke()) block.invoke()
}

fun String.capitalize(): String = this.replaceFirstChar {
    if (it.isLowerCase()) it.titlecase(Locale.getDefault())
    else it.toString()
}