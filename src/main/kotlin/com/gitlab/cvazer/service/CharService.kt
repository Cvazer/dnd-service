@file:Suppress("unused")

package com.gitlab.cvazer.service

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import com.gitlab.cvazer.constant.Stat
import com.gitlab.cvazer.dao.entity.CharEntity
import com.gitlab.cvazer.dao.entity.InventoryEntity
import com.gitlab.cvazer.dao.entity.dict.SkillDictEntity
import com.gitlab.cvazer.dao.entity.dict.StatDictEntity
import com.gitlab.cvazer.dao.entity.link.*
import com.gitlab.cvazer.dao.entity.link.identity.CharProfCatLinkIdentity
import com.gitlab.cvazer.dao.repo.*
import com.gitlab.cvazer.dao.repo.generic.*
import com.gitlab.cvazer.dao.repo.link.AccountCampaignLinkDao
import com.gitlab.cvazer.dao.repo.system.AccountDao
import com.gitlab.cvazer.model.character.CharNotes
import com.gitlab.cvazer.model.character.CharHitDie
import com.gitlab.cvazer.model.character.CharSkill
import com.gitlab.cvazer.model.character.CharStat
import com.gitlab.cvazer.model.character.magic.CharMagicSlotInfo
import com.gitlab.cvazer.model.view.CharCard
import com.gitlab.cvazer.model.view.CharView
import com.gitlab.cvazer.model.view.SpellView
import com.gitlab.cvazer.system.proxy.RemoteProxy
import com.gitlab.cvazer.util.business.Money
import com.gitlab.cvazer.util.business.getOrError
import com.gitlab.cvazer.util.etc.EntityNotFoundException
import com.gitlab.cvazer.util.getBean
import com.gitlab.cvazer.web.comms.ErrorInfo
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@RemoteProxy("charService")
class CharService(
        private val charDao: CharDao,
        private val statDao: StatDictDao,
        private val skillDao: SkillDictDao,
        private val accountDao: AccountDao
) {

    fun getView(charId: Long): CharView {
        val char: CharEntity = charDao.findById(charId)
                .orElseThrow { return@orElseThrow EntityNotFoundException("CharDao", charId.toString()) }

        return CharView(
                id = char.id!!,
                name = char.name,
                hpCurrent = char.hpInfo.hpCurrent,
                hpMax = char.hpInfo.hpMax,
                hpTmpCurrent = char.hpInfo.hpTmpCurrent,
                hd = char.hpInfo.hd,
                ac = char.genericInfo.ac,
                speed = char.genericInfo.speed,
                lvl = char.genericInfo.lvl,
                exp = char.genericInfo.exp,
                pp = char.moneyInfo.pp,
                gp = char.moneyInfo.gp,
                sp = char.moneyInfo.sp,
                cp = char.moneyInfo.cp,
                backgroundName = char.background.name,
                playerName = char.playerName,
                attacks = char.miscInfo.attacks,
                stats = statDao.findAll().map { getStat(char, it) },
                raceName = char.race.name+(if (char.subrace == null) "" else " (${char.subrace!!.name})"),
                initiative = char.stats.find { it.identity.statId == Stat.DEX.name }?.getMod() ?: -5,
                expNext = Lvl.findNext(char.genericInfo.lvl).exp,
                expPrev = Lvl.find(char.genericInfo.lvl)!!.exp,
                prof = char.getProfBonus(),
                classes = char.classes.map { mapOf(
                        Pair("id", it.cls.id.toString()),
                        Pair("name", it.cls.name),
                        Pair("lvl", it.level.toString())
                ) }
        )
    }

    private fun getStat(char: CharEntity, stat: StatDictEntity): CharStat {
        val cs = char.stats.find { it.identity.statId == stat.id }
        return CharStat(
                id = stat.id,
                name = stat.name,
                bonus = cs?.getMod() ?: -5,
                save = cs?.save ?: false,
                saveBonus = cs?.getSaveMod(cs.char.getProfBonus()) ?: -5,
                value = cs?.value ?: 0
        ).apply { this.skills = stat.skills!!.map { getSkill(it, char, this.bonus) } }
    }

    private fun getSkill(skill: SkillDictEntity, char: CharEntity, statBonus: Int): CharSkill {
        val csk = char.skills.find { it.identity.skillId == skill.id }
        return CharSkill(
                id = skill.id,
                name = skill.name,
                nameEng = skill.nameEng,
                bonus = statBonus + if (csk?.prof == true) char.getProfBonus() * (if (csk.comp) 2 else 1) else 0,
                prof = csk?.prof ?: false
        )
    }

    fun setStatValue(statId: String, value: Int, charId: Long): CharStat {
        val char = charDao.getOrError(charId)
        val stat = statDao.getById(statId)

        val cs = char.stats.find { it.identity.statId == stat.id }
        if (cs == null) {
            char.stats.add(CharStatLinkEntity(char = char, save = false, value = value, stat = stat))
        } else { cs.value = value }

        return getStat(charDao.save(char), stat)
    }

    fun setStatSave(statId: String, value: Boolean, charId: Long): CharStat {
        val char = charDao.getOrError(charId)
        val stat = statDao.getById(statId)

        val cs = char.stats.find { it.identity.statId == stat.id }
        if (cs == null) {
            char.stats.add(CharStatLinkEntity(char = char, save = value, value = 0, stat = stat))
        } else { cs.save = value }

        return getStat(charDao.save(char), stat)
    }

    fun setSkillProf(skillId: String, value: Boolean, charId: Long): CharStat {
        val char = charDao.getOrError(charId)
        val skill = skillDao.getById(skillId)

        val cs = char.skills.find { it.identity.skillId == skill.id }
        if (cs == null) {
            char.skills.add(CharSkillLinkEntity(char = char, skill = skill, prof = value, comp = false))
        } else { cs.prof = value }

        return getStat(charDao.save(char), skill.stat)
    }

    fun changeMoney(src: String, direction: Int, charId: Long): Money {
        val char = charDao.getOrError(charId)

        char.moneyInfo.money = if (direction >= 0)
                            char.moneyInfo.money + Money.parse(src)
                       else char.moneyInfo.money - Money.parse(src)

        return charDao.save(char).moneyInfo.money
    }

    fun addExp(exp: Long, charId: Long): Map<String, Long> {
        var char = charDao.getOrError(charId)

        char.genericInfo.exp += exp
        val lvl = Lvl.values()
            .filter { it.exp <= char.genericInfo.exp }
            .maxByOrNull { it.exp }
        char.genericInfo.lvl = lvl!!.lvl

        char = charDao.save(char)

        return mapOf(
                Pair("next", Lvl.findNext(lvl.lvl).exp),
                Pair("prev", Lvl.find(lvl.lvl)!!.exp),
                Pair("exp", char.genericInfo.exp)
        )
    }

    fun getAllCard() = charDao.findAll().map { getCard(it) }
    fun getCard(charId: Long) = getCard(charDao.getOrError(charId))
    fun getCard(char: CharEntity) = CharCard(
                id = char.id!!,
                name = char.name,
                playerName = char.playerName,
                classes = char.classes.map { mapOf(
                        Pair("id", it.identity.clsId!!.toString()),
                        Pair("name", it.cls.name),
                        Pair("lvl", it.level.toString())
                ) }
        )

    fun getGroups(key: String) = accountDao.getOrError(key).chars.asSequence()
        .map { it.tab }
        .toSet()

    fun getCardsForGroup(key: String, group: String) = accountDao.getOrError(key).chars.asSequence()
        .filter { it.tab == group }
        .map { getCard(it.char) }
        .toList()

    @Transactional
    fun create(rq: NewCharRq): Long {
        val account = accountDao.getOrError(rq.key)

        var char = CharEntity(
            name = rq.name,
            playerName = account.name,
            race = getBean<RaceDao>().getById(rq.race),
            subrace = rq.subRace?.let { getBean<RaceDao>().getById(rq.subRace) },
            background = getBean<BackgroundDao>().getById(rq.bg)
        )

        char.classes.add(CharClsLinkEntity(
            cls = getBean<ClsDao>().getById(rq.cls),
            subcls = rq.subCls?.let { getBean<ClsDao>().getById(rq.subCls) },
            char = char,
            level = rq.lvl))

        getBean<SkillDictDao>().findAll().forEach { skill -> char.skills.add(CharSkillLinkEntity(
            prof = rq.skills.find { it.contentEquals(skill.id) }?.let { true } ?: false,
            char = char,
            skill = skill
        ))}

        val weaponCat = getBean<ProfCatDictDao>().getById("WEAPON")
        val langCat = getBean<ProfCatDictDao>().getById("LANGUAGE")
        val armorCat = getBean<ProfCatDictDao>().getById("ARMOR")

        val baseWeaponDictDao: BaseWeaponDictDao = getBean()
        rq.weapons.map { baseWeaponDictDao.getById(it) }.onEach { char.profs.add(CharProfLinkEntity(
            identity = CharProfCatLinkIdentity(value = it.id),
            profCat = weaponCat,
            char = char
        )) }

        val langDictDao: LangDictDao = getBean()
        rq.langs.map { langDictDao.getById(it) }.onEach { char.profs.add(CharProfLinkEntity(
            identity = CharProfCatLinkIdentity(value = it.id),
            profCat = langCat,
            char = char
        )) }

        val baseArmorDictDao: BaseArmorDictDao = getBean()
        rq.armor.map { baseArmorDictDao.getById(it) }.onEach { char.profs.add(CharProfLinkEntity(
            identity = CharProfCatLinkIdentity(value = it.id),
            profCat = armorCat,
            char = char
        )) }

        val profCatDictDao: ProfCatDictDao = getBean()
        rq.tools.forEach { tool -> char.profs.add(CharProfLinkEntity(
                identity = CharProfCatLinkIdentity(value = tool.id),
                profCat = profCatDictDao.getById(tool.cat),
                char = char
        )) }

        val statDictDao: StatDictDao = getBean()
        rq.stats.map { st -> char.stats.add(CharStatLinkEntity(
                stat = statDictDao.getById(st.id),
                char = char,
                value = st.value,
                save = char.classes
                    .flatMap { it.cls.saves }
                    .map { it.id }
                    .contains(st.id)
        )) }

        char.inventories.add(InventoryEntity("Персонаж", "bag"))

        char.genericInfo.exp = Lvl.find(rq.lvl)?.exp ?: 0
        char.genericInfo.initiative = char.stats.find { it.stat.id.contentEquals("DEX") }?.getMod() ?: 0
        char.genericInfo.lvl = rq.lvl
        char.genericInfo.profBonus = Lvl.find(rq.lvl)?.prof ?: 2

        char.hpInfo.hd = char.classes.map { CharHitDie(it.cls.hd, it.level, it.level) }
        char.hpInfo.hpCurrent = rq.hpMax
        char.hpInfo.hpMax = rq.hpMax

        char = getBean<CharDao>().save(char)
        account.chars.add(AccountCharLinkEntity(char = char, account = account))
        getBean<AccountDao>().save(account)
        return char.id!!
    }

    @Transactional
    fun delete(id: Long): ErrorInfo {
        val accountCampaignLinkDao: AccountCampaignLinkDao = getBean()
        val campaignDao: CampaignDao = getBean()
        accountDao.saveAll(accountDao.findByCharId(id).onEach { it.chars.removeIf { c -> c.identity.charId == id } })
        accountDao.flush()
        campaignDao.saveAll(campaignDao.getCampaignsByCharId(id).onEach { it.chars.removeIf {c -> c.id == id} })
        campaignDao.flush()
        accountCampaignLinkDao.saveAll(accountCampaignLinkDao.getByActiveChar_Id(id).onEach { it.activeChar = null })
        accountCampaignLinkDao.flush()
        charDao.deleteById(id)
        return ErrorInfo.ok()
    }

    @Transactional
    fun setNotes(rq: CharNotes, charId: Long): ErrorInfo {
        val char = charDao.getOrError(charId)
        char.miscInfo.notes = rq
        charDao.save(char)
        return ErrorInfo.ok()
    }

    fun getNotes(charId: Long): CharNotes {
        val char = charDao.getOrError(charId)
        return char.miscInfo.notes
    }

    @Transactional
    fun setMagicSlots(charId: Long, level: Int, current: Int?, total: Int?) {
        val char = charDao.getOrError(charId)
        if (current != null && total != null && char.miscInfo.magic.slots[level] == null) {
            char.miscInfo.magic = char.miscInfo.magic
                .apply { slots[level] = CharMagicSlotInfo(level, total, current) }
            return
        }
        current?.also { char.miscInfo.magic = char.miscInfo.magic.apply {
            this.slots[level]?.apply { this.current = it }
        } }
        total?.also { char.miscInfo.magic = char.miscInfo.magic.apply {
            this.slots[level]?.apply { this.total = it }
        } }
    }

    @Transactional
    fun deleteMagicSlots(charId: Long, level: Int) {
        val char = charDao.getOrError(charId)
        char.miscInfo.magic = char.miscInfo.magic
            .apply { slots.remove(level) }
    }

    @Transactional
    fun addSpell(charId: Long, view: SpellView) {
        val char = charDao.getOrError(charId)
        char.miscInfo.magic = char.miscInfo.magic
            .apply { spells.add(view) }
    }

    @Transactional
    fun removeSpell(charId: Long, spellId: Long) {
        val char = charDao.getOrError(charId)
        char.miscInfo.magic = char.miscInfo.magic
            .apply { spells.removeIf { it.id == spellId } }
    }

    @Transactional
    fun setPrepared(charId: Long, spellId: Long, value: Boolean) {
        val char = charDao.getOrError(charId)
        char.miscInfo.magic = char.miscInfo.magic
            .apply { spells.find { it.id == spellId }?.prepared = value }
    }

    @Transactional
    fun resetAllMagicSlots(charId: Long) {
        val char = charDao.getOrError(charId)
        char.miscInfo.magic = char.miscInfo.magic
            .apply { slots.values.asSequence().filterNotNull().forEach {
                it.current = it.total
            } }
    }

    data class NewCharRq @JsonCreator constructor(
        @JsonProperty("key") val key: String,
        @JsonProperty("name") val name: String,
        @JsonProperty("lvl") val lvl: Int,
        @JsonProperty("cls") val cls: Long,
        @JsonProperty("subCls") val subCls: Long?,
        @JsonProperty("race") val race: Long,
        @JsonProperty("subRace") val subRace: Long?,
        @JsonProperty("bg") val bg: Long,
        @JsonProperty("skills") val skills: List<String>,
        @JsonProperty("weapons") val weapons: List<String>,
        @JsonProperty("langs") val langs: List<String>,
        @JsonProperty("tools") val tools: List<Tool>,
        @JsonProperty("armor") val armor: List<String>,
        @JsonProperty("hpMax") val hpMax: Int,
        @JsonProperty("stats") val stats: List<Stat>
    )
    {
        data class Tool @JsonCreator constructor(
            @JsonProperty("id") val id: String,
            @JsonProperty("cat") val cat: String
        )

        data class Stat @JsonCreator constructor(
            @JsonProperty("id") val id: String,
            @JsonProperty("val") val value: Int
        )
    }

    enum class Lvl(val lvl: Int, val exp: Long, val prev: Int?, val next: Int?, val prof: Int) {
        ONE(1, 0, null, 2, 2), TOW(2, 300, 1, 3, 2), THREE(3, 900, 2, 4, 2),
        FOUR(4, 2_700, 3, 5, 2), FIVE(5, 6_500, 4, 6, 3), SIX(6, 14_000, 5, 7, 3),
        SEVEN(7, 23_000, 6, 8, 3), EIGHT(8, 34_000, 7, 9, 3), NINE(9, 48_000, 8, 10, 4),
        TEN(10, 64_000, 9, 11, 4), ELEVEN(11, 85_000, 10, 12, 4), TWELFTH(12, 100_000, 11, 13, 4),
        THIRTEEN(13, 120_000, 12, 14, 5), FOURTEEN(14, 140_000, 13, 15, 5), FIFTEEN(15, 165_000, 14, 16, 5),
        SIXTEEN(16, 195_000, 15, 17, 5), SEVENTEEN(17, 225_000, 16, 18, 6), EIGHTEEN(18, 265_000, 17, 19, 6),
        NINETEEN(19, 305_000, 18, 20, 6), TWENTY(20, 355_000, 19, null, 6);

        companion object {
            fun find(lvl: Int?) = values().find { it.lvl == lvl }
            fun findNext(lvl: Int) = values().find { it.prev == lvl } ?: find(lvl)!!
            fun findPrev(lvl: Int) = values().find { it.next == lvl } ?: find(lvl)!!
        }
    }
}