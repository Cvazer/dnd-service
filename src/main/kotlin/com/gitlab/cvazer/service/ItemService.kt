package com.gitlab.cvazer.service

import com.gitlab.cvazer.dao.entity.ItemEntity
import com.gitlab.cvazer.model.view.CharInvItem
import com.gitlab.cvazer.system.proxy.RemoteProxy
import com.gitlab.cvazer.web.comms.ErrorInfo
import com.gitlab.cvazer.web.comms.paging.PagingRs
import org.intellij.lang.annotations.Language
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import javax.persistence.EntityManager

@Service
@RemoteProxy("itemService")
class ItemService(
        private val entityManager: EntityManager,
        private val inventoryService: InventoryService
) {
    @Language("MySQL") private val expr = """
            select * from t_item where 
                upper(name) = upper(:skey) and 
                generic = 0 and 
                catalogue = 0
            union
            select * from t_item where 
                upper(name) like upper(concat(:skey, '%')) and 
                generic = 0 and 
                catalogue = 0
            union
            select * from t_item where 
                upper(name) like upper(concat('%', :skey, '%')) and 
                generic = 0 and 
                catalogue = 0
            union
            select * from t_item where 
                upper(name) like upper(concat('%', :skey)) and 
                generic = 0 and 
                catalogue = 0
        """.trimIndent()
    @Language("MySQL") private val exprAll = """
            select * from t_item where upper(name) = upper(:skey) union
            select * from t_item where upper(name) like upper(concat(:skey, '%')) union
            select * from t_item where upper(name) like upper(concat('%', :skey, '%')) union
            select * from t_item where upper(name) like upper(concat('%', :skey))
        """.trimIndent()

    @Transactional
    fun findItemsByNameTrans(pageSize: Int, pageNum: Int, name: String): PagingRs<List<CharInvItem>> =
        findItemsByName(pageSize, pageNum, name)

    @Transactional
    fun findAllItemsByNameTrans(pageSize: Int, pageNum: Int, name: String): PagingRs<List<CharInvItem>> =
            findAllItemsByName(pageSize, pageNum, name)

    fun findItemsByName(pageSize: Int, pageNum: Int, name: String): PagingRs<List<CharInvItem>> {
        val resList = entityManager.createNativeQuery(expr, ItemEntity::class.java)
                .setParameter("skey", name)
                .setFirstResult(pageNum*pageSize)
                .setMaxResults(pageSize)
                .resultList
        return PagingRs.get(resList.map { inventoryService.getItemView(it as ItemEntity, 0, null, null, null) }, pageNum, pageSize)
    }

    fun findAllItemsByName(pageSize: Int, pageNum: Int, name: String): PagingRs<List<CharInvItem>> {
        val resList = entityManager.createNativeQuery(exprAll, ItemEntity::class.java)
                .setParameter("skey", name)
                .setFirstResult(pageNum*pageSize)
                .setMaxResults(pageSize)
                .resultList
        return PagingRs.get(resList.map { inventoryService.getItemView(it as ItemEntity, 0, null, null, null) }, pageNum, pageSize)
    }
}