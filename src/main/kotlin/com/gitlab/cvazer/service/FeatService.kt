package com.gitlab.cvazer.service

import com.gitlab.cvazer.dao.entity.FeatEntity
import com.gitlab.cvazer.dao.repo.CharDao
import com.gitlab.cvazer.dao.repo.FeatDao
import com.gitlab.cvazer.system.proxy.RemoteProxy
import com.gitlab.cvazer.util.business.getOrError
import com.gitlab.cvazer.web.comms.ErrorInfo
import org.springframework.stereotype.Service

@Service
@RemoteProxy("featService")
class FeatService(private val charDao: CharDao, private val featDao: FeatDao) {

    fun addFeature(charId: Long, featId: Long): ErrorInfo {
        val char = charDao.getOrError(charId)
        val feat = featDao.getOrError(featId)

        char.feats.add(feat)
        charDao.save(char)
        return ErrorInfo.ok()
    }

    fun list(): List<FeatEntity> = featDao.findAll()

}