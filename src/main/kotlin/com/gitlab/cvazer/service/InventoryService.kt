package com.gitlab.cvazer.service

import com.gitlab.cvazer.dao.entity.CharEntity
import com.gitlab.cvazer.dao.entity.InventoryEntity
import com.gitlab.cvazer.dao.entity.ItemEntity
import com.gitlab.cvazer.dao.entity.link.InvItemLinkEntity
import com.gitlab.cvazer.dao.repo.CharDao
import com.gitlab.cvazer.dao.repo.InventoryDao
import com.gitlab.cvazer.dao.repo.ItemDao
import com.gitlab.cvazer.dao.repo.link.InvItemLinkDao
import com.gitlab.cvazer.model.view.CharInv
import com.gitlab.cvazer.model.view.CharInvItem
import com.gitlab.cvazer.model.view.InvView
import com.gitlab.cvazer.system.proxy.RemoteProxy
import com.gitlab.cvazer.util.business.getOrError
import com.gitlab.cvazer.web.comms.ErrorInfo
import org.springframework.stereotype.Service
import java.util.*

@Service
@RemoteProxy("inventoryService")
class InventoryService(
        private val charDao: CharDao,
        private val itemDao: ItemDao,
        private val invDao: InventoryDao,
        private val invItemLinkDao: InvItemLinkDao
) {

    fun getForChar(charId: Long): InvView {
        val char: CharEntity = charDao.getOrError(charId)
        return InvView(inventories = char.inventories.map { inv -> CharInv(
                id = inv.id!!,
                name = inv.name,
                icon = inv.icon,
                items = inv.items.map { getItemView(it) }
        ) })
    }

    fun deleteItem(id: Long, invId: Long) {
        val inv: InventoryEntity = invDao.getOrError(invId)
        inv.items.removeIf { it.id == id }
        invDao.save(inv)
    }

    fun setItem(id: Long, count: Int, com: String?, alias: String?) {
        val link: InvItemLinkEntity = invItemLinkDao.getOrError(id)
        link.count = count
        com?.let { link.comment = String(Base64.getDecoder().decode(com)) }
        link.alias = alias
        invItemLinkDao.save(link)
    }

    //Not used right now
    fun addItem(itemId: Long, invId: Long, count: Int, com: String?, alias: String?) {
        val inv: InventoryEntity = invDao.getOrError(invId)
        val item: ItemEntity = itemDao.getOrError(itemId)

        inv.items.add(InvItemLinkEntity(
                inventory = inv,
                item = item,
                count = count,
                comment = com?.let { String(Base64.getDecoder().decode(com)) },
                alias = alias
        ))
        invDao.save(inv)
    }

    fun moveItem(id: Long, targetInvId: Long): ErrorInfo {
        val link: InvItemLinkEntity = invItemLinkDao.getOrError(id)
        val inv: InventoryEntity = invDao.getOrError(targetInvId)

        link.inventory = inv
        invItemLinkDao.save(link)

        return ErrorInfo.ok()
    }

    fun getItemView(itemId: Long) = getItemView(itemDao.getOrError(itemId), 1, null, null, null)
    fun getItemView(link: InvItemLinkEntity) = getItemView(link.item, link.count, link.comment, link.alias, link.id)
        .apply { this.invId = link.inventory.id }
    fun getItemView(item: ItemEntity,
                    count: Int, com: String?,
                    alias: String?,
                    id: Long?) = CharInvItem(
            id = id,
            itemId = item.id!!,
            name = alias ?: item.name,
            type = item.type.id,
            count = count,
            weight = item.weight,
            cost = item.value,
            ac = item.armorInfo.ac,
            damage = item.weaponInfo.damage,
            distance = item.weaponInfo.distance?.id,
            range = item.weaponInfo.range(),
            stealth = item.armorInfo.stealth,
            armor = item.armorInfo.armor,
            comment = com
    )

}