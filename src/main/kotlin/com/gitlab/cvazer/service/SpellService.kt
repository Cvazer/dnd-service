package com.gitlab.cvazer.service

import com.gitlab.cvazer.dao.entity.SpellEntity
import com.gitlab.cvazer.dao.repo.SpellDao
import com.gitlab.cvazer.model.view.SpellView
import com.gitlab.cvazer.system.proxy.RemoteProxy
import com.gitlab.cvazer.util.business.getOrError
import com.gitlab.cvazer.web.comms.paging.PagingRs
import org.intellij.lang.annotations.Language
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import javax.persistence.EntityManager

@Service
@RemoteProxy("spellService")
class SpellService(
    private val spellDao: SpellDao,
    private val entityManager: EntityManager
) {
    @Language("MySQL") private val exprAll = """
            select * from t_spell where upper(name) = upper(:skey) union
            select * from t_spell where upper(name) like upper(concat(:skey, '%')) union
            select * from t_spell where upper(name) like upper(concat('%', :skey, '%')) union
            select * from t_spell where upper(name) like upper(concat('%', :skey))
        """.trimIndent()

    fun list(page: Int, size: Int): PagingRs<List<SpellEntity>> {
        return PagingRs.get(spellDao.findAll(PageRequest.of(page, size)).toList(), page, size)
    }

    fun findPage(query: String, size: Int, page: Int): PagingRs<List<SpellView>> {
        val resList = entityManager.createNativeQuery(exprAll, SpellEntity::class.java)
            .setParameter("skey", query)
            .setFirstResult(page*size)
            .setMaxResults(size)
            .resultList
        return PagingRs.get(resList
            .map { view(it as SpellEntity) }, page, size)
    }

    fun view(id: Long): SpellView = view(spellDao.getOrError(id))
    fun view(spell: SpellEntity): SpellView = SpellView(
        id = spell.id!!,
        name = spell.name,
        level = spell.level ?: 0,
        verbal = spell.castingInfo.verbal,
        somatic = spell.castingInfo.somatic,
        material = spell.castingInfo.material,
        ritual = spell.castingInfo.ritual,
        castingTime = spell.castingInfo.timeUnit!!.name,
        range = spell.rangeInfo.distance?.name
    )
}