package com.gitlab.cvazer.service.admin

import com.gitlab.cvazer.dao.repo.system.AccountDao
import com.gitlab.cvazer.system.proxy.RemoteProxy
import org.springframework.messaging.simp.user.SimpUserRegistry
import org.springframework.stereotype.Service

@Service
@RemoteProxy("userService")
class UserService(private val registry: SimpUserRegistry, private val accountDao: AccountDao) {

    fun getOnlineUsers() = registry.users.map { accountDao.findByKey(it.name)?.apply { this.chars = mutableListOf() } }

}