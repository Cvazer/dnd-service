package com.gitlab.cvazer.service

import com.gitlab.cvazer.dao.entity.GameEventLogEntity
import com.gitlab.cvazer.dao.repo.CampaignDao
import com.gitlab.cvazer.dao.repo.CharDao
import com.gitlab.cvazer.dao.repo.GameEventLogDao
import com.gitlab.cvazer.dao.repo.generic.GameEventTypeDictDao
import com.gitlab.cvazer.dao.repo.system.AccountDao
import com.gitlab.cvazer.system.proxy.RemoteProxy
import com.gitlab.cvazer.util.business.getOrError
import com.gitlab.cvazer.util.dtoMap
import com.gitlab.cvazer.util.paste
import com.gitlab.cvazer.web.comms.paging.PagingRs
import org.intellij.lang.annotations.Language
import org.springframework.data.domain.PageRequest
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.util.stream.Stream
import javax.persistence.EntityManager

@Service
@RemoteProxy("gameEventService")
class GameEventService(
        private val gameEventLogDao: GameEventLogDao,
        private val gameEventTypeDictDao: GameEventTypeDictDao,
        private val charDao: CharDao,
        private val accountDao: AccountDao,
        private val campaignDao: CampaignDao,
        private val entityManager: EntityManager,
        private val simpleMessagingTemplate: SimpMessagingTemplate
    ) {

    @Language("MySQL")
    private val recordsExpr = """
        select * from t_game_event_log 
        where campaign_id = :id 
        order by game_date desc, id desc
    """
    
    fun logCharAction(charId: Long, campaignId: Long, text: String, key: String) {
        val char = charDao.getOrError(charId)
        val campaign = campaignDao.getOrError(campaignId)
        val account = accountDao.getOrError(key)

        val res = gameEventLogDao.save(GameEventLogEntity(
            timestamp = LocalDateTime.now(),
            gameDate = campaign.currentDateTime,
            account = account,
            campaign = campaign,
            char = char,
            type = gameEventTypeDictDao.getOrError("CHAR_ACTION"),
            textData = text
        ))

        simpleMessagingTemplate.convertAndSend("/topic/log/${campaign.id}", enrich(res))
    }

    fun logGenericEvent(campaignId: Long, text: String, key: String) {
        val campaign = campaignDao.getOrError(campaignId)
        val account = accountDao.getOrError(key)

        val res = gameEventLogDao.save(GameEventLogEntity(
            timestamp = LocalDateTime.now(),
            gameDate = campaign.currentDateTime,
            account = account,
            campaign = campaign,
            type = gameEventTypeDictDao.getOrError("GENERIC_EVENT"),
            textData = text
        ))

        simpleMessagingTemplate.convertAndSend("/topic/log/${campaign.id}", enrich(res))
    }

    fun notifyUpdate(id: Long) = gameEventLogDao.getOrError(id)
        .let { simpleMessagingTemplate.convertAndSend("/topic/log/${it.campaign!!.id}/updates", enrich(it)) }

    private fun enrich(rec: GameEventLogEntity) = rec.dtoMap().paste(
        Pair("account", rec.account?.let { mapOf(
            Pair("key", it.key),
            Pair("name", it.name)
        ) }),
        Pair("char", rec.char?.let { mapOf(
            Pair("name", it.name),
            Pair("id", it.id)
        ) })
    )

    fun getRecords(pageNum: Int, pageSize: Int, campaignId: Long): PagingRs<List<MutableMap<String, *>>> {
        return PagingRs.get(entityManager.createNativeQuery(recordsExpr, GameEventLogEntity::class.java)
            .setParameter("id", campaignId)
            .setFirstResult(pageNum*pageSize)
            .setMaxResults(pageSize)
            .resultList
            .map { enrich(it as GameEventLogEntity) }, pageNum, pageSize)
    }
    
}