package com.gitlab.cvazer.service

import com.gitlab.cvazer.dao.entity.link.CharProfLinkEntity
import com.gitlab.cvazer.dao.entity.link.identity.CharProfCatLinkIdentity
import com.gitlab.cvazer.dao.repo.CharDao
import com.gitlab.cvazer.system.profcat.ProfCatRegistry
import com.gitlab.cvazer.system.proxy.RemoteProxy
import com.gitlab.cvazer.util.business.getOrError
import com.gitlab.cvazer.web.comms.ErrorInfo
import org.springframework.stereotype.Service

@Service
@RemoteProxy("profsService")
class ProfsService(private val charDao: CharDao, private val reg: ProfCatRegistry) {

    fun list(charId: Long): List<Map<*,*>> {
        val char = charDao.getOrError(charId)
        return char.profs.map { mapOf(
            Pair("value", reg[it.profCat.id]!!.get(it.identity.value!!)),
            Pair("cat", it.profCat)
        ) }
    }

    fun all(): List<Map<*,*>> = reg.registry.values
        .map { source ->
            val cat = source.cat()
            source.all<Any>().map { mapOf(Pair("value", it), Pair("cat", cat)) }
        }.flatten()

    fun addToChar(charId: Long, prof: String, catId: String): ErrorInfo {
        val char = charDao.getOrError(charId)
        val registry = reg[catId] ?: error("No prof cat registry for id [$catId]");
        val cat = registry.cat(); registry.get<Any>(prof)

        char.profs.add(CharProfLinkEntity(
            identity = CharProfCatLinkIdentity(value = prof),
            char = char,
            profCat = cat
        ))
        charDao.save(char)
        return ErrorInfo.ok()
    }

    fun deleteFromChar(charId: Long, prof: String, catId: String): ErrorInfo {
        val char = charDao.getOrError(charId)
        val registry = reg[catId] ?: error("No prof cat registry for id [$catId]");
        val cat = registry.cat(); registry.get<Any>(prof)

        char.profs.removeIf { it.identity.value == prof && it.profCat.id == cat.id }
        charDao.save(char)
        return ErrorInfo.ok()
    }
}