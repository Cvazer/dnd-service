package com.gitlab.cvazer.service

import com.gitlab.cvazer.dao.entity.CampaignEntity
import com.gitlab.cvazer.dao.entity.link.AccountCampaignLinkEntity
import com.gitlab.cvazer.dao.entity.link.AccountCharLinkEntity
import com.gitlab.cvazer.dao.repo.CampaignDao
import com.gitlab.cvazer.dao.repo.CharDao
import com.gitlab.cvazer.dao.repo.link.AccountCampaignLinkDao
import com.gitlab.cvazer.dao.repo.system.AccountDao
import com.gitlab.cvazer.model.icu.ICUDateTime
import com.gitlab.cvazer.system.proxy.RemoteProxy
import com.gitlab.cvazer.util.business.getOrError
import com.gitlab.cvazer.util.checkValid
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern

@Service
@RemoteProxy("campaignService")
class CampaignService(
    private val campaignDao: CampaignDao,
    private val charDao: CharDao,
    private val accountDao: AccountDao,
    private val accountCampaignLinkDao: AccountCampaignLinkDao
) {

    fun getAll(): Collection<Any> = campaignDao.findAll()
        .map { campaign -> CampaignSaveDto(
            id = campaign.id,
            currentDateTime = campaign.currentDateTime ?: ICUDateTime(),
            name = campaign.name,
            chars = campaign.chars.map { it.id!! }.toSet(),
            accounts = accountCampaignLinkDao.findAllByCampaign_Id(campaign.id!!)
                .map { CampaignSaveDto.CampaignSaveAccountDto(
                    key = it.account.key,
                    role = it.role,
                    activeCharId = it.activeChar?.id
                ) }
        ) }

    fun list(key: String): List<Any> {
        val list = accountCampaignLinkDao.getAllLinksForAccountCampaigns(key)
        val campaignMap = mutableMapOf<Long, MutableMap<String, Any>>()
        val playerCampaignMap = mutableMapOf<Long, MutableList<Map<String, Any>>>()

        list.forEach { link ->
            if (!campaignMap.containsKey(link.campaign.id)) {
                campaignMap[link.campaign.id!!] = mutableMapOf(
                    Pair("id", link.campaign.id!!),
                    Pair("name", link.campaign.name)
                )
            }

            playerCampaignMap.putIfAbsent(link.campaign.id!!, mutableListOf())

            val playerRecord = mutableMapOf<String, Any>()
            playerRecord["key"] = link.account.key
            playerRecord["name"] = link.account.name
            playerRecord["role"] = link.role
            link.activeChar?.let { playerRecord["charName"] = link.activeChar!!.name }
            link.activeChar?.let { playerRecord["charRace"] = link.activeChar!!.race.name }
            link.activeChar?.let { playerRecord["charLvl"] = link.activeChar!!.genericInfo.lvl }

            playerCampaignMap[link.campaign.id!!]!!.add(playerRecord)
        }

        return campaignMap.keys.map {
            val res = campaignMap[it]!!
            res["players"] = playerCampaignMap[it] ?: emptyList<Any>()
            return@map res
        }
    }

    fun getCampaignCharsForAccount(key: String, campaignId: Long): List<Map<String, Any?>> {
        val viableCharIds = campaignDao.getOrError(campaignId).chars.map { it.id }.toSet()
        return accountDao.getOrError(key).chars
            .filter { viableCharIds.contains(it.identity.charId) }
            .map { char -> mapOf(
                Pair("name", char.char.name),
                Pair("id", char.char.id),
                Pair("lvl", char.char.genericInfo.lvl),
                Pair("classes", char.char.classes
                    .map { mapOf(
                        Pair("name", it.cls.name),
                        Pair("subname", it.subcls?.name),
                        Pair("lvl", it.level)
                    ) })
            ) }
    }

    fun getAllActiveChars(campaignId: Long): List<Map<String, Any?>> {
        val campaign = campaignDao.getOrError(campaignId)
        return accountCampaignLinkDao.findAllByCampaign_Id(campaign.id!!)
            .filter { it.activeChar != null }
            .map { mapOf(
                Pair("name", it.activeChar!!.name),
                Pair("id", it.activeChar!!.id)
            ) }
    }

    fun save(dto: CampaignSaveDto) {
        dto.checkValid()

        var candidate = dto.id?.let { campaignDao.findByIdOrNull(dto.id) }
        if (candidate == null) {
            candidate = CampaignEntity(
                name = dto.name,
                currentDateTime = dto.currentDateTime,
                chars = dto.chars
                    .mapNotNull { charDao.findByIdOrNull(it) }
                    .toMutableList(),
                accounts = mutableListOf()
            )
        } else {
            candidate.name = dto.name
            candidate.currentDateTime = dto.currentDateTime
        }
        candidate = campaignDao.save(candidate)

        candidate.chars.removeIf { !dto.chars.contains(it.id) }
        dto.chars
            .filter { candidate.chars.find { link -> link.id == it } == null }
            .mapNotNull { charDao.findByIdOrNull(it) }
            .forEach { candidate.chars.add(it) }

        val links = accountCampaignLinkDao.findAllByCampaign_Id(candidate.id!!)

        accountCampaignLinkDao.deleteAll(
            links.filter { dto.accounts.find { e -> e.key == it.account.key } == null }
        )

        accountCampaignLinkDao.saveAll(
            dto.accounts
                .filter { links.find { e -> e.account.key == it.key } == null }
                .map { AccountCampaignLinkEntity(
                    role = it.role,
                    activeChar = it.activeCharId?.let { id -> charDao.findByIdOrNull(id) },
                    account = accountDao.findByKeyCaseSensitive(it.key)!!,
                    campaign = candidate
                ) }
        )

        accountCampaignLinkDao.saveAll(
            dto.accounts
                .map { Pair(it, links.find { e -> e.account.key == it.key }) }
                .filter { it.second != null }
                .map {
                    val link = it.second!!
                    link.role = it.first.role
                    link.activeChar = it.first.activeCharId
                        ?.let { id -> charDao.findByIdOrNull(id) }
                    return@map link
                }
        )
        campaignDao.save(candidate)
    }

    data class CampaignSaveDto(
        @get:NotNull val currentDateTime: ICUDateTime,
        @get:NotBlank val name: String,

        @get:NotNull val chars: Set<Long>,
        @get:NotNull val accounts: List<CampaignSaveAccountDto>,

        val id: Long?
    ) {
        data class CampaignSaveAccountDto(
            @get:NotBlank val key: String,

            @get:NotBlank
            @get:Pattern(regexp = "(^player$)|(^master$)")
            val role: String,

            val activeCharId: Long?,
        )
    }
}