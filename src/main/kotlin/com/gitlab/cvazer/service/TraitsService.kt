package com.gitlab.cvazer.service

import com.gitlab.cvazer.dao.entity.link.CharHiddenLink
import com.gitlab.cvazer.dao.entity.link.identity.CharHiddenLinkIdentity
import com.gitlab.cvazer.dao.repo.CharDao
import com.gitlab.cvazer.system.proxy.RemoteProxy
import com.gitlab.cvazer.util.business.getOrError
import com.gitlab.cvazer.model.view.TraitOrigin
import com.gitlab.cvazer.model.view.TraitView
import com.gitlab.cvazer.web.comms.ErrorInfo
import org.springframework.stereotype.Service

@Service
@RemoteProxy("traitsService")
class TraitsService(private val charDao: CharDao) {

    fun getCharTraits(charId: Long): List<TraitView> {
        val char = charDao.getOrError(charId)

        val list = mutableListOf<TraitView>()
        val hidden = char.hidden.filter { it.identity.domain == "TRAIT" }.map { it.identity.objectId }

        list.addAll(
            char.classes
                .flatMap { link -> link.cls.traits.filter { it.lvl <= link.level }.map { it.trait } }
                .map { trait -> TraitView(trait, TraitOrigin("CLS", "Класс")) }
        )
        char.classes.forEach { link ->
            link.subcls?.let { list.addAll(link.subcls!!.traits.filter { it.lvl <= link.level }.map { it.trait }
                .map { trait -> TraitView(trait, TraitOrigin("SUBCLS", "Архетип")) }
            ) }
        }

        char.subrace?.let { list.addAll(char.subrace!!.traits.map { TraitView(it, TraitOrigin("SUBRACE", "Подвид")) }) }
        list.addAll(char.race.traits.map { TraitView(it, TraitOrigin("RACE", "Раса")) })
        list.addAll(char.feats.map { TraitView(it.trait, TraitOrigin("FEAT", "Черта")) })
        list.add(TraitView(char.background.trait, TraitOrigin("BG", "Предыстория")))

        return list.onEach { it.hidden = hidden.contains(it.trait.id.toString()) }.sortedBy { it.trait.name }
    }

    fun hideTrait(charId: Long, traitId: Long): ErrorInfo {
        val char = charDao.getOrError(charId)
        char.hidden.add(CharHiddenLink(
            identity = CharHiddenLinkIdentity(char.id, traitId.toString(), "TRAIT"),
            char = char
        ))
        charDao.save(char)
        return ErrorInfo.ok()
    }

    fun showTrait(charId: Long, traitId: Long): ErrorInfo {
        val char = charDao.getOrError(charId)
        char.hidden.removeIf { it.identity.objectId == traitId.toString() }
        charDao.save(char)
        return ErrorInfo.ok()
    }

}