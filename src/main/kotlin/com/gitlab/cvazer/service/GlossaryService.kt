package com.gitlab.cvazer.service

import com.gitlab.cvazer.dao.entity.GlossaryRecordEntity
import com.gitlab.cvazer.system.proxy.RemoteProxy
import com.gitlab.cvazer.util.cut
import com.gitlab.cvazer.util.cutAllExcept
import com.gitlab.cvazer.util.dtoMap
import com.gitlab.cvazer.web.comms.paging.PagingRs
import org.intellij.lang.annotations.Language
import org.springframework.stereotype.Service
import javax.persistence.EntityManager

@Service
@RemoteProxy("glossaryService")
class GlossaryService(
    private val entityManager: EntityManager,
) {

    @Language("MySQL") private val findByNameExpr = """
        select * from t_glossary_record where upper(name) = upper(:skey) union
        select * from t_glossary_record where upper(name) like upper(concat(:skey, '%')) union
        select * from t_glossary_record where upper(name) like upper(concat('%', :skey, '%')) union
        select * from t_glossary_record where upper(name) like upper(concat('%', :skey))
    """.trimIndent()

    fun findByName(pageSize: Int, pageNum: Int, name: String) = PagingRs
        .get(entityManager.createNativeQuery(findByNameExpr, GlossaryRecordEntity::class.java)
            .setParameter("skey", name)
            .setFirstResult(pageNum*pageSize)
            .setMaxResults(pageSize)
            .resultList.map { it!!.dtoMap().cutAllExcept("name", "id") }, pageNum, pageSize)
}