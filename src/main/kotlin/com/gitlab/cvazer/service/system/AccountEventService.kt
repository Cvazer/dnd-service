package com.gitlab.cvazer.service.system

import com.gitlab.cvazer.dao.entity.CharEntity
import com.gitlab.cvazer.dao.entity.system.AccountEntity
import com.gitlab.cvazer.dao.entity.system.AccountEventEntity
import com.gitlab.cvazer.dao.entity.system.TokenEntity
import com.gitlab.cvazer.dao.repo.CharDao
import com.gitlab.cvazer.dao.repo.generic.AccountEventTypeDictDao
import com.gitlab.cvazer.dao.repo.system.AccountDao
import com.gitlab.cvazer.dao.repo.system.AccountEventDao
import com.gitlab.cvazer.model.meta.system.CharLvlUpEventMeta
import com.gitlab.cvazer.system.proxy.RemoteProxy
import com.gitlab.cvazer.util.business.getOrError
import com.gitlab.cvazer.util.dtoMap
import com.gitlab.cvazer.web.comms.paging.PagingRs
import org.springframework.data.domain.PageRequest
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@RemoteProxy("accountEventService")
class AccountEventService(
    private val accountDao: AccountDao,
    private val accountEventDao: AccountEventDao,
    private val accountEventTypeDictDao: AccountEventTypeDictDao,
    private val charDao: CharDao,
    private val simpleMessagingTemplate: SimpMessagingTemplate
) {

    fun generic(account: String, text: String? = null) = generic(accountDao.getOrError(account), text)
    @Transactional fun generic(account: AccountEntity, text: String? = null) = accountEventDao.save(AccountEventEntity(
        textData = text,
        type = accountEventTypeDictDao.getOrError("GENERIC"),
        account = account
    ))

    fun charLvlUp(account: String, charId: Long) = charLvlUp(accountDao.getOrError(account), charDao.getOrError(charId))
    fun charLvlUp(account: AccountEntity, char: CharEntity) = accountEventDao.save(AccountEventEntity(
        type = accountEventTypeDictDao.getOrError("CHAR_LVL_UP"),
        account = account,
        token = TokenEntity(),
        textData = "Персонаж \"${char.name}\" получил новый уровень!"
    ).apply { meta = CharLvlUpEventMeta(char, account, token!!).dtoMap() })

    fun getForAccount(key: String, page: Int, size: Int) = PagingRs.get(
        accountEventDao.findAllByAccount_Key(key, PageRequest.of(page, size)).toList(),
        page,
        size
    )

    fun countUnseenForAccount(key: String) = accountEventDao
        .countAllBySeenFalseAndAccount_Key(key)

    fun updateForAccount(key: String, eventId: Long) = simpleMessagingTemplate.convertAndSend(
        "/topic/accountEvents/$key",
        accountEventDao.findById(eventId).get()
    );
}