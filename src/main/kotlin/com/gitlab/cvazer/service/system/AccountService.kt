package com.gitlab.cvazer.service.system

import com.gitlab.cvazer.dao.entity.link.AccountCharLinkEntity
import com.gitlab.cvazer.dao.entity.system.AccountEntity
import com.gitlab.cvazer.dao.repo.CharDao
import com.gitlab.cvazer.dao.repo.GameEventLogDao
import com.gitlab.cvazer.dao.repo.link.AccountCampaignLinkDao
import com.gitlab.cvazer.dao.repo.system.AccountDao
import com.gitlab.cvazer.system.proxy.RemoteProxy
import com.gitlab.cvazer.util.business.getOrError
import com.gitlab.cvazer.util.checkValid
import com.gitlab.cvazer.util.etc.AccountNotFoundException
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern

@Service
@RemoteProxy("accountService")
class AccountService(
    private val accountDao: AccountDao,
    private val accountCampaignLinkDao: AccountCampaignLinkDao,
    private val gameEventLogDao: GameEventLogDao,
    private val charDao: CharDao
) {

    fun getAccount(key: String): AccountEntity {
        val account = accountDao.findByKeyCaseSensitive(key) ?: throw AccountNotFoundException(key)
        if (!account.key.contentEquals(key)) throw AccountNotFoundException(key)
        return account.apply { this.chars = mutableListOf() }
    }

    fun getAll(): List<AccountEntity> = accountDao.findAll()
        .onEach { it.chars = mutableListOf() }

    fun getCharIds(key: String): List<Long> {
        return (accountDao.findByKey(key) ?: throw AccountNotFoundException(key))
            .chars.map { it.identity.charId!! }
    }

    fun save(dto: AccountSaveDto) {
        dto.checkValid()
        var candidate = accountDao.findByKeyCaseSensitive(dto.key)

        if (candidate == null) {
            candidate = AccountEntity(dto.key, dto.name, dto.role)
        } else {
            candidate.name = dto.name
            candidate.role = dto.role
        }

        candidate.chars.removeIf { !dto.chars.contains(it.identity.charId) }
        dto.chars
            .filter { candidate.chars.find { link -> link.identity.charId == it } == null }
            .mapNotNull { charDao.findByIdOrNull(it) }
            .map { AccountCharLinkEntity(char = it, account = candidate) }
            .forEach { candidate.chars.add(it) }
        accountDao.save(candidate)
    }

    @Transactional
    fun delete(@NotBlank key: String) {
        val account = accountDao.getOrError(key)
        account.chars.clear()
        accountCampaignLinkDao.deleteAll(
            accountCampaignLinkDao.getAllByIdentity_AccountKey(account.key)
        )
        gameEventLogDao.nullAllEventReferencesForKey(account.key)
        accountDao.save(account)
        accountDao.delete(account)
    }

    data class AccountSaveDto(
        @get:NotBlank val key: String,
        @get:NotBlank val name: String,

        @get:NotBlank
        @get:Pattern(regexp = "(^admin$)|(^user$)")
        val role: String,

        val chars: Set<Long>
    )
}