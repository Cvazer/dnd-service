package com.gitlab.cvazer.web.comms

data class ErrorInfo(val code: String, val descr: String) {
    companion object {
        fun ok() = ErrorInfo("0", "OK")
        fun error(text: String) = ErrorInfo("1", text)
        fun nyi() = ErrorInfo("2", "Not yet implemented")
    }
}