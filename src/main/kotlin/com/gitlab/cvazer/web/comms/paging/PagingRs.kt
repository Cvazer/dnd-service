package com.gitlab.cvazer.web.comms.paging

import com.gitlab.cvazer.web.comms.ErrorInfo
import com.gitlab.cvazer.web.comms.ServiceResponse
import org.springframework.data.domain.Page

data class PagingRs<T>(
    override val errorInfo: ErrorInfo,
    override val response: T?,
    val meta: Meta
): ServiceResponse<T?> {

    data class Meta(
            val page: Int,
            val size: Int
    )

    companion object {
        fun <T> get(res: T?, page: Page<*>): PagingRs<T>{
            return get(res, page.number, page.numberOfElements)
        }
        fun <T> get(res: T?, page: Int, size: Int): PagingRs<T>{
            return PagingRs(
                errorInfo = ErrorInfo.ok(),
                response = res,
                meta = Meta(
                    page = page,
                    size = size
                )
            )
        }
    }

}