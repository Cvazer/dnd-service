package com.gitlab.cvazer.web.comms

data class ObjectServiceResponse(
        override val errorInfo: ErrorInfo,
        override val response: Any?
) : ServiceResponse<Any?>