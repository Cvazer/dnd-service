package com.gitlab.cvazer.web.comms

interface ServiceResponse<T> {
    val errorInfo: ErrorInfo
    val response: T
}