package com.gitlab.cvazer.web.controllers

import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.http.server.ServerHttpResponse
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@RestController
@RequestMapping(value = ["/"],
        consumes = [MediaType.ALL_VALUE],
        produces = [MediaType.ALL_VALUE]
)
class RootController {
    @GetMapping(value = ["/check"]) fun check() = "OK"
}