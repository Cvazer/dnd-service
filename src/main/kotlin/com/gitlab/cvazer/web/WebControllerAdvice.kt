package com.gitlab.cvazer.web

import com.gitlab.cvazer.util.etc.AccountNotFoundException
import com.gitlab.cvazer.util.etc.EntityNotFoundException
import com.gitlab.cvazer.util.serviceResponse
import com.gitlab.cvazer.web.comms.ErrorInfo
import com.gitlab.cvazer.web.comms.ServiceResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import java.lang.RuntimeException

@ControllerAdvice
class WebControllerAdvice {

    @ExceptionHandler(RuntimeException::class)
    fun generic(e: RuntimeException): ResponseEntity<out ServiceResponse<*>> =
            ResponseEntity(ErrorInfo.error(e.message.toString()).serviceResponse(), HttpStatus.OK).apply {
                e.printStackTrace()
            }

    @ExceptionHandler(AccountNotFoundException::class)
    fun accountNotFound(e: AccountNotFoundException): ResponseEntity<out ServiceResponse<*>> =
            ResponseEntity(ErrorInfo.error(e.message.toString()).serviceResponse(), HttpStatus.OK)

    @ExceptionHandler(EntityNotFoundException::class)
    fun entityNotFound(e: EntityNotFoundException): ResponseEntity<out ServiceResponse<*>> =
            ResponseEntity(ErrorInfo.error(e.message.toString()).serviceResponse(), HttpStatus.OK)
}