package com.gitlab.cvazer.model.meta

import com.fasterxml.jackson.annotation.JsonProperty

data class SourceEntry(
        @JsonProperty("source") var source: String,
        @JsonProperty("page") var page: Int = 0
)