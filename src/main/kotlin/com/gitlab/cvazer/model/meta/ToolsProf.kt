package com.gitlab.cvazer.model.meta

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import com.gitlab.cvazer.dao.entity.dict.ToolsDictEntity
import com.gitlab.cvazer.dao.repo.generic.ToolsDictDao
import com.gitlab.cvazer.util.getBean

data class ToolsProf @JsonCreator constructor(
        @JsonProperty("fixed") private var _fixed: List<String>? = listOf(),
        @JsonProperty("choicePrompt") var choicePrompt: String? = null,
        @JsonProperty("getChoicesCode") var getChoicesCode: String? = null,
        @JsonProperty("getIsAllowedCode") var getIsAllowedCode: String? = null,
        @JsonProperty("getCheckChoiceCode") var getCheckChoiceCode: String? = null
) {
    var fixed: List<ToolsDictEntity>?
        get() = if (_fixed == null) listOf() else getBean<ToolsDictDao>().findAllById(_fixed!!)
        set(value) {_fixed = value!!.map { it.id }}
}