package com.gitlab.cvazer.model.meta

import com.fasterxml.jackson.annotation.JsonProperty

data class ContainerByItem(
        @JsonProperty("itemId") var itemId: Long,
        @JsonProperty("count") var count: Int
)