package com.gitlab.cvazer.model.meta

import com.fasterxml.jackson.annotation.JsonProperty

data class GenericInherits (
        @JsonProperty("setValExp") var setValExp: String? = null,
        @JsonProperty("getValExp") var getValExp: String? = null,
        @JsonProperty("type") var type: String = "ENTRIES"
) {
    enum class InheritType (val value: String) {
        FIELD("FIELD"), ENTRIES("ENTRIES")
    }
}