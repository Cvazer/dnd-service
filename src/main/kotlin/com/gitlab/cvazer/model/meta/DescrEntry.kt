package com.gitlab.cvazer.model.meta

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

data class DescrEntry @JsonCreator constructor(
        @JsonProperty("type") var type: String,
        @JsonProperty("name") var name: String?,
        @JsonProperty("value") var value: String?,
        @JsonProperty("entries") var entries: MutableList<DescrEntry>?
) {

    @JsonCreator
    constructor(text: String) : this (
            type = DescEntryType.PLAIN.value,
            name = null, value = text, entries = null
    )
}

enum class DescEntryType(val value: String) {
    ENTRIES("entries"), PLAIN("plain")
}