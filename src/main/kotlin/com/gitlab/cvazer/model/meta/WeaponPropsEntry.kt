package com.gitlab.cvazer.model.meta

import com.fasterxml.jackson.annotation.JsonProperty

data class WeaponPropsEntry(
        @JsonProperty("id") var id: String,
        @JsonProperty("params") var params: MutableMap<String, String>
)