package com.gitlab.cvazer.model.meta.system

import com.gitlab.cvazer.dao.entity.CharEntity
import com.gitlab.cvazer.dao.entity.system.AccountEntity
import com.gitlab.cvazer.dao.entity.system.TokenEntity

class CharLvlUpEventMeta(
    val charId: Long,
    val accountKey: String,
    val token: String
) {
    constructor(char: CharEntity,
                account: AccountEntity,
                token: TokenEntity) : this(char.id!!, account.key, token.id)
}