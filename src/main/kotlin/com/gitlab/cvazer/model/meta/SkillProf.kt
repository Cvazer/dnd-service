package com.gitlab.cvazer.model.meta

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import com.gitlab.cvazer.dao.entity.dict.SkillDictEntity
import com.gitlab.cvazer.dao.repo.generic.SkillDictDao
import com.gitlab.cvazer.util.getBean

data class SkillProf @JsonCreator constructor(
        @JsonProperty("list") private var _list: List<String>? = null,
        @JsonProperty("count") var count: Int? = null
){
    var list: List<SkillDictEntity>?
        get() = getBean<SkillDictDao>().findAllById(_list ?: listOf())
        set(value) { _list = value?.map { it.id } }
}