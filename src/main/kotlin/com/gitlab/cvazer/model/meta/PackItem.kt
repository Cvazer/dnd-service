package com.gitlab.cvazer.model.meta

import com.fasterxml.jackson.annotation.JsonProperty

data class PackItem(
        @JsonProperty("type") var type: String,
        @JsonProperty("name") var name: String?,
        @JsonProperty("id") var id: Long?,
        @JsonProperty("count") var count: Int = 1
)