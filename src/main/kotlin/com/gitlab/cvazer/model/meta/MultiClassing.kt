package com.gitlab.cvazer.model.meta

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

data class MultiClassing @JsonCreator constructor(
        @JsonProperty("tools") var toolsProf: String? = null,
        @JsonProperty("armorProf") var armorProf: String? = null,
        @JsonProperty("weaponProf") var weaponProf: String? = null,
        @JsonProperty("requirements") var requirements: String? = null,
        @JsonProperty("skills") var skills: String? = null
)