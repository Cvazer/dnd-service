package com.gitlab.cvazer.model.view

data class CharCard(
        val id: Long,
        val name: String,
        val playerName: String,
        val classes: List<Map<String, String>>
)