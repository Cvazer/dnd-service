package com.gitlab.cvazer.model.view

data class InvView(
        val inventories: List<CharInv>
)

data class CharInv(
        val id: Long,
        val icon: String,
        val name: String,
        val items: List<CharInvItem>
)

data class CharInvItem(
        var id: Long? = null,
        val itemId: Long,
        val type: String,
        val name: String,
        var invId: Long? = null,
        val weight: Double?,
        var count: Int,
        val cost: Long?,
        val ac: Int?,
        val stealth: Boolean?,
        val armor: Boolean?,
        val distance: String?,
        val range: String?,
        val damage: String?,
        val comment: String?,
        val exists: Boolean = true
)