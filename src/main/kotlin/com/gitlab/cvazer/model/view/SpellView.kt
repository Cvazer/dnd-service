package com.gitlab.cvazer.model.view

data class SpellView(
    var id: Long,
    val name: String,
    val level: Int,
    val verbal: Boolean,
    val somatic: Boolean,
    val material: Boolean,
    val ritual: Boolean,
    val castingTime: String,
    val range: String?,
    var prepared: Boolean = false
)