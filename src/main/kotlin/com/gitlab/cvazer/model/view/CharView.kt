package com.gitlab.cvazer.model.view

import com.gitlab.cvazer.model.character.CharAttack
import com.gitlab.cvazer.model.character.CharHitDie
import com.gitlab.cvazer.model.character.CharStat

data class CharView(
    val id: Long,
    val name: String,
    val hpCurrent: Int,
    val hpMax: Int,
    val hpTmpCurrent: Int,
    val hd: List<CharHitDie>,

    val ac: Int,
    val initiative: Int,
    val speed: Int,

    val lvl: Int,
    val exp: Long,
    val expNext: Long,
    val expPrev: Long,

    val classes: List<Map<String, String>>,
    val attacks: List<CharAttack>,
    val stats: List<CharStat>,

    val raceName: String,
    val backgroundName: String,
    val playerName: String,
    val prof: Int,
    val pp: Int,
    val gp: Int,
    val sp: Int,
    val cp: Int
)