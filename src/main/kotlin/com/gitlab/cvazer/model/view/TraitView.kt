package com.gitlab.cvazer.model.view

import com.gitlab.cvazer.dao.entity.TraitEntity

data class TraitView(val trait: TraitEntity, val origin: TraitOrigin, var hidden: Boolean = false)
data class TraitOrigin(val id: String, val name: String)