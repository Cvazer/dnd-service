package com.gitlab.cvazer.model.icu

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import kotlin.math.ceil
import kotlin.math.roundToInt

data class ICUDateTime @JsonCreator constructor(
    @JsonProperty("age") val age: ICUAge = ICUAge.POWER,
    @JsonProperty("year") val year: Long = 1,
    @JsonProperty("dayOfYear") val dayOfYear: Int = 1,
    @JsonProperty("hour") val hour: Int = 0,
    @JsonProperty("minute") val minute: Int = 0,
    @JsonProperty("second") val second: Int = 0
) {

    @JsonCreator constructor(
        @JsonProperty("age") age: Int,
        @JsonProperty("year") year: Long,
        @JsonProperty("dayOfYear") dayOfYear: Int,
        @JsonProperty("hour") hour: Int,
        @JsonProperty("minute") minute: Int,
        @JsonProperty("second") second: Int
    ): this(ICUAge.getByNum(age), year, dayOfYear, hour, minute, second)

    constructor(
        age: ICUAge, year: Long, month: ICUMonth, day: Int,
        hour: Int, minute: Int, second: Int
    ): this(age, year, (month.num-1)*28+day, hour, minute, second)

    constructor(
        year: Long, dayOfYear: Int, hour: Int, minute: Int, second: Int
    ): this(
        ICUAge.POWER, year, dayOfYear, hour, minute, second
    )

    constructor(
        age: ICUAge, period: ICUPeriod
    ): this(
        age, period.year, period.dayOfYear,
        period.hour, period.minute, period.second
    )
    constructor(period: ICUPeriod): this(ICUAge.POWER, period)
    constructor(seconds: Long): this(ICUPeriod.of(seconds))

    val weekOfMonth: Int
        get() = ((ceil(dayOfYear / 7F).roundToInt()) % 4)
            .let { if (it == 0) 4 else it }

    val dayOfWeek: ICUDay
        get() = ICUDay.getByNum(dayOfYear % 7)

    val dayOfMonth: Int
        get() = (weekOfMonth - 1) * 7 + dayOfWeek.pos

    val month: ICUMonth
        get() = ICUMonth.getByNum(ceil(dayOfYear / 28F).roundToInt())

    fun add(period: ICUPeriod) = ICUDateTime(age, ICUPeriod.of(long()+period.long()))
    fun sub(period: ICUPeriod) = ICUDateTime(age, ICUPeriod.of(long()-period.long()))

    fun period() = ICUPeriod(year, dayOfYear, hour, minute, second)
    fun long() = period().long()

    fun add(seconds: Long) = add(ICUPeriod.of(seconds))
    fun add(value: Long, unit: ICUTimeUnit) = add(ICUPeriod.of(value, unit))
    fun add(src: String) = add(ICUPeriod.of(src))

    fun sub(seconds: Long) = sub(ICUPeriod.of(seconds))
    fun sub(value: Long, unit: ICUTimeUnit) = sub(ICUPeriod.of(value, unit))
    fun sub(src: String) = sub(ICUPeriod.of(src))
}