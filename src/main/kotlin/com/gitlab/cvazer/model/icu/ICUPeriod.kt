package com.gitlab.cvazer.model.icu

import com.gitlab.cvazer.model.icu.ICUTimeUnit.*

data class ICUPeriod(
    val year: Long = 0,
    val dayOfYear: Int = 1,
    val hour: Int = 0,
    val minute: Int = 0,
    val second: Int = 0
) {

    fun getString() = "$year лет $dayOfYear дней $hour часов " +
                "$minute минут $second секунд"

    fun long() = year * YEAR.factor +
            dayOfYear * DAY.factor +
            hour * HOUR.factor +
            minute * MINUTE.factor +
            second * SECOND.factor

    companion object {
        fun of(seconds: Long): ICUPeriod {
            var delta = seconds
            val years = delta / YEAR.factor; delta -= years * YEAR.factor
            val days = delta / DAY.factor; delta -= days * DAY.factor
            val hours = delta / HOUR.factor; delta -= hours * HOUR.factor
            val minutes = delta / MINUTE.factor; delta -= minutes * MINUTE.factor
            return ICUPeriod(
                years,
                days.toInt(),
                hours.toInt(),
                minutes.toInt(),
                delta.toInt())
        }

        fun parse(src: String): Long {
            val regex = """([1-9]+[0-9]*)([yMdhmsw])""".toRegex()
            if (!regex.containsMatchIn(src)) throw RuntimeException("Неверный формат [$src]")
            return regex.findAll(src).map {
                val (count, unit) = it.destructured
                return@map count.toLong() * ICUTimeUnit.findBySymbol(unit).factor
            }.sum()
        }
        fun of(src: String)= of(parse(src))

        fun of(value: Long, unit: ICUTimeUnit) = of(value * unit.factor)
    }
}