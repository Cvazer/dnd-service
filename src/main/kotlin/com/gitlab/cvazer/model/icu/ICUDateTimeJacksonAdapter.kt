package com.gitlab.cvazer.model.icu

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.*
import com.gitlab.cvazer.model.icu.ICUAge

class ICUDateTimeJacksonAdapter {

    class ICUDateTimeJacksonSerializer : JsonSerializer<ICUDateTime>() {
        override fun serialize(value: ICUDateTime?, gen: JsonGenerator?, serializers: SerializerProvider?) {
            if (value == null) { gen?.writeNull(); return }
            gen?.writeStartObject()
            gen?.writeNumberField("age", value.age.num)
            gen?.writeNumberField("year", value.year)
            gen?.writeNumberField("dayOfYear", value.dayOfYear)
            gen?.writeNumberField("hour", value.hour)
            gen?.writeNumberField("minute", value.minute)
            gen?.writeNumberField("second", value.second)
            gen?.writeEndObject()
        }
    }

    class ICUDateTimeJacksonDeserializer : JsonDeserializer<ICUDateTime?>() {
        override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): ICUDateTime? {
            if (p == null) return null;
            val node = p.codec?.readTree<JsonNode>(p) ?: return null
            return ICUDateTime(
                ICUAge.getByNum(node.get("age").intValue()),
                node.get("year").longValue(),
                node.get("dayOfYear").intValue(),
                node.get("hour").intValue(),
                node.get("minute").intValue(),
                node.get("second").intValue()
            )
        }

    }
}