package com.gitlab.cvazer.model.icu

@Suppress("UNUSED_PARAMETER", "unused")
enum class ICUMonth(val num: Int, val eng: String, ru: String) {
    JAN(1, "January", "Январь"),
    FEB(2, "February", "Февраль"),
    MAR(3, "March", "Март"),
    APR(4, "April", "Апрель"),
    MAY(5, "May", "Май"),
    JUN(6, "June", "Июнь"),
    SOL(7, "Sol", "Сол"),
    JUL(8, "Jul", "Июль"),
    AUG(9, "August", "Август"),
    SEP(10, "September", "Сентябрь"),
    OCT(11, "October", "Октябрь"),
    NOV(12, "November", "Ноябрь"),
    DEC(13, "December", "Декабрь");

    companion object {
        fun getByNum(num: Int) = values()
            .asList().find { it.num == num }!!
    }
}