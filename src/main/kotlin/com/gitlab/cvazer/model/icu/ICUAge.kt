package com.gitlab.cvazer.model.icu

enum class ICUAge(val num: Int, val alias: String) {
    POWER(0, "Силы"),
    DRAGON(1, "Драконов"),
    MAGIC(2, "Магии"),
    PROSPERITY(3, "Процветания"),
    REBIRTH(4, "Возрождения"),
    WAR(5, "Войны"),
    NEW_WORLD(6, "Нового Мира"),
    ENLIGHTENMENT(7, "Просвещения");

    companion object {
        fun getByNum(num: Int) = values()
            .asList().find { it.num == num }!!
    }
}