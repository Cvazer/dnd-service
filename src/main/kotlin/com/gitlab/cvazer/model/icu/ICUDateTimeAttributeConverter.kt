package com.gitlab.cvazer.model.icu

import javax.persistence.AttributeConverter
import javax.persistence.Converter

@Converter(autoApply = true)
class ICUDateTimeAttributeConverter : AttributeConverter<ICUDateTime?, String?> {
    private val separator = "|"

    override fun convertToDatabaseColumn(date: ICUDateTime?): String? {
        if (date == null) return null;
        return "${date.age.num}" +
                "|${date.year}" +
                "|${date.dayOfYear}" +
                "|${prependZero(date.hour)}" +
                "|${prependZero(date.minute)}" +
                "|${prependZero(date.second)}"
    }

    private fun prependZero(num: Int) = if (num>=10) ""+num else "0$num"

    override fun convertToEntityAttribute(src: String?): ICUDateTime? {
        if (src == null) return null
        val split = src.split(separator)
        return ICUDateTime(
            ICUAge.getByNum(split[0].toInt()),
            split[1].toLong(),
            split[2].toInt(),
            split[3].toInt(),
            split[4].toInt(),
            split[5].toInt()
        )
    }
}