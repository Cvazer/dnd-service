package com.gitlab.cvazer.model.icu

enum class ICUTimeUnit(val factor: Long, val symbol: String) {
    YEAR(31449600, "y"),
    MONTH(2419200, "M"),
    WEEK(604800, "w"),
    DAY(86400, "d"),
    HOUR(3600, "h"),
    MINUTE(60, "m"),
    SECOND(1, "s");

    companion object {
        fun findBySymbol(symbol: String) = values().toList()
            .find { it.symbol.contentEquals(symbol) }!!
    }
}