package com.gitlab.cvazer.model.icu

enum class ICUDay(val num: Int, val alias: String) {
    MONDAY(1, "Понедельник"),
    TUESDAY(2, "Вторник"),
    WEDNESDAY(3, "Среда"),
    THURSDAY(4, "Четверг"),
    FRIDAY(5, "Пятница"),
    SATURDAY(6, "Суббота"),
    SUNDAY(0, "Воскресенье");

    val pos: Int
        get() = if (this == SUNDAY) 7 else this.num

    companion object {
        fun getByNum(num: Int) = values()
            .asList().find { it.num == num }!!
    }
}