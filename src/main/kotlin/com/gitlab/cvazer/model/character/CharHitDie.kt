package com.gitlab.cvazer.model.character

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

data class CharHitDie @JsonCreator constructor(
        @JsonProperty("die") val die: String,
        @JsonProperty("current") val current: Int,
        @JsonProperty("max") val max: Int
)