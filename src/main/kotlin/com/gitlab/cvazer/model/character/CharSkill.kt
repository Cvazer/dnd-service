package com.gitlab.cvazer.model.character

data class CharSkill(
        val id: String,
        val name: String,
        val nameEng: String = "NO_ENG_NAME",
        val bonus: Int = -5,
        val prof: Boolean = false
)