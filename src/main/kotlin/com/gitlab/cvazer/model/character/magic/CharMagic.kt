package com.gitlab.cvazer.model.character.magic

import com.gitlab.cvazer.model.view.SpellView

data class CharMagic(
    var spells: MutableList<SpellView> = mutableListOf(),
    var slots: MutableMap<Int, CharMagicSlotInfo?> = mutableMapOf()
)