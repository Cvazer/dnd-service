package com.gitlab.cvazer.model.character

data class CharStat(
        val id: String,
        val name: String,
        val value: Int = 0,
        val bonus: Int = -5,
        val saveBonus: Int = -5,
        val save: Boolean = false,
        var skills: List<CharSkill> = listOf()
)