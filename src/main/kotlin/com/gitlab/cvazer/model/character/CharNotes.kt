package com.gitlab.cvazer.model.character

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class CharNotes @JsonCreator constructor(
    @JsonProperty("notes") val notes: MutableList<CharNote>? = mutableListOf(),
    @JsonProperty("counters") val counters: MutableList<CharCounter>? = mutableListOf()
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class CharNote @JsonCreator constructor(
    @JsonProperty("name") var name: String,
    @JsonProperty("text") var text: String,
    @JsonProperty("uuid") var uuid: String
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class CharCounter @JsonCreator constructor(
    @JsonProperty("uuid") var uuid: String,
    @JsonProperty("name") var name: String,
    @JsonProperty("max") var max: Int,
    @JsonProperty("min") var min: Int,
    @JsonProperty("step") var step: Int,
    @JsonProperty("value") var value: Int
)