package com.gitlab.cvazer.model.character.magic

data class CharMagicSlotInfo(
    var level: Int = 0,
    var total: Int = 0,
    var current: Int = 0
)