package com.gitlab.cvazer.model.character

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

data class CharAttack @JsonCreator constructor(
        @JsonProperty("id") var id: String,
        @JsonProperty("name") var name: String,
        @JsonProperty("damageBase") var damageBase: String,
        @JsonProperty("attackBonus") var attackBonus: Int,
        @JsonProperty("damageBonus") var damageBonus: Int
)