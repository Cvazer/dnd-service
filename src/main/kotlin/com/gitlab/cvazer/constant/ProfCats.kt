package com.gitlab.cvazer.constant

enum class ProfCats(val id: String) {
    ARMOR("ARMOR"), ARTISAN_TOOLS("ARTISAN_TOOLS"),
    GAMING_SET("GAMING_SET"), LANGUAGE("LANGUAGE"),
    MUSICAL_INSTRUMENT("MUSICAL_INSTRUMENT"),
    TOOLS("TOOLS"), WEAPON("WEAPON")
}