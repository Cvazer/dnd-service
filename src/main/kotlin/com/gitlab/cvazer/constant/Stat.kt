package com.gitlab.cvazer.constant

enum class Stat(val id: String) {
    STR("STR"), DEX("DEX"),
    CON("CON"), INT("INT"),
    WIS("WIS"), CHA("CHA");
}