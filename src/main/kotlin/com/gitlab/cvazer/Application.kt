package com.gitlab.cvazer

import com.gitlab.cvazer.util.Lang
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder

@SpringBootApplication(scanBasePackages = ["com.gitlab.cvazer"])
class Application constructor(context: ApplicationContext){
    init { Application.context = context }

    @Bean @Primary fun objectMapper(builder: Jackson2ObjectMapperBuilder) = Lang.mapper

    companion object {
        lateinit var context: ApplicationContext

        @JvmStatic
        fun main(args: Array<String>) {
            runApplication<Application>(*args)
        }
    }
}