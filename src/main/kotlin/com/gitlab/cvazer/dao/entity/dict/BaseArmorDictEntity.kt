package com.gitlab.cvazer.dao.entity.dict

import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "v_base_armor_dict")
data class BaseArmorDictEntity(
        override var id: String,
        override var name: String
) : BaseDictEntity(id, name)