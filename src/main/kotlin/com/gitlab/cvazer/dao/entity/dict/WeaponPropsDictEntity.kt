package com.gitlab.cvazer.dao.entity.dict

import javax.persistence.*

import org.hibernate.annotations.DynamicInsert

@Entity
@DynamicInsert
@Table(name = "t_weapon_props_dict")
data class WeaponPropsDictEntity (
        override var id: String,
        override var name: String,
        var page: Int? = null,
        var template: String,
        @Column(columnDefinition = "TEXT") var descr: String?,

        @ManyToOne
        @JoinColumn(name = "source", referencedColumnName = "id")
        var source: SourceDictEntity? = null
) : BaseDictEntity(id, name)