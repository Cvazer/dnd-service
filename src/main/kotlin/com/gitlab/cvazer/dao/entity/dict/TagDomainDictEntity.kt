package com.gitlab.cvazer.dao.entity.dict

import org.hibernate.annotations.DynamicInsert
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@DynamicInsert
@Table(name = "t_tag_domain_dict")
data class TagDomainDictEntity(
        @Id var id: String,
        var name: String,
)