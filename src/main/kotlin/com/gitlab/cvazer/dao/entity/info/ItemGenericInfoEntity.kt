package com.gitlab.cvazer.dao.entity.info

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.ItemEntity
import com.gitlab.cvazer.util.jsonString
import com.gitlab.cvazer.util.readAs
import javax.persistence.*

import org.hibernate.annotations.DynamicInsert

@Entity
@DynamicInsert
@Table(name = "t_item_generic_info")
data class ItemGenericInfoEntity(
        var addedValue: Long = 0,
        var namePrefix: String? = null,
        var nameSuffix: String? = null,

        @Id
        @Column(name = "item_id")
        var id: Long? = null,

        @MapsId
        @OneToOne
        @JsonIgnore
        @JoinColumn(name = "item_id", referencedColumnName = "id")
        var item: ItemEntity? = null,

        @Column(name = "inherits", columnDefinition = "JSON")
        private var _inherits: String = "[]",

        @Column(name = "exclude_by_names", columnDefinition = "JSON")
        private var _excludeByNames: String = "[]",

        @Column(name = "exclude_by_properties", columnDefinition = "JSON")
        private var _excludeByProperties: String = "[]",

        @Column(name = "exclude_by_pages", columnDefinition = "JSON")
        private var _excludeByPages: String = "[]",

        @Column(name = "requires", columnDefinition = "JSON")
        private var _requires: String = "[]"
) {
    var inherits: List<String>
        get() = _inherits.readAs()
        set(value) { _inherits = value.jsonString() }

    var excludeByNames: List<String>
        get() = _excludeByNames.readAs()
        set(value) { _excludeByNames = value.jsonString() }

    var excludeByProperties: List<String>
        get() = _excludeByProperties.readAs()
        set(value) { _excludeByProperties = value.jsonString() }

    var excludeByPages: List<Int>
        get() = _excludeByPages.readAs()
        set(value) { _excludeByPages = value.jsonString() }

    var requires: List<String>
        get() = _requires.readAs()
        set(value) { _requires = value.jsonString() }
}