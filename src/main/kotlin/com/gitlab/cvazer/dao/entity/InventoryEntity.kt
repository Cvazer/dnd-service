package com.gitlab.cvazer.dao.entity

import com.gitlab.cvazer.dao.entity.link.InvItemLinkEntity
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_inventory")
data class InventoryEntity (
        var name: String,
        var icon: String = "question",

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,

        @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, mappedBy = "inventory", orphanRemoval = true)
        var items: MutableList<InvItemLinkEntity> = mutableListOf()
)