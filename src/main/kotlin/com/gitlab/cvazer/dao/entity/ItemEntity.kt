package com.gitlab.cvazer.dao.entity

import com.gitlab.cvazer.dao.entity.dict.ItemTypeDictEntity
import com.gitlab.cvazer.dao.entity.info.*
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_item")
data class ItemEntity(
        var name: String,
        var value: Long? = null,
        var weight: Double? = null,
        var base: Boolean = false,
        var generic: Boolean = false,
        var concrete: Boolean = false,
        var catalogue: Boolean = false,

        @Column(columnDefinition = "TEXT") var descr: String? = null,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,

        @ManyToOne(cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name = "type", referencedColumnName = "id")
        var type: ItemTypeDictEntity,

        @ManyToOne(cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY)
        @JoinColumn(name = "base_id", referencedColumnName = "id")
        var baseItem: ItemEntity? = null
) {
        @OneToOne(mappedBy = "item", cascade = [CascadeType.ALL],
                fetch = FetchType.LAZY, orphanRemoval = true, optional = false)
        var vehicleInfo: ItemVehicleInfoEntity = ItemVehicleInfoEntity().also { it.item = this }

        @OneToOne(mappedBy = "item", cascade = [CascadeType.ALL],
                fetch = FetchType.LAZY, orphanRemoval = true, optional = false)
        var armorInfo: ItemArmorInfoEntity = ItemArmorInfoEntity().also { it.item = this }

        @OneToOne(mappedBy = "item", cascade = [CascadeType.ALL],
                fetch = FetchType.LAZY, orphanRemoval = true, optional = false)
        var catalogueInfo: ItemCatalogueInfoEntity = ItemCatalogueInfoEntity().also { it.item = this }

        @OneToOne(mappedBy = "item", cascade = [CascadeType.ALL],
                fetch = FetchType.LAZY, orphanRemoval = true, optional = false)
        var containerInfo: ItemContainerInfoEntity = ItemContainerInfoEntity().also { it.item = this }

        @OneToOne(mappedBy = "item", cascade = [CascadeType.ALL],
                fetch = FetchType.LAZY, orphanRemoval = true, optional = false)
        var genericInfo: ItemGenericInfoEntity = ItemGenericInfoEntity().also { it.item = this }

        @OneToOne(mappedBy = "item", cascade = [CascadeType.ALL],
                fetch = FetchType.LAZY, orphanRemoval = true, optional = false)
        var magicInfo: ItemMagicInfoEntity = ItemMagicInfoEntity().also { it.item = this }

        @OneToOne(mappedBy = "item", cascade = [CascadeType.ALL],
                fetch = FetchType.LAZY, orphanRemoval = true, optional = false)
        var miscInfo: ItemMiscInfoEntity = ItemMiscInfoEntity().also { it.item = this }

        @OneToOne(mappedBy = "item", cascade = [CascadeType.ALL],
                fetch = FetchType.LAZY, orphanRemoval = true, optional = false)
        var mountInfo: ItemMountInfoEntity = ItemMountInfoEntity().also { it.item = this }

        @OneToOne(mappedBy = "item", cascade = [CascadeType.ALL],
                fetch = FetchType.LAZY, orphanRemoval = true, optional = false)
        var sourceInfo: ItemSourceInfoEntity = ItemSourceInfoEntity().also { it.item = this }

        @OneToOne(mappedBy = "item", cascade = [CascadeType.ALL],
                fetch = FetchType.LAZY, orphanRemoval = true, optional = false)
        var weaponInfo: ItemWeaponInfoEntity = ItemWeaponInfoEntity().also { it.item = this }
}