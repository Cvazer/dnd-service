package com.gitlab.cvazer.dao.entity.link.identity

import java.io.Serializable
import javax.persistence.*

@Embeddable
data class CharHiddenLinkIdentity(
        @Column(insertable = false, updatable = false) var charId: Long?,
        @Column(insertable = false, updatable = false) var objectId: String?,
        @Column(insertable = false, updatable = false) var domain: String?
) : Serializable