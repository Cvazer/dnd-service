package com.gitlab.cvazer.dao.entity.link

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.CharEntity
import com.gitlab.cvazer.dao.entity.ClsEntity
import com.gitlab.cvazer.dao.entity.link.identity.CharClsLinkIdentity
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_char_cls_link")
data class CharClsLinkEntity(
        @EmbeddedId @JsonIgnore val identity: CharClsLinkIdentity = CharClsLinkIdentity(),

        @ManyToOne
        @JsonIgnore
        @MapsId("charId")
        @JoinColumn(name = "char_id", referencedColumnName = "id")
        val char: CharEntity,


        @MapsId("clsId")
        @OneToOne(cascade = [CascadeType.REFRESH])
        @JoinColumn(name = "cls_id", referencedColumnName = "id")
        val cls: ClsEntity,

        @OneToOne(cascade = [CascadeType.REFRESH])
        @JoinColumn(name = "subcls_id", referencedColumnName = "id")
        val subcls: ClsEntity?,

        val level: Int = 1
)