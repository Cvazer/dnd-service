package com.gitlab.cvazer.dao.entity.dict

import org.hibernate.annotations.DynamicInsert
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@DynamicInsert
@Table(name = "t_scrapping_resource_type_dict")
data class ScrappingResourceTypeDictEntity (
    override var id: String,
    override var name: String
) : BaseDictEntity(id, name)