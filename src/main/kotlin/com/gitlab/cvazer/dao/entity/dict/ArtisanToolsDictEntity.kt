package com.gitlab.cvazer.dao.entity.dict

import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "v_artisan_tools_dict")
data class ArtisanToolsDictEntity(
        override var id: String,
        override var name: String
) : BaseDictEntity(id, name)