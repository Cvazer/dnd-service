package com.gitlab.cvazer.dao.entity.link

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.CharEntity
import com.gitlab.cvazer.dao.entity.dict.SkillDictEntity
import com.gitlab.cvazer.dao.entity.link.identity.CharSkillLinkIdentity
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_char_skill_link")
data class CharSkillLinkEntity(
        @EmbeddedId @JsonIgnore val identity: CharSkillLinkIdentity = CharSkillLinkIdentity(),

        @ManyToOne
        @JsonIgnore
        @MapsId("charId")
        @JoinColumn(name = "char_id", referencedColumnName = "id")
        val char: CharEntity,

        @MapsId("skillId")
        @OneToOne(cascade = [CascadeType.REFRESH])
        @JoinColumn(name = "skill_id", referencedColumnName = "id")
        val skill: SkillDictEntity,

        var comp: Boolean = false,
        var prof: Boolean = false
)