package com.gitlab.cvazer.dao.entity.link.identity

import java.io.Serializable
import javax.persistence.*

@Embeddable
data class InvItemLinkIdentity(
        @Column(insertable = false, updatable = false) var invId: Long? = null,
        @Column(insertable = false, updatable = false) var itemId: Long? = null
) : Serializable