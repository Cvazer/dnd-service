package com.gitlab.cvazer.dao.entity.misc

import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_item_generation_index")
data class ItemGenerationIndexEntity(
        private val sourceId: Long,
        private val genericId: Long,
        private val specificId: Long,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null

)