package com.gitlab.cvazer.dao.entity.link.identity

import java.io.Serializable
import javax.persistence.*

@Embeddable
data class CharProfCatLinkIdentity(
        @Column(insertable = false, updatable = false) var charId: Long? = null,
        @Column(insertable = false, updatable = false) var profCatId: String? = null,
        @Column(insertable = false, updatable = false) var value: String? = null
) : Serializable