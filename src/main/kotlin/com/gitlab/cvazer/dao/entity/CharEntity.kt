package com.gitlab.cvazer.dao.entity

import com.gitlab.cvazer.constant.ProfCats
import com.gitlab.cvazer.dao.entity.dict.*
import com.gitlab.cvazer.dao.entity.info.CharGenericInfoEntity
import com.gitlab.cvazer.dao.entity.info.CharHpInfoEntity
import com.gitlab.cvazer.dao.entity.info.CharMiscInfoEntity
import com.gitlab.cvazer.dao.entity.info.CharMoneyInfoEntity
import com.gitlab.cvazer.dao.entity.link.*
import com.gitlab.cvazer.dao.repo.generic.*
import com.gitlab.cvazer.service.CharService
import com.gitlab.cvazer.util.getBean
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_char")
data class CharEntity (
        var name: String,
        var playerName: String,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,

        @ManyToOne(cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name = "race_id", referencedColumnName = "id")
        var race: RaceEntity,

        @ManyToOne(cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY)
        @JoinColumn(name = "subrace_id", referencedColumnName = "id")
        var subrace: RaceEntity? = null,

        @ManyToOne(cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name = "background_id", referencedColumnName = "id")
        var background: BackgroundEntity,

        @ManyToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
        @JoinTable(
                name = "t_char_feat_link",
                joinColumns = [JoinColumn(name = "char_id", referencedColumnName = "id")],
                inverseJoinColumns = [JoinColumn(name = "feat_id", referencedColumnName = "id")]
        )
        var feats: MutableList<FeatEntity> = mutableListOf(),

        @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
        @JoinTable(
                name = "t_char_inv_link",
                joinColumns = [JoinColumn(name = "char_id", referencedColumnName = "id")],
                inverseJoinColumns = [JoinColumn(name = "inv_id", referencedColumnName = "id")]
        )
        var inventories: MutableList<InventoryEntity> = mutableListOf(),

        @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, mappedBy = "char", orphanRemoval = true)
        var profs: MutableList<CharProfLinkEntity> = mutableListOf(),

        @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, mappedBy = "char", orphanRemoval = true)
        var hidden: MutableList<CharHiddenLink> = mutableListOf(),

        @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, mappedBy = "char")
        var skills: MutableList<CharSkillLinkEntity> = mutableListOf(),

        @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, mappedBy = "char")
        var stats: MutableList<CharStatLinkEntity> = mutableListOf(),

        @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, mappedBy = "char")
        var classes: MutableList<CharClsLinkEntity> = mutableListOf()
) {
        @OneToOne(mappedBy = "char", cascade = [CascadeType.ALL],
                fetch = FetchType.LAZY, orphanRemoval = true, optional = false)
        var genericInfo: CharGenericInfoEntity = CharGenericInfoEntity().also { it.char = this }

        @OneToOne(mappedBy = "char", cascade = [CascadeType.ALL],
                fetch = FetchType.LAZY, orphanRemoval = true, optional = false)
        var moneyInfo: CharMoneyInfoEntity = CharMoneyInfoEntity().also { it.char = this }

        @OneToOne(mappedBy = "char", cascade = [CascadeType.ALL],
                fetch = FetchType.LAZY, orphanRemoval = true, optional = false)
        var hpInfo: CharHpInfoEntity = CharHpInfoEntity().also { it.char = this }

        @OneToOne(mappedBy = "char", cascade = [CascadeType.ALL],
                fetch = FetchType.LAZY, orphanRemoval = true, optional = false)
        var miscInfo: CharMiscInfoEntity = CharMiscInfoEntity().also { it.char = this }

        val armorProfs: List<BaseArmorDictEntity>
                get() = profs.filter { it.profCat.id === ProfCats.ARMOR.id }
                        .map { getBean<BaseArmorDictDao>().getById(it.identity.value!!) }

        val artisanToolProfs: List<ArtisanToolsDictEntity>
                get() = profs.filter { it.profCat.id === ProfCats.ARTISAN_TOOLS.id }
                        .map { getBean<ArtisanToolsDictDao>().getById(it.identity.value!!) }

        val gamingSetProfs: List<GamingSetDictEntity>
                get() = profs.filter { it.profCat.id === ProfCats.GAMING_SET.id }
                        .map { getBean<GamingSetDictDao>().getById(it.identity.value!!) }

        val langProfs: List<LangDictEntity>
                get() = profs.filter { it.profCat.id === ProfCats.LANGUAGE.id }
                        .map { getBean<LangDictDao>().getById(it.identity.value!!) }

        val musInstrProfs: List<MusicalInstrumentDictEntity>
                get() = profs.filter { it.profCat.id === ProfCats.MUSICAL_INSTRUMENT.id }
                        .map { getBean<MusicalInstrumentDictDao>().getById(it.identity.value!!) }

        val toolProfs: List<ToolsDictEntity>
                get() = profs.filter { it.profCat.id === ProfCats.TOOLS.id }
                        .map { getBean<ToolsDictDao>().getById(it.identity.value!!) }

        val weaponProfs: List<BaseWeaponDictEntity>
                get() = profs.filter { it.profCat.id === ProfCats.WEAPON.id }
                        .map { getBean<BaseWeaponDictDao>().getById(it.identity.value!!) }

        fun getProfBonus() = CharService.Lvl.find(genericInfo.lvl)?.prof ?: 2
}