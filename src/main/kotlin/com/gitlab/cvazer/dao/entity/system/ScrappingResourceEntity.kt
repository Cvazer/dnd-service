package com.gitlab.cvazer.dao.entity.system

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.dict.ScrappingResourceTypeDictEntity
import com.gitlab.cvazer.system.scrapping.abstraction.Scrap
import com.gitlab.cvazer.util.jsonString
import com.gitlab.cvazer.util.readAs
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_scrapping_resource")
data class ScrappingResourceEntity (
    override val locator: String,
    val source: String,
    val subType: String? = null,
    override var rawHash: String? = null,
    var baked_hash: String? = null,
    override var bake: Boolean = false,
    var resId: String? = null,

    @Column(columnDefinition = "TEXT") var data: String,
    @JsonIgnore @Column(name = "meta", columnDefinition = "JSON") var _meta: String? = "{}",

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,

    @ManyToOne(cascade = [CascadeType.REFRESH], optional = false)
    @JoinColumn(name = "type", referencedColumnName = "id")
    val type: ScrappingResourceTypeDictEntity,
): Scrap<String, String> {
    var meta: MutableMap<String, Any>
        get() = _meta?.readAs() ?: mutableMapOf()
        set(value) { _meta = value.jsonString() }
}