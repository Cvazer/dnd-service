package com.gitlab.cvazer.dao.entity.info

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.ItemEntity
import com.gitlab.cvazer.dao.entity.dict.AgeDictEntity
import com.gitlab.cvazer.model.meta.PackItem
import com.gitlab.cvazer.util.jsonString
import com.gitlab.cvazer.util.readAs
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_item_misc_info")
data class ItemMiscInfoEntity(
        var tattoo: Boolean = false,
        var staff: Boolean = false,
        var focus: Boolean = false,
        var axe: Boolean = false,
        var sword: Boolean = false,
        var resist: Boolean = false,
        var firearm: Boolean = false,
        var ammo: Boolean = false,
        var poison: Boolean = false,
        var srd: Boolean = false,
        var srdName: String? = null,
        var tier: String? = null,
        var weightFactor: Double? = null,
        var valueFactor: Double? = null,
        var quantity: Int = 1,

        @Id
        @Column(name = "item_id")
        var id: Long? = null,

        @MapsId
        @OneToOne
        @JsonIgnore
        @JoinColumn(name = "item_id", referencedColumnName = "id")
        var item: ItemEntity? = null,

        @ManyToOne(cascade = [CascadeType.REFRESH])
        @JoinColumn(name = "age", referencedColumnName = "id")
        var age: AgeDictEntity? = null,

        @Column(name = "poison_types", columnDefinition = "JSON")
        private var _poisonTypes: String = "[]",

        @Column(name = "loot_tables", columnDefinition = "JSON")
        private var _lootTables: String = "[]",

        @Column(name ="pack", columnDefinition = "JSON")
        private var _pack: String = "[]"
) {
    var poisonTypes: List<String>
        get() = _poisonTypes.readAs()
        set(value) { _poisonTypes = value.jsonString() }

    var lootTables: List<String>
        get() = _lootTables.readAs()
        set(value) { _lootTables = value.jsonString() }

    var pack: List<PackItem>
        get() = _pack.readAs()
        set(value) { _pack = value.jsonString() }
}