package com.gitlab.cvazer.dao.entity.system

import com.gitlab.cvazer.dao.entity.link.AccountCharLinkEntity
import javax.persistence.*

@Entity
@Table(name = "t_account")
data class AccountEntity(
        @Id @Column(name = "account_key") val key: String,
        var name: String,
        var role: String,

        @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, mappedBy = "account", orphanRemoval = true)
        var chars: MutableList<AccountCharLinkEntity> = mutableListOf()
)