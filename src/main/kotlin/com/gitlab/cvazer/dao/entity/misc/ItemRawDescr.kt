package com.gitlab.cvazer.dao.entity.misc

import com.gitlab.cvazer.model.meta.DescrEntry
import com.gitlab.cvazer.util.jsonString
import com.gitlab.cvazer.util.readAs
import org.hibernate.annotations.DynamicInsert
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@DynamicInsert
@Table(name = "t_item_raw_descr")
data class ItemRawDescr(
        @Id private var itemId: Long,

        @Column(name = "raw", columnDefinition = "JSON")
        private var _raw: String = "[]"
) {
        var raw: MutableList<DescrEntry>
                get() = _raw.readAs()
                set(value) { _raw = value.jsonString() }
}