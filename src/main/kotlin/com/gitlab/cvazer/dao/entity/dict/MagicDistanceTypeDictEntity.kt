package com.gitlab.cvazer.dao.entity.dict

import javax.persistence.Entity
import javax.persistence.Table

import org.hibernate.annotations.DynamicInsert

@Entity
@DynamicInsert
@Table(name = "t_magic_distance_type_dict")
data class MagicDistanceTypeDictEntity(
        override var id: String,
        override var name: String
) : BaseDictEntity(id, name)