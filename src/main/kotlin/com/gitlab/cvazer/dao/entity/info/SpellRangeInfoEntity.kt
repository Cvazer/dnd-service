package com.gitlab.cvazer.dao.entity.info

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.ItemEntity
import com.gitlab.cvazer.dao.entity.SpellEntity
import com.gitlab.cvazer.dao.entity.dict.*
import com.gitlab.cvazer.util.Md5Hashable
import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*

@Entity
@DynamicUpdate
@Table(name = "t_spell_range_info")
class SpellRangeInfoEntity(
    var amount: Int? = null,

    @Id
    @Column(name = "spell_id")
    var id: Long? = null,

    @MapsId
    @OneToOne
    @JsonIgnore
    @JoinColumn(name = "spell_id", referencedColumnName = "id")
    var spell: SpellEntity? = null,

    @ManyToOne(cascade = [CascadeType.REFRESH], optional = false)
    @JoinColumn(name = "type_id", referencedColumnName = "id")
    var type: MagicRangeTypeDictEntity? = null,

    @ManyToOne(cascade = [CascadeType.REFRESH])
    @JoinColumn(name = "distance_id", referencedColumnName = "id")
    var distance: MagicDistanceTypeDictEntity? = null,
)