package com.gitlab.cvazer.dao.entity.info

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.ItemEntity
import com.gitlab.cvazer.dao.entity.dict.VehicleTypeDictEntity
import javax.persistence.*

import org.hibernate.annotations.DynamicInsert

@Entity
@DynamicInsert
@Table(name = "t_item_vehicle_info")
data class ItemVehicleInfoEntity(
        var vehicle: Boolean = false,
        var crew: Int? = null,
        var crewMin: Int? = null,
        var crewMax: Int? = null,
        var ac: Int? = null,
        var hp: Int? = null,
        var speed: Int? = null,
        var passengers: Int? = null,
        var cargo: Int? = null,
        var dmgThreshold: Int? = null,
        var travelCost: Long? = null,
        var shippingCost: Long? = null,

        @Id
        @Column(name = "item_id")
        var id: Long? = null,

        @MapsId
        @OneToOne
        @JsonIgnore
        @JoinColumn(name = "item_id", referencedColumnName = "id")
        var item: ItemEntity? = null,


        @ManyToOne(cascade = [CascadeType.REFRESH])
        @JoinColumn(name = "type", referencedColumnName = "id")
        var type: VehicleTypeDictEntity? = null
)

