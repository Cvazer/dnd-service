package com.gitlab.cvazer.dao.entity.dict

import com.gitlab.cvazer.model.meta.DescEntryType
import com.gitlab.cvazer.model.meta.DescrEntry
import com.gitlab.cvazer.util.jsonString
import com.gitlab.cvazer.util.readAs
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_item_type_dict")
data class ItemTypeDictEntity(
        override var id: String,
        override var name: String,
        var page: Int? = null,
        @Column(name = "entries", columnDefinition = "JSON") private var _entries: String = "[]",

        @ManyToOne
        @JoinColumn(name = "source", referencedColumnName = "id")
        var source: SourceDictEntity? = null
) : BaseDictEntity(id, name) {
    var entries: MutableList<DescrEntry>
        get() { return _entries.readAs() }
        set(value) { _entries = value.map {
            @Suppress("IMPLICIT_CAST_TO_ANY")
            when (it.type){
                DescEntryType.PLAIN.value -> it.value
                else -> it
            }
        }.jsonString() }
}