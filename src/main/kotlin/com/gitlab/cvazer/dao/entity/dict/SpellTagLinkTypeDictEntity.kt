package com.gitlab.cvazer.dao.entity.dict

import javax.persistence.Entity
import javax.persistence.Table

import org.hibernate.annotations.DynamicInsert
import javax.persistence.Id

@Entity
@DynamicInsert
@Table(name = "t_spell_tag_link_type_dict")
data class SpellTagLinkTypeDictEntity(
        @Id var id: String,
        var name: String
)