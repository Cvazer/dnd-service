package com.gitlab.cvazer.dao.entity.dict

import org.hibernate.annotations.DynamicInsert
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@DynamicInsert
@Table(name = "t_age_dict")
data class AgeDictEntity(
        override var id: String,
        override var name: String
) : BaseDictEntity(id, name)