package com.gitlab.cvazer.dao.entity.dict

import javax.persistence.Id
import javax.persistence.MappedSuperclass

@MappedSuperclass
class BaseDictEntity (
    @Id var id: String,
    var name: String
)