package com.gitlab.cvazer.dao.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.dict.GameEventTypeDictEntity
import com.gitlab.cvazer.dao.entity.system.AccountEntity
import com.gitlab.cvazer.model.icu.ICUDateTime
import org.hibernate.annotations.DynamicInsert
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_game_event_log")
data class GameEventLogEntity (
        val timestamp: LocalDateTime = LocalDateTime.now(),
        @Suppress("JpaAttributeTypeInspection") val gameDate: ICUDateTime?,
        @Column(columnDefinition = "TEXT") var textData: String?,
        @Column(columnDefinition = "JSON") var meta: String? = "{}",

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,

        @ManyToOne(cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name = "type", referencedColumnName = "id")
        val type: GameEventTypeDictEntity,

        @JsonIgnore
        @ManyToOne(cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY)
        @JoinColumn(name = "campaign_id", referencedColumnName = "id")
        val campaign: CampaignEntity? = null,

        @JsonIgnore
        @ManyToOne(cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY)
        @JoinColumn(name = "account_key", referencedColumnName = "account_key")
        val account: AccountEntity? = null,

        @JsonIgnore
        @ManyToOne(cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY)
        @JoinColumn(name = "char_id", referencedColumnName = "id")
        val char: CharEntity? = null
)