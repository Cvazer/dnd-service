package com.gitlab.cvazer.dao.entity.system

import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import java.time.LocalDateTime
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "t_token")
class TokenEntity(
    val created: LocalDateTime = LocalDateTime.now(),
    val purpose: String? = null,
    val valid: Boolean = true,

    @Id
    val id: String = UUID.randomUUID().toString()
)