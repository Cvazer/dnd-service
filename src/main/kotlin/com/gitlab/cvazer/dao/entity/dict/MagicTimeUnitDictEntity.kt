package com.gitlab.cvazer.dao.entity.dict

import javax.persistence.Entity
import javax.persistence.Table

import org.hibernate.annotations.DynamicInsert

@Entity
@DynamicInsert
@Table(name = "t_magic_time_unit_dict")
data class MagicTimeUnitDictEntity(
        override var id: String,
        override var name: String,
        var plural: String
) : BaseDictEntity(id, name)