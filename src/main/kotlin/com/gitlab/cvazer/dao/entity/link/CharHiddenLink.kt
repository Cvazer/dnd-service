package com.gitlab.cvazer.dao.entity.link

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.CharEntity
import com.gitlab.cvazer.dao.entity.link.identity.CharHiddenLinkIdentity
import javax.persistence.*

@Entity
@Table(name = "t_char_hidden_link")
data class CharHiddenLink(

    @EmbeddedId
    val identity: CharHiddenLinkIdentity,

    @ManyToOne
    @JsonIgnore
    @MapsId("charId")
    @JoinColumn(name = "char_id", referencedColumnName = "id")
    val char: CharEntity
)