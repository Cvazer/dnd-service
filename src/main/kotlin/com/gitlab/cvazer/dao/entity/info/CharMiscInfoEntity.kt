package com.gitlab.cvazer.dao.entity.info

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.CharEntity
import com.gitlab.cvazer.model.character.CharNotes
import com.gitlab.cvazer.model.character.CharAttack
import com.gitlab.cvazer.model.character.magic.CharMagic
import com.gitlab.cvazer.util.jsonString
import com.gitlab.cvazer.util.readAs
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_char_misc_info")
data class CharMiscInfoEntity (
        @Column(name = "attacks", columnDefinition = "json") private var _attacks: String = "[]",
        @Column(name = "notes", columnDefinition = "json") private var _notes: String = "{}",
        @Column(name = "magic", columnDefinition = "json") private var _magic: String = "{}",

        @Id
        @Column(name = "char_id")
        private var _charId: Long? = null,

        @MapsId
        @OneToOne
        @JsonIgnore
        @JoinColumn(name = "char_id", referencedColumnName = "id")
        var char: CharEntity? = null
) {
        var attacks: List<CharAttack>
                get() = _attacks.readAs()
                set(value) { _attacks = value.jsonString() }

        var notes: CharNotes
                get() = _notes.readAs()
                set(value) { _notes = value.jsonString() }

        var magic: CharMagic
                get() = _magic.readAs()
                set(value) { _magic = value.jsonString() }
}