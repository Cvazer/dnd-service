package com.gitlab.cvazer.dao.entity.link

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.CharEntity
import com.gitlab.cvazer.dao.entity.dict.ProfCatDictEntity
import com.gitlab.cvazer.dao.entity.link.identity.CharProfCatLinkIdentity
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_char_prof_link")
data class CharProfLinkEntity(
        @EmbeddedId @JsonIgnore val identity: CharProfCatLinkIdentity = CharProfCatLinkIdentity(),

        @ManyToOne
        @JsonIgnore
        @MapsId("charId")
        @JoinColumn(name = "char_id", referencedColumnName = "id")
        val char: CharEntity,

        @MapsId("profCatId")
        @OneToOne(cascade = [CascadeType.REFRESH])
        @JoinColumn(name = "prof_cat_id", referencedColumnName = "id")
        val profCat: ProfCatDictEntity
)