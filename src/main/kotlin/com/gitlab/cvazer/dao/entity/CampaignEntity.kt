package com.gitlab.cvazer.dao.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.system.AccountEntity
import com.gitlab.cvazer.dao.repo.CampaignDao
import com.gitlab.cvazer.model.icu.ICUDateTime
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_campaign")
data class CampaignEntity(
    var name: String,
    @Suppress("JpaAttributeTypeInspection") var currentDateTime: ICUDateTime?,

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @JsonIgnore
    @ManyToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    @JoinTable(
        name = "t_char_campaign_link",
        joinColumns = [JoinColumn(name = "campaign_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "char_id", referencedColumnName = "id")]
    )
    var chars: MutableList<CharEntity> = mutableListOf(),

    @JsonIgnore
    @ManyToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    @JoinTable(
        name = "t_account_campaign_link",
        joinColumns = [JoinColumn(name = "campaign_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "account_key", referencedColumnName = "account_key")]
    )
    var accounts: MutableList<AccountEntity> = mutableListOf()
)