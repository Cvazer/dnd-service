package com.gitlab.cvazer.dao.entity.dict

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_skill_dict")
data class SkillDictEntity(
        override var id: String,
        override var name: String,
        var nameEng: String,

        @JsonIgnore
        @ManyToOne(cascade = [CascadeType.REFRESH])
        @JoinColumn(name = "stat_id", referencedColumnName = "id")
        var stat: StatDictEntity
) : BaseDictEntity(id, name)