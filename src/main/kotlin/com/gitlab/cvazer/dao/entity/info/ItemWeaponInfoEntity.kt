package com.gitlab.cvazer.dao.entity.info

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.ItemEntity
import com.gitlab.cvazer.dao.entity.dict.DamageTypeDictEntity
import com.gitlab.cvazer.dao.entity.dict.WeaponCategoryDictEntity
import com.gitlab.cvazer.dao.entity.dict.WeaponDistanceDictEntity
import com.gitlab.cvazer.model.meta.WeaponPropsEntry
import com.gitlab.cvazer.util.jsonString
import com.gitlab.cvazer.util.readAs
import javax.persistence.*

import org.hibernate.annotations.DynamicInsert

@Entity
@DynamicInsert
@Table(name = "t_item_weapon_info")
data class ItemWeaponInfoEntity(
        var weapon: Boolean = false,
        var damage: String? = null,
        var reload: Int? = null,
        var rangeEffective: Int? = null,
        var rangeMax: Int? = null,
        var ammoId: Long? = null,

        @Id
        @Column(name = "item_id")
        var id: Long? = null,

        @MapsId
        @OneToOne
        @JsonIgnore
        @JoinColumn(name = "item_id", referencedColumnName = "id")
        var item: ItemEntity? = null,

        @Column(name = "props", columnDefinition = "JSON")
        private var _props: String = "[]",

        @ManyToOne(cascade = [CascadeType.REFRESH])
        @JoinColumn(name = "category", referencedColumnName = "id")
        var category: WeaponCategoryDictEntity? = null,

        @ManyToOne(cascade = [CascadeType.REFRESH])
        @JoinColumn(name = "distance", referencedColumnName = "id")
        var distance: WeaponDistanceDictEntity? = null,

        @ManyToOne(cascade = [CascadeType.REFRESH])
        @JoinColumn(name = "damage_type", referencedColumnName = "id")
        var damageType: DamageTypeDictEntity? = null

) {
    var props: List<WeaponPropsEntry>
        get() = _props.readAs()
        set(value) { _props = value.jsonString() };

    fun range(): String? {
        if (rangeEffective == null || rangeMax == null) return null
        return "$rangeEffective/$rangeMax"
    }
}