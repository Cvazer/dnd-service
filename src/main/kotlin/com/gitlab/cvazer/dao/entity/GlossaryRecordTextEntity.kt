package com.gitlab.cvazer.dao.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_glossary_record_text")
data class GlossaryRecordTextEntity(
    @Column(columnDefinition = "TEXT") val data: String?,
    val age: Int,

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "record_id", referencedColumnName = "id", nullable = false)
    var record: GlossaryRecordEntity,
)