package com.gitlab.cvazer.dao.entity.link.identity

import java.io.Serializable
import javax.persistence.*

@Embeddable
data class AccountCharLinkIdentity(
        @Column(insertable = false, updatable = false) var charId: Long? = null,
        @Column(insertable = false, updatable = false) var accountKey: String? = null
) : Serializable