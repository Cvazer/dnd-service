package com.gitlab.cvazer.dao.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.dict.MagicSchoolDictEntity
import com.gitlab.cvazer.dao.entity.dict.SourceDictEntity
import com.gitlab.cvazer.dao.entity.info.SpellCastingInfoEntity
import com.gitlab.cvazer.dao.entity.info.SpellDurationInfoEntity
import com.gitlab.cvazer.dao.entity.info.SpellRangeInfoEntity
import com.gitlab.cvazer.dao.entity.link.SpellTagLinkEntity
import com.gitlab.cvazer.util.jsonString
import com.gitlab.cvazer.util.readAs
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_spell")
data class SpellEntity(
        var name: String,
        var level: Int? = 0,

        @JsonIgnore
        @Column(name = "entries", columnDefinition = "JSON")
        private var _entries: String? = null,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null,

        @ManyToOne(cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name = "source_id", referencedColumnName = "id")
        var source: SourceDictEntity = SourceDictEntity("NONE", "Нет"),

        @ManyToOne(cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name = "school_id", referencedColumnName = "id")
        var school: MagicSchoolDictEntity = MagicSchoolDictEntity("UNKNOWN", "Unknown"),

) {
        @OneToOne(mappedBy = "spell", cascade = [CascadeType.ALL],
                fetch = FetchType.LAZY, orphanRemoval = true, optional = false)
        var durationInfo: SpellDurationInfoEntity = SpellDurationInfoEntity().also { it.spell = this }

        @OneToOne(mappedBy = "spell", cascade = [CascadeType.ALL],
                fetch = FetchType.LAZY, orphanRemoval = true, optional = false)
        var rangeInfo: SpellRangeInfoEntity = SpellRangeInfoEntity().also { it.spell = this }

        @OneToOne(mappedBy = "spell", cascade = [CascadeType.ALL],
                fetch = FetchType.LAZY, orphanRemoval = true, optional = false)
        var castingInfo: SpellCastingInfoEntity = SpellCastingInfoEntity().also { it.spell = this }

        @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, mappedBy = "spell")
        var tags: MutableList<SpellTagLinkEntity> = mutableListOf()

        var entries: List<Any?>
                get() = _entries?.readAs() ?: listOf()
                set(value) { _entries = value.jsonString() }
}