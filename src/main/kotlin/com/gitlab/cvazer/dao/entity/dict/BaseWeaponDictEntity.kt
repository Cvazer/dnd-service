package com.gitlab.cvazer.dao.entity.dict

import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "v_base_weapon_dict")
data class BaseWeaponDictEntity(
        override var id: String,
        override var name: String
) : BaseDictEntity(id, name)