package com.gitlab.cvazer.dao.entity.dict

import javax.persistence.Entity
import javax.persistence.Table

import org.hibernate.annotations.DynamicInsert

@Entity
@DynamicInsert
@Table(name = "t_size_dict")
data class SizeDictEntity(
        override var id: String,
        override var name: String,
        var nameEng: String
) : BaseDictEntity(id, name)