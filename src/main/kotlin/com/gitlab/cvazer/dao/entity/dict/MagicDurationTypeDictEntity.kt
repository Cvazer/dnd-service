package com.gitlab.cvazer.dao.entity.dict

import javax.persistence.Entity
import javax.persistence.Table

import org.hibernate.annotations.DynamicInsert

@Entity
@DynamicInsert
@Table(name = "t_magic_duration_type_dict")
data class MagicDurationTypeDictEntity(
        override var id: String,
        override var name: String
) : BaseDictEntity(id, name)