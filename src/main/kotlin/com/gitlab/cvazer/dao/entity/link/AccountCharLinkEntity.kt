package com.gitlab.cvazer.dao.entity.link

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.CharEntity
import com.gitlab.cvazer.dao.entity.link.identity.AccountCharLinkIdentity
import com.gitlab.cvazer.dao.entity.system.AccountEntity
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_account_char_link")
data class AccountCharLinkEntity(
        @EmbeddedId @JsonIgnore val identity: AccountCharLinkIdentity = AccountCharLinkIdentity(),
        val tab: String = "Все",

        @MapsId("charId")
        @OneToOne(cascade = [CascadeType.REFRESH])
        @JoinColumn(name = "char_id", referencedColumnName = "id")
        val char: CharEntity,


        @JsonIgnore
        @MapsId("accountKey")
        @ManyToOne(cascade = [CascadeType.REFRESH])
        @JoinColumn(name = "account_key", referencedColumnName = "account_key")
        val account: AccountEntity
)