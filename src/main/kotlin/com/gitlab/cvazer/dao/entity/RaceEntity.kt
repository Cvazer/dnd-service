package com.gitlab.cvazer.dao.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.dict.SourceDictEntity
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_race")
data class RaceEntity(
        var name: String,
        var sizeId: String? = null,
        var sourcePage: Int = 0,
        var npc: Boolean = false,
        var srd: Boolean = false,
        @Column(name = "speed", columnDefinition = "JSON") private var _speed: String = "{}",
        @Column(name = "stats", columnDefinition = "JSON") private var _stats: String = "{}",
        @Column(name = "meta", columnDefinition = "JSON") private var _meta: String = "{}",
        @Column(columnDefinition = "TEXT") var descr: String = "",

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,

        @ManyToOne
        @JsonIgnore
        @JoinColumn(name = "parent_id", referencedColumnName = "id")
        var parent: RaceEntity?,

        @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, mappedBy = "parent")
        var subraces: MutableList<RaceEntity>,

        @ManyToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
        @JoinTable(
                name = "t_race_trait_link",
                joinColumns = [JoinColumn(name = "race_id", referencedColumnName = "id")],
                inverseJoinColumns = [JoinColumn(name = "trait_id", referencedColumnName = "id")]
        )
        var traits: MutableList<TraitEntity>,

        @ManyToOne(cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name = "source_id", referencedColumnName = "id")
        var source: SourceDictEntity = SourceDictEntity("NONE", "Нет")
)