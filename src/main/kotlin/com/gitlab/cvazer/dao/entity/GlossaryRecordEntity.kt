package com.gitlab.cvazer.dao.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_glossary_record")
data class GlossaryRecordEntity(
    val name: String,

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = [CascadeType.REFRESH])
    @JoinColumn(name = "parent_id", referencedColumnName = "id")
    val parent: GlossaryRecordEntity? = null,

    @OneToMany(mappedBy = "record", cascade = [CascadeType.ALL],
        fetch = FetchType.LAZY, orphanRemoval = true)
    val entries: MutableList<GlossaryRecordTextEntity> = mutableListOf(),
)