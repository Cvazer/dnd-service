package com.gitlab.cvazer.dao.entity.link.identity

import java.io.Serializable
import javax.persistence.*

@Embeddable
data class AccountCampaignLinkIdentity(
        @Column(insertable = false, updatable = false) var campaignId: Long? = null,
        @Column(insertable = false, updatable = false) var accountKey: String? = null
) : Serializable