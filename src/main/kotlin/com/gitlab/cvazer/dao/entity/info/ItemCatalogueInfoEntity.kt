package com.gitlab.cvazer.dao.entity.info

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.ItemEntity
import com.gitlab.cvazer.util.jsonString
import com.gitlab.cvazer.util.readAs
import javax.persistence.*

import org.hibernate.annotations.DynamicInsert

@Entity
@DynamicInsert
@Table(name = "t_item_catalogue_info")
data class ItemCatalogueInfoEntity(
        @Column(name = "items", columnDefinition = "JSON") private var _items: String = "[]",

        @Id
        @Column(name = "item_id")
        var id: Long? = null,

        @MapsId
        @OneToOne
        @JsonIgnore
        @JoinColumn(name = "item_id", referencedColumnName = "id")
        var item: ItemEntity? = null
) {
    var items: List<Long>
        get() = _items.readAs()
        set(value) { _items = value.jsonString() }
}