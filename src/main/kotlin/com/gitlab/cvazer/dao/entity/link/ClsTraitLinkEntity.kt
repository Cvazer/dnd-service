package com.gitlab.cvazer.dao.entity.link

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.ClsEntity
import com.gitlab.cvazer.dao.entity.TraitEntity
import com.gitlab.cvazer.dao.entity.link.identity.ClsTraitLinkIdentity
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_cls_trait_link")
data class ClsTraitLinkEntity(
        @EmbeddedId @JsonIgnore val identity: ClsTraitLinkIdentity,

        @ManyToOne
        @JsonIgnore
        @MapsId("clsId")
        @JoinColumn(name = "cls_id", referencedColumnName = "id")
        val cls: ClsEntity,

        @OneToOne
        @MapsId("traitId")
        @JoinColumn(name = "trait_id", referencedColumnName = "id")
        val trait: TraitEntity,

        var lvl: Int = 3
)