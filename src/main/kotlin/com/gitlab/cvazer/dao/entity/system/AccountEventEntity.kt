package com.gitlab.cvazer.dao.entity.system

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.dict.AccountEventTypeDictEntity
import com.gitlab.cvazer.util.jsonString
import com.gitlab.cvazer.util.readAs
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*
import javax.persistence.CascadeType.*

@Entity
@DynamicInsert
@Table(name = "t_account_event")
class AccountEventEntity(
    var seen: Boolean = false,
    @Column(columnDefinition = "TEXT") var textData: String? = null,

    @JsonIgnore
    @Column(columnDefinition = "JSON", name = "meta")
    var _meta: String? = null,

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,

    @ManyToOne(cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "type_id", referencedColumnName = "id")
    var type: AccountEventTypeDictEntity,

    @OneToOne(cascade = [REFRESH, PERSIST, DETACH, MERGE], fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "token_id", referencedColumnName = "id")
    val token: TokenEntity? = null,

    @JsonIgnore
    @ManyToOne(cascade = [REFRESH], fetch = FetchType.LAZY)
    @JoinColumn(name = "account_key", referencedColumnName = "account_key")
    val account: AccountEntity
) {
    var meta: Map<String, Any?>?
        get() = _meta?.readAs()
        set(value) { _meta = value.jsonString() }
}