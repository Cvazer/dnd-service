package com.gitlab.cvazer.dao.entity.system

import com.gitlab.cvazer.dao.entity.dict.TagDomainDictEntity
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*
import javax.persistence.CascadeType.*

@Entity
@DynamicInsert
@Table(name = "t_tag")
class TagEntity (
    @Id var id: String,
    var name: String,

    @ManyToOne(cascade = [REFRESH, PERSIST], optional = false)
    @JoinColumn(name = "domain_id", referencedColumnName = "id")
    var domain: TagDomainDictEntity? = null
)