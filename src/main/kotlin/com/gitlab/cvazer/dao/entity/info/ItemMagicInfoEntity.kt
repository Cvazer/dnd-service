package com.gitlab.cvazer.dao.entity.info

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.ItemEntity
import com.gitlab.cvazer.dao.entity.dict.DamageTypeDictEntity
import com.gitlab.cvazer.dao.entity.dict.MagicRechargeDictEntity
import com.gitlab.cvazer.dao.entity.dict.RarityDictEntity
import com.gitlab.cvazer.dao.entity.dict.ScfTypeDictEntity
import com.gitlab.cvazer.util.jsonString
import com.gitlab.cvazer.util.readAs
import javax.persistence.*

import org.hibernate.annotations.DynamicInsert

@Entity
@DynamicInsert
@Table(name = "t_item_magic_info")
data class ItemMagicInfoEntity(
        var magical: Boolean = false,
        var attunement: Boolean = false,
        var wondrous: Boolean = false,
        var curse: Boolean = false,
        var sentient: Boolean = false,
        var attuneCondition: String? = null,
        var bonusBase: Int = 0,
        var bonusAc: Int = 0,
        var bonusAttack: Int = 0,
        var bonusSave: Int = 0,
        var bonusSpell: Int = 0,
        var bonusDamage: Int = 0,

        @Id
        @Column(name = "item_id")
        var id: Long? = null,

        @MapsId
        @OneToOne
        @JsonIgnore
        @JoinColumn(name = "item_id", referencedColumnName = "id")
        var item: ItemEntity? = null,

        @ManyToOne(cascade = [CascadeType.REFRESH])
        @JoinColumn(name = "scf_id", referencedColumnName = "id")
        var scf: ScfTypeDictEntity? = null,

        @ManyToOne(cascade = [CascadeType.REFRESH])
        @JoinColumn(name = "rarity", referencedColumnName = "id")
        var rarity: RarityDictEntity? = null,

        @ManyToOne(cascade = [CascadeType.REFRESH])
        @JoinColumn(name = "resist_type", referencedColumnName = "id")
        var resistType: DamageTypeDictEntity? = null,

        @ManyToOne(cascade = [CascadeType.REFRESH])
        @JoinColumn(name = "recharge", referencedColumnName = "id")
        var recharge: MagicRechargeDictEntity? = null,

        @Column(columnDefinition = "JSON")
        var ability: String? = "{}",

        @Column(name = "attached_spells", columnDefinition = "JSON")
        private var _attachedSpells: String = "[]",

        @Column(name = "focus_for_classes", columnDefinition = "JSON")
        private var _focusForClasses: String = "[]"
) {
    var attachedSpells: List<String>
        get() = _attachedSpells.readAs()
        set(value) { _attachedSpells = value.jsonString() }

    var focusForClasses: List<String>
        get() = _focusForClasses.readAs()
        set(value) { _focusForClasses = value.jsonString() }
}