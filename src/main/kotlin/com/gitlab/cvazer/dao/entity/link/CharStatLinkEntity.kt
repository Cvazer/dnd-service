package com.gitlab.cvazer.dao.entity.link

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.CharEntity
import com.gitlab.cvazer.dao.entity.dict.StatDictEntity
import com.gitlab.cvazer.dao.entity.link.identity.CharStatLinkIdentity
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*
import kotlin.math.floor

@Entity
@DynamicInsert
@Table(name = "t_char_stats_link")
data class CharStatLinkEntity(
        @EmbeddedId @JsonIgnore val identity: CharStatLinkIdentity = CharStatLinkIdentity(),

        @ManyToOne
        @JsonIgnore
        @MapsId("charId")
        @JoinColumn(name = "char_id", referencedColumnName = "id")
        val char: CharEntity,


        @MapsId("statId")
        @OneToOne(cascade = [CascadeType.REFRESH])
        @JoinColumn(name = "stat_id", referencedColumnName = "id")
        val stat: StatDictEntity,

        var value: Int = 10,
        var save: Boolean = false
) {
        fun getMod() = Companion.getMod(value)
        fun getSaveMod(profBonus: Int) = getMod() + if (save) profBonus else 0

        companion object {
                fun getMod(value: Int) = floor((value - 10) / 2F).toInt()
        }
}