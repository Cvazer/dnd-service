package com.gitlab.cvazer.dao.entity.info

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.CharEntity
import com.gitlab.cvazer.util.business.Money
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_char_money_info")
data class CharMoneyInfoEntity (
        var pp: Int = 0,
        var gp: Int = 0,
        var sp: Int = 0,
        var cp: Int = 0,

        @Id
        @Column(name = "char_id")
        private var _charId: Long? = null,

        @MapsId
        @OneToOne
        @JsonIgnore
        @JoinColumn(name = "char_id", referencedColumnName = "id")
        var char: CharEntity? = null
) {
        var money: Money
                get() = Money(cp, sp, gp, pp)
                set(value) { cp = value.cp; sp = value.sp; gp = value.gp; pp = value.pp }
}