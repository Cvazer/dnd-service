package com.gitlab.cvazer.dao.entity.info

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.ItemEntity
import com.gitlab.cvazer.dao.entity.SpellEntity
import com.gitlab.cvazer.dao.entity.dict.MagicDurationTypeDictEntity
import com.gitlab.cvazer.dao.entity.dict.MagicRangeTypeDictEntity
import com.gitlab.cvazer.dao.entity.dict.MagicTimeUnitDictEntity
import com.gitlab.cvazer.dao.entity.dict.WeaponCategoryDictEntity
import com.gitlab.cvazer.util.Md5Hashable
import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*

@Entity
@DynamicUpdate
@Table(name = "t_spell_casting_info")
class SpellCastingInfoEntity(
    var timeAmount: Int = 1,
    var castCondition: String? = null,
    var verbal: Boolean = false,
    var somatic: Boolean = false,
    var material: Boolean = false,
    var materialCost: Int? = null,
    var materialConsume: Boolean = false,
    var ritual: Boolean = false,

    @Column(columnDefinition = "TEXT") var materialFlavor: String? = null,

    @Id
    @Column(name = "spell_id")
    var id: Long? = null,

    @MapsId
    @OneToOne
    @JsonIgnore
    @JoinColumn(name = "spell_id", referencedColumnName = "id")
    var spell: SpellEntity? = null,

    @ManyToOne(cascade = [CascadeType.REFRESH], optional = false)
    @JoinColumn(name = "time_unit_id", referencedColumnName = "id")
    var timeUnit: MagicTimeUnitDictEntity? = null,
)