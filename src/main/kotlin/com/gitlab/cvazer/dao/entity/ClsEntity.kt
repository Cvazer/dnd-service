package com.gitlab.cvazer.dao.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.dict.BaseArmorDictEntity
import com.gitlab.cvazer.dao.entity.dict.BaseWeaponDictEntity
import com.gitlab.cvazer.dao.entity.dict.SourceDictEntity
import com.gitlab.cvazer.dao.entity.dict.StatDictEntity
import com.gitlab.cvazer.dao.entity.link.ClsTraitLinkEntity
import com.gitlab.cvazer.dao.repo.generic.BaseArmorDictDao
import com.gitlab.cvazer.dao.repo.generic.BaseWeaponDictDao
import com.gitlab.cvazer.dao.repo.generic.StatDictDao
import com.gitlab.cvazer.model.meta.MultiClassing
import com.gitlab.cvazer.model.meta.SkillProf
import com.gitlab.cvazer.model.meta.ToolsProf
import com.gitlab.cvazer.util.getBean
import com.gitlab.cvazer.util.jsonString
import com.gitlab.cvazer.util.readAs
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_cls")
data class ClsEntity(
        var name: String,
        var shortName: String? = null,
        var sourcePage: Int = 0,
        var hd: String = "d8",
        var subclassTitle: String = "Subclass",
        var srd: Boolean = false,
        @Column(name = "armor_prof", columnDefinition = "JSON") private var _armorProf: String = "[]",
        @Column(name = "weapon_prof", columnDefinition = "JSON") private var _weaponProf: String = "[]",
        @Column(name = "tools_prof", columnDefinition = "JSON") private var _toolsProf: String = "{}",
        @Column(name = "skills_prof", columnDefinition = "JSON") private var _skillsProf: String = "{}",
        @Column(name = "saves", columnDefinition = "JSON") private var _saves: String = "[]",
        @Column(columnDefinition = "TEXT") var classTable: String = "",
        @Column(name = "multi_classing", columnDefinition = "JSON") private var _multiClassing: String = "{}",
        @Column(name = "meta", columnDefinition = "JSON") private var _meta: String = "{}",

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,

        @ManyToOne
        @JsonIgnore
        @JoinColumn(name = "parent_id", referencedColumnName = "id")
        var parent: ClsEntity?,

        @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, mappedBy = "parent")
        var subclasses: MutableList<ClsEntity>,

        @ManyToOne(cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name = "source_id", referencedColumnName = "id")
        var source: SourceDictEntity = SourceDictEntity("NONE", "Нет")
) {

        @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, mappedBy = "cls")
        var traits: MutableList<ClsTraitLinkEntity> = mutableListOf()

        var armorProf: List<BaseArmorDictEntity>
                get() = getBean<BaseArmorDictDao>().findAllById(_armorProf.readAs())
                set(value) { _armorProf = value.map { it.id }.jsonString() }

        var weaponProf: List<BaseWeaponDictEntity>
                get() = getBean<BaseWeaponDictDao>().findAllById(_weaponProf.readAs())
                set(value) { _weaponProf = value.map { it.id }.jsonString() }

        var toolsProf: ToolsProf
                get() = _toolsProf.readAs()
                set(value) { _toolsProf = value.jsonString() }

        var skillsProf: SkillProf
                get() = _skillsProf.readAs()
                set(value) { _skillsProf = value.jsonString() }

        var saves: List<StatDictEntity>
                get() = getBean<StatDictDao>().findAllById(_saves.readAs())
                        .map { it.copy(skills = null) }
                set(value) { _saves = value.map { it.id }.jsonString() }

        var multiClassing: MultiClassing
                get() = _multiClassing.readAs()
                set(value) {_multiClassing = value.jsonString()}

        var meta: Map<String, Any>
                get() = _meta.readAs()
                set(value) { _meta = value.jsonString() }
}