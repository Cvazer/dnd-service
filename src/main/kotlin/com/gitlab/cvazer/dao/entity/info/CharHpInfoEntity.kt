package com.gitlab.cvazer.dao.entity.info

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.CharEntity
import com.gitlab.cvazer.model.character.CharHitDie
import com.gitlab.cvazer.util.jsonString
import com.gitlab.cvazer.util.readAs
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_char_hp_info")
data class CharHpInfoEntity (
        var hpCurrent: Int = 0,
        var hpTmpCurrent: Int = 0,
        var hpMax: Int = 0,
        @Column(name = "hd", columnDefinition = "JSON") private var _hd: String = "[]",

        @Id
        @Column(name = "char_id")
        private var _charId: Long? = null,

        @MapsId
        @OneToOne
        @JsonIgnore
        @JoinColumn(name = "char_id", referencedColumnName = "id")
        var char: CharEntity? = null
) {
        var hd: List<CharHitDie>
                get() = _hd.readAs()
                set(value) { _hd = value.jsonString() }
}