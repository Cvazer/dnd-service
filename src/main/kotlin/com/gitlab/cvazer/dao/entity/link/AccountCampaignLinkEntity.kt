package com.gitlab.cvazer.dao.entity.link

import com.gitlab.cvazer.dao.entity.CampaignEntity
import com.gitlab.cvazer.dao.entity.CharEntity
import com.gitlab.cvazer.dao.entity.link.identity.AccountCampaignLinkIdentity
import com.gitlab.cvazer.dao.entity.system.AccountEntity
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_account_campaign_link")
data class AccountCampaignLinkEntity(
        @EmbeddedId val identity: AccountCampaignLinkIdentity = AccountCampaignLinkIdentity(),
        var role: String,

        @ManyToOne
        @JoinColumn(name = "active_char_id", referencedColumnName = "id", nullable = true)
        var activeChar: CharEntity?,

        @ManyToOne
        @MapsId("accountKey")
        @JoinColumn(name = "account_key", referencedColumnName = "account_key")
        val account: AccountEntity,

        @ManyToOne
        @MapsId("campaignId")
        @JoinColumn(name = "campaign_id", referencedColumnName = "id")
        val campaign: CampaignEntity
)