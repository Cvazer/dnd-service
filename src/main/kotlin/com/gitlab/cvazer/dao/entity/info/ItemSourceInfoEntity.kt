package com.gitlab.cvazer.dao.entity.info

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.ItemEntity
import com.gitlab.cvazer.dao.entity.dict.SourceDictEntity
import com.gitlab.cvazer.model.meta.SourceEntry
import com.gitlab.cvazer.util.jsonString
import com.gitlab.cvazer.util.readAs
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_item_source_info")
data class ItemSourceInfoEntity(
        var page: Int = 0,

        @Id
        @Column(name = "item_id")
        var id: Long? = null,

        @MapsId
        @OneToOne
        @JsonIgnore
        @JoinColumn(name = "item_id", referencedColumnName = "id")
        var item: ItemEntity? = null,

        @ManyToOne(cascade = [CascadeType.REFRESH])
        @JoinColumn(name = "source_id", referencedColumnName = "id")
        var source: SourceDictEntity? = null,

        @Column(name = "additional", columnDefinition = "JSON")
        private var _additional: String = "[]"
) {
    var additional: List<SourceEntry>
        get() = _additional.readAs()
        set(value) { _additional = value.jsonString() }
}