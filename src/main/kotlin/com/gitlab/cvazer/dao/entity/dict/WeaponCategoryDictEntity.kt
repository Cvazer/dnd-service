package com.gitlab.cvazer.dao.entity.dict

import javax.persistence.Entity
import javax.persistence.Table

import org.hibernate.annotations.DynamicInsert

@Entity
@DynamicInsert
@Table(name = "t_weapon_category_dict")
data class WeaponCategoryDictEntity(
        override var id: String,
        override var name: String
) : BaseDictEntity(id, name)