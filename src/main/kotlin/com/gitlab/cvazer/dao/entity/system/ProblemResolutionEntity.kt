package com.gitlab.cvazer.dao.entity.system

import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_resource_problem_resolution")
data class ProblemResolutionEntity (
    val problem_id: String,
    @Column(columnDefinition = "TEXT") var meta: String? = "{}",

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
)