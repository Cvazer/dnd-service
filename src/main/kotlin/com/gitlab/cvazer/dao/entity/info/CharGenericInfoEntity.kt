package com.gitlab.cvazer.dao.entity.info

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.CharEntity
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_char_generic_info")
data class CharGenericInfoEntity (
        var exp: Long = 0,
        var profBonus: Int = 2,
        var ac: Int = 10,
        var initiative: Int = 0,
        var speed: Int = 30,
        var lvl: Int = 1,

        @Id
        @Column(name = "char_id")
        private var _charId: Long? = null,

        @MapsId
        @OneToOne
        @JsonIgnore
        @JoinColumn(name = "char_id", referencedColumnName = "id")
        var char: CharEntity? = null
)