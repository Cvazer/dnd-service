package com.gitlab.cvazer.dao.entity.dict

import javax.persistence.Entity
import javax.persistence.Table

import org.hibernate.annotations.DynamicInsert

@Entity
@DynamicInsert
@Table(name = "t_lang_dict")
data class LangDictEntity(
        override var id: String,
        override var name: String,
        var type: String
) : BaseDictEntity(id, name)