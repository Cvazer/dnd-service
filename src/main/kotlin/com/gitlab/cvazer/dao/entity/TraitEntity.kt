package com.gitlab.cvazer.dao.entity

import com.gitlab.cvazer.dao.entity.dict.SourceDictEntity
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_trait")
data class TraitEntity (
        var name: String,
        var sourcePage: Int = 0,
        var srd: Boolean = false,

        @Column(columnDefinition = "TEXT") var descr: String? = null,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,

        @ManyToOne(cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name = "source_id", referencedColumnName = "id")
        var source: SourceDictEntity = SourceDictEntity("NONE", "Нет")
)