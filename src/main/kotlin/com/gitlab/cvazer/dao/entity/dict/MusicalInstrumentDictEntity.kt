package com.gitlab.cvazer.dao.entity.dict

import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "v_musical_instrument_dict")
data class MusicalInstrumentDictEntity(
        override var id: String,
        override var name: String
) : BaseDictEntity(id, name)