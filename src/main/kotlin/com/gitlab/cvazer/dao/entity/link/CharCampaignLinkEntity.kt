package com.gitlab.cvazer.dao.entity.link

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.CampaignEntity
import com.gitlab.cvazer.dao.entity.CharEntity
import com.gitlab.cvazer.dao.entity.ClsEntity
import com.gitlab.cvazer.dao.entity.link.identity.CharCampaignLinkIdentity
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_char_campaign_link")
data class CharCampaignLinkEntity(
        @EmbeddedId @JsonIgnore val identity: CharCampaignLinkIdentity = CharCampaignLinkIdentity(),

        @ManyToOne
        @JsonIgnore
        @MapsId("charId")
        @JoinColumn(name = "char_id", referencedColumnName = "id")
        val char: CharEntity,

        @ManyToOne
        @JsonIgnore
        @MapsId("campaignId")
        @JoinColumn(name = "campaign_id", referencedColumnName = "id")
        val campaign: CampaignEntity
)