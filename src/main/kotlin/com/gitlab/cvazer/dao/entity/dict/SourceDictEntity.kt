package com.gitlab.cvazer.dao.entity.dict

import javax.persistence.*

import org.hibernate.annotations.DynamicInsert

@Entity
@DynamicInsert
@Table(name = "t_source_dict")
data class SourceDictEntity(
        override var id: String,
        override var name: String
) : BaseDictEntity(id, name)