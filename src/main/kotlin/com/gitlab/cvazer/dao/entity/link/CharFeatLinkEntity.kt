package com.gitlab.cvazer.dao.entity.link

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.CharEntity
import com.gitlab.cvazer.dao.entity.FeatEntity
import com.gitlab.cvazer.dao.entity.link.identity.CharFeatLinkIdentity
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_char_feat_link")
data class CharFeatLinkEntity(
        @EmbeddedId @JsonIgnore val identity: CharFeatLinkIdentity,

        @ManyToOne
        @JsonIgnore
        @MapsId("charId")
        @JoinColumn(name = "char_id", referencedColumnName = "id")
        val char: CharEntity,

        @MapsId("featId")
        @OneToOne(cascade = [CascadeType.REFRESH])
        @JoinColumn(name = "feat_id", referencedColumnName = "id")
        val feat: FeatEntity
)