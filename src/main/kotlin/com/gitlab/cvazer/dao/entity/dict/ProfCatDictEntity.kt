package com.gitlab.cvazer.dao.entity.dict

import javax.persistence.Entity
import javax.persistence.Table

import org.hibernate.annotations.DynamicInsert

@Entity
@DynamicInsert
@Table(name = "t_prof_cat_dict")
data class ProfCatDictEntity(
        override var id: String,
        override var name: String,
        var nameEng: String
) : BaseDictEntity(id, name)