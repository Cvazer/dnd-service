package com.gitlab.cvazer.dao.entity.link.identity

import java.io.Serializable
import javax.persistence.*

@Embeddable
data class ClsTraitLinkIdentity(
        @Column(insertable = false, updatable = false) var clsId: Long?,
        @Column(insertable = false, updatable = false) var traitId: Long?
) : Serializable