package com.gitlab.cvazer.dao.entity.dict

import org.hibernate.annotations.DynamicInsert
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@DynamicInsert
@Table(name = "t_stat_dict")
data class StatDictEntity(
        override var id: String,
        override var name: String,
        var name_eng: String,

        @OneToMany(mappedBy = "stat", cascade = [CascadeType.ALL], orphanRemoval = true)
        var skills: MutableList<SkillDictEntity>? = mutableListOf()
) : BaseDictEntity(id, name)