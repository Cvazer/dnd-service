package com.gitlab.cvazer.dao.entity.link.identity

import java.io.Serializable
import javax.persistence.*

@Embeddable
data class CharClsLinkIdentity(
        @Column(insertable = false, updatable = false) var charId: Long? = null,
        @Column(insertable = false, updatable = false) var clsId: Long? = null
) : Serializable