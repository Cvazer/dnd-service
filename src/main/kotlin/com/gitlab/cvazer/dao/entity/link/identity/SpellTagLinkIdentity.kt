package com.gitlab.cvazer.dao.entity.link.identity

import java.io.Serializable
import javax.persistence.*

@Embeddable
data class SpellTagLinkIdentity(
        @Column(insertable = false, updatable = false) var spellId: Long? = null,
        @Column(insertable = false, updatable = false) var tagId: String? = null,
        @Column(insertable = false, updatable = false) var typeId: String? = null
) : Serializable {
        override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (other !is SpellTagLinkIdentity) return false

                if (spellId != other.spellId) return false
                if (tagId != other.tagId) return false
                if (typeId != other.typeId) return false

                return true
        }

        override fun hashCode(): Int {
                var result = spellId?.hashCode() ?: 0
                result = 31 * result + (tagId?.hashCode() ?: 0)
                result = 31 * result + (typeId?.hashCode() ?: 0)
                return result
        }
}