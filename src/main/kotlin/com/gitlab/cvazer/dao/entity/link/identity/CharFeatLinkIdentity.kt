package com.gitlab.cvazer.dao.entity.link.identity

import java.io.Serializable
import javax.persistence.*

@Embeddable
data class CharFeatLinkIdentity(
        @Column(insertable = false, updatable = false) var charId: Long?,
        @Column(insertable = false, updatable = false) var featId: Long?
) : Serializable