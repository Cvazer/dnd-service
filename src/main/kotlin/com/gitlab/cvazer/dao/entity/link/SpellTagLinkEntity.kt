package com.gitlab.cvazer.dao.entity.link

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.SpellEntity
import com.gitlab.cvazer.dao.entity.dict.SpellTagLinkTypeDictEntity
import com.gitlab.cvazer.dao.entity.link.identity.SpellTagLinkIdentity
import com.gitlab.cvazer.dao.entity.system.TagEntity
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_spell_tag_link")
class SpellTagLinkEntity (
    @EmbeddedId @JsonIgnore val identity: SpellTagLinkIdentity = SpellTagLinkIdentity(),

    @JsonIgnore
    @MapsId("spellId")
    @ManyToOne(optional = false)
    @JoinColumn(name = "spell_id", referencedColumnName = "id")
    val spell: SpellEntity,

    @MapsId("tagId")
    @ManyToOne(optional = false)
    @JoinColumn(name = "tag_id", referencedColumnName = "id")
    val tag: TagEntity,

    @MapsId("typeId")
    @ManyToOne(optional = false)
    @JoinColumn(name = "type_id", referencedColumnName = "id")
    val type: SpellTagLinkTypeDictEntity


)