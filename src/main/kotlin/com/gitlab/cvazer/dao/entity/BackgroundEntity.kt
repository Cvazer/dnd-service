package com.gitlab.cvazer.dao.entity

import com.gitlab.cvazer.dao.entity.dict.SourceDictEntity
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_background")
data class BackgroundEntity (
        var name: String,
        var sourcePage: Int = 0,
        @Column(name = "descr", columnDefinition = "TEXT") val descr: String = "[]",
        @Column(name = "meta", columnDefinition = "JSON") val meta: String = "{}",

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,

        @OneToOne(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name = "trait_id", referencedColumnName = "id")
        val trait: TraitEntity,

        @ManyToOne(cascade = [CascadeType.REFRESH], fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name = "source_id", referencedColumnName = "id")
        var source: SourceDictEntity = SourceDictEntity("NONE", "Нет")
)