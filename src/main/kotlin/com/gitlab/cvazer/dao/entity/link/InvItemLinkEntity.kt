package com.gitlab.cvazer.dao.entity.link

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dao.entity.InventoryEntity
import com.gitlab.cvazer.dao.entity.ItemEntity
import org.hibernate.annotations.DynamicInsert
import javax.persistence.*

@Entity
@DynamicInsert
@Table(name = "t_inv_item_link")
data class InvItemLinkEntity(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,

        var count: Int = 1,
        var alias: String?,

        @ManyToOne(cascade = [CascadeType.REFRESH])
        @JsonIgnore
        @JoinColumn(name = "inv_id", referencedColumnName = "id")
        var inventory: InventoryEntity,


        @OneToOne(cascade = [CascadeType.REFRESH])
        @JoinColumn(name = "item_id", referencedColumnName = "id")
        val item: ItemEntity,

        @Column(name = "comm") var comment: String? = null
)