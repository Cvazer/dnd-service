package com.gitlab.cvazer.dao.repo

import com.gitlab.cvazer.dao.entity.GlossaryRecordEntity
import com.gitlab.cvazer.system.proxy.RemoteProxy
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
@RemoteProxy("glossaryRecordDao")
interface GlossaryRecordDao: JpaRepository<GlossaryRecordEntity, Long>