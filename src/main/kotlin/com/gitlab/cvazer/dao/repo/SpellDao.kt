package com.gitlab.cvazer.dao.repo

import com.gitlab.cvazer.dao.entity.SpellEntity
import com.gitlab.cvazer.system.proxy.RemoteProxy
import org.intellij.lang.annotations.Language
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

@RemoteProxy("spellDao")
interface SpellDao: JpaRepository<SpellEntity, Long> {

    @Query("select distinct e.tag.id as id, e.tag.name as name, e.type.id as typeId, e.type.name as type from SpellTagLinkEntity e")
    fun getAllSpellTags(): List<SpellTag>

    fun findByName(name: String): SpellEntity

    interface SpellTag {
        fun getId(): String
        fun getName(): String
        fun getTypeId(): String
        fun getType(): String
    }
}