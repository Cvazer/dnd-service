package com.gitlab.cvazer.dao.repo.system

import com.gitlab.cvazer.dao.entity.system.AccountEventEntity
import com.gitlab.cvazer.system.proxy.RemoteProxy
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository

@RemoteProxy("accountEventDao")
interface AccountEventDao: JpaRepository<AccountEventEntity, Long> {

    fun findAllByAccount_Key(key: String, pageable: Pageable): Page<AccountEventEntity>

    fun countAllBySeenFalseAndAccount_Key(key: String): Int
}