package com.gitlab.cvazer.dao.repo

import com.gitlab.cvazer.dao.entity.ClsEntity
import com.gitlab.cvazer.system.proxy.RemoteProxy
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

@RemoteProxy("clsDao")
interface ClsDao: JpaRepository<ClsEntity, Long> {

    @Query(value = "select e from ClsEntity e where e.parent is null")
    fun findAllCls(): List<ClsEntity>

}