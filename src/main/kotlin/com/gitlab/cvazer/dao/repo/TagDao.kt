package com.gitlab.cvazer.dao.repo

import com.gitlab.cvazer.dao.entity.system.TagEntity
import org.springframework.data.jpa.repository.JpaRepository

interface TagDao: JpaRepository<TagEntity, String>