package com.gitlab.cvazer.dao.repo.link

import com.gitlab.cvazer.dao.entity.link.InvItemLinkEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface InvItemLinkDao: JpaRepository<InvItemLinkEntity, Long>