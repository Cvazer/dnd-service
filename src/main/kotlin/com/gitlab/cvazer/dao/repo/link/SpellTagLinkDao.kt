package com.gitlab.cvazer.dao.repo.link

import com.gitlab.cvazer.dao.entity.link.SpellTagLinkEntity
import com.gitlab.cvazer.dao.entity.link.identity.SpellTagLinkIdentity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface SpellTagLinkDao: JpaRepository<SpellTagLinkEntity, SpellTagLinkIdentity>