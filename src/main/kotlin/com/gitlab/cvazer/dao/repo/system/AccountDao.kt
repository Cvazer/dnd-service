package com.gitlab.cvazer.dao.repo.system

import com.gitlab.cvazer.dao.entity.system.AccountEntity
import com.gitlab.cvazer.system.proxy.RemoteProxy
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
@RemoteProxy(name = " ")
interface AccountDao: JpaRepository<AccountEntity, String> {
    fun findByKey(key: String): AccountEntity?

    @Query(value = "select e from AccountEntity e where e.key = :key")
    fun findByKeyCaseSensitive(@Param("key") key: String): AccountEntity?

    @Query(value = """
        select * from t_account where account_key in (select account_key from t_account_char_link where char_id = :id)
    """, nativeQuery = true)
    fun findByCharId(@Param("id") id: Long): List<AccountEntity>
}