package com.gitlab.cvazer.dao.repo.link

import com.gitlab.cvazer.dao.entity.link.AccountCampaignLinkEntity
import com.gitlab.cvazer.dao.entity.link.identity.AccountCampaignLinkIdentity
import com.gitlab.cvazer.system.proxy.RemoteProxy
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

@RemoteProxy("accountCampaignLinkDao")
interface AccountCampaignLinkDao: JpaRepository<
        AccountCampaignLinkEntity,
        AccountCampaignLinkIdentity
        >
{
    fun getAllByIdentity_AccountKey(key: String): List<AccountCampaignLinkEntity>

    fun getByIdentity_AccountKeyAndIdentity_CampaignId(key: String, id: Long): AccountCampaignLinkEntity?

    fun getByIdentity_CampaignIdAndActiveChar_Id(campaignId: Long, charId: Long): AccountCampaignLinkEntity?

    fun getByActiveChar_Id(id: Long): List<AccountCampaignLinkEntity>

    fun findAllByCampaign_Id(id: Long): List<AccountCampaignLinkEntity>

    @Query(value = """
        select * from t_account_campaign_link where campaign_id in 
        (select campaign_id from t_account_campaign_link where account_key = :key)
    """, nativeQuery = true)
    fun getAllLinksForAccountCampaigns(@Param("key") key: String): List<AccountCampaignLinkEntity>
}