package com.gitlab.cvazer.dao.repo

import com.gitlab.cvazer.dao.entity.RaceEntity
import com.gitlab.cvazer.system.proxy.RemoteProxy
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
@RemoteProxy("raceDao")
interface RaceDao : JpaRepository<RaceEntity, Long> {

    @Query(value = "select e from RaceEntity e where e.parent is null and e.npc = false")
    fun findAllBase(): List<RaceEntity>

}