package com.gitlab.cvazer.dao.repo.generic

import com.gitlab.cvazer.dao.entity.dict.*
import com.gitlab.cvazer.system.proxy.RemoteProxy
import org.springframework.data.jpa.repository.JpaRepository

@RemoteProxy(name = "sourceDictDao") interface SourceDictDao: JpaRepository<SourceDictEntity, String>
@RemoteProxy(name = "itemTypeDictDao") interface ItemTypeDictDao: JpaRepository<ItemTypeDictEntity, String>
@RemoteProxy(name = "weaponPropsDictDao") interface WeaponPropsDictDao: JpaRepository<WeaponPropsDictEntity, String>
@RemoteProxy(name = "scfTypeDictDao") interface ScfTypeDictDao: JpaRepository<ScfTypeDictEntity, String>
@RemoteProxy(name = "rarityDictDao") interface RarityDictDao: JpaRepository<RarityDictEntity, String>
@RemoteProxy(name = "statDictDao") interface StatDictDao: JpaRepository<StatDictEntity, String>
@RemoteProxy(name = "skillDictDao") interface SkillDictDao: JpaRepository<SkillDictEntity, String>
@RemoteProxy(name = "baseWeaponDictDao") interface BaseWeaponDictDao: JpaRepository<BaseWeaponDictEntity, String>
@RemoteProxy(name = "baseArmorDictDao") interface BaseArmorDictDao: JpaRepository<BaseArmorDictEntity, String>
@RemoteProxy(name = "artisanToolsDictDao") interface ArtisanToolsDictDao: JpaRepository<ArtisanToolsDictEntity, String>
@RemoteProxy(name = "musicalInstrumentDictDao") interface MusicalInstrumentDictDao: JpaRepository<MusicalInstrumentDictEntity, String>
@RemoteProxy(name = "gamingSetDictDao") interface GamingSetDictDao: JpaRepository<GamingSetDictEntity, String>
@RemoteProxy(name = "toolsDictDao") interface ToolsDictDao: JpaRepository<ToolsDictEntity, String>
@RemoteProxy(name = "damageTypeDictDao") interface DamageTypeDictDao: JpaRepository<DamageTypeDictEntity, String>
@RemoteProxy(name = "profCatDictDao") interface ProfCatDictDao: JpaRepository<ProfCatDictEntity, String>
@RemoteProxy(name = "langDictDao") interface LangDictDao: JpaRepository<LangDictEntity, String>
@RemoteProxy(name = "magicRechargeDictDao") interface MagicRechargeDictDao: JpaRepository<MagicRechargeDictEntity, String>
@RemoteProxy(name = "ageDictDao") interface AgeDictDao: JpaRepository<AgeDictEntity, String>
@RemoteProxy(name = "sizeDictDao") interface SizeDictDao: JpaRepository<SizeDictEntity, String>
@RemoteProxy(name = "vehicleTypeDictDao") interface VehicleTypeDictDao: JpaRepository<VehicleTypeDictEntity, String>
@RemoteProxy(name = "weaponCategoryDictDao") interface WeaponCategoryDictDao: JpaRepository<WeaponCategoryDictEntity, String>
@RemoteProxy(name = "weaponDistanceDictDao") interface WeaponDistanceDictDao: JpaRepository<WeaponDistanceDictEntity, String>
@RemoteProxy(name = "gameEventTypeDictDao") interface GameEventTypeDictDao: JpaRepository<GameEventTypeDictEntity, String>
@RemoteProxy(name = "scrappingResourceTypeDictDao") interface ScrappingResourceTypeDictDao: JpaRepository<ScrappingResourceTypeDictEntity, String>
@RemoteProxy(name = "magicSchoolDictDao") interface MagicSchoolDictDao: JpaRepository<MagicSchoolDictEntity, String>
@RemoteProxy(name = "magicDistanceTypeDictDao") interface MagicDistanceTypeDictDao: JpaRepository<MagicDistanceTypeDictEntity, String>
@RemoteProxy(name = "magicDurationTypeDictDao") interface MagicDurationTypeDictDao: JpaRepository<MagicDurationTypeDictEntity, String>
@RemoteProxy(name = "magicRangeTypeDictDao") interface MagicRangeTypeDictDao: JpaRepository<MagicRangeTypeDictEntity, String>
@RemoteProxy(name = "magicTimeUnitDictDao") interface MagicTimeUnitDictDao: JpaRepository<MagicTimeUnitDictEntity, String>
@RemoteProxy(name = "spellTagLinkTypeDictDao") interface SpellTagLinkTypeDictDao: JpaRepository<SpellTagLinkTypeDictEntity, String>
@RemoteProxy(name = "tagDomainDictDao") interface TagDomainDictDao: JpaRepository<TagDomainDictEntity, String>
@RemoteProxy(name = "accountEventTypeDictDao") interface AccountEventTypeDictDao: JpaRepository<AccountEventTypeDictEntity, String>