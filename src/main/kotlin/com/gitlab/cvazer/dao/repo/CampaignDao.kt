package com.gitlab.cvazer.dao.repo

import com.gitlab.cvazer.dao.entity.CampaignEntity
import com.gitlab.cvazer.system.proxy.RemoteProxy
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
@RemoteProxy("campaignDao")
interface CampaignDao: JpaRepository<CampaignEntity, Long> {

    @Query(value = """
        select tc.* from t_campaign tc 
        join t_account_campaign_link tacl on tc.id = tacl.campaign_id 
        where tacl.account_key = :key
    """, nativeQuery = true)
    fun getAllByAccount(@Param("key") key: String): List<CampaignEntity>

    @Query(value = """
        select role from t_account_campaign_link 
        where account_key = :key and campaign_id = :id
    """, nativeQuery = true)
    fun getRoleForKeyAndCampaign(
        @Param("key") key: String,
        @Param("id") id: Long
    ): String?

    @Query(value = """
        select tc.* from t_campaign tc
        join t_char_campaign_link tccl on tc.id = tccl.campaign_id
        where tccl.char_id = :id
    """, nativeQuery = true)
    fun getCampaignsByCharId(@Param("id") id: Long): List<CampaignEntity>
}