package com.gitlab.cvazer.dao.repo.generic

import com.gitlab.cvazer.dao.entity.link.AccountCharLinkEntity
import com.gitlab.cvazer.dao.entity.link.identity.AccountCharLinkIdentity
import com.gitlab.cvazer.dao.entity.misc.ItemGenerationIndexEntity
import com.gitlab.cvazer.dao.entity.misc.ItemRawDescr
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

interface ItemRawDescrDao: JpaRepository<ItemRawDescr, Long>

interface ItemGenerationIndexDao: JpaRepository<ItemGenerationIndexEntity, Long> {
    @Query("from ItemGenerationIndexEntity e where e.sourceId = :sId and e.genericId = :gId")
    fun findBySourceIdAndAndGenericId(
            @Param("sId") sourceId: Long,
            @Param("gId") genericId: Long): ItemGenerationIndexEntity?
}

//interface AccountCharLinkDao: JpaRepository<AccountCharLinkEntity, AccountCharLinkIdentity> {
//
//    @Query(value = "delete from AccountCharLinkEntity e where e.char.id = :id")
//    fun deleteForChar(@Param("id") id: Long)
//}