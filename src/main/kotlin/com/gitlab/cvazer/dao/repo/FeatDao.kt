package com.gitlab.cvazer.dao.repo

import com.gitlab.cvazer.dao.entity.FeatEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface FeatDao: JpaRepository<FeatEntity, Long>