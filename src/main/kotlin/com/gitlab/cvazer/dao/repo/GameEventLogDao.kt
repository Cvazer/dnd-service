package com.gitlab.cvazer.dao.repo

import com.gitlab.cvazer.dao.entity.GameEventLogEntity
import com.gitlab.cvazer.system.proxy.RemoteProxy
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
@RemoteProxy("gameEventLogDao")
interface GameEventLogDao: JpaRepository<GameEventLogEntity, Long> {

    @Modifying
    @Query(value = """
        update GameEventLogEntity set account = null where account.key = :key
    """)
    fun nullAllEventReferencesForKey(@Param("key") key: String)
}