package com.gitlab.cvazer.dao.repo

import com.gitlab.cvazer.dao.entity.ItemEntity
import com.gitlab.cvazer.system.proxy.RemoteProxy
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

@RemoteProxy("itemDao")
interface ItemDao: JpaRepository<ItemEntity, Long> {
    @Query("from ItemEntity e where e.base = true")
    fun findAllBaseItems() : List<ItemEntity>

    @Query("from ItemEntity e where e.generic = true")
    fun findAllGenericItems() : List<ItemEntity>

    @Query("from ItemEntity e where e.generic = true")
    fun findAllGenericItems(pageable: Pageable) : Page<ItemEntity>
}