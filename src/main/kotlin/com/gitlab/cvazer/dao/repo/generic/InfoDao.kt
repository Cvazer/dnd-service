package com.gitlab.cvazer.dao.repo.generic

import com.gitlab.cvazer.dao.entity.info.*
import org.springframework.data.jpa.repository.JpaRepository

interface ItemArmorInfoDao : JpaRepository<ItemArmorInfoEntity, Long>
interface ItemCatalogueInfoDao : JpaRepository<ItemCatalogueInfoEntity, Long>
interface ItemContainerInfoDao : JpaRepository<ItemContainerInfoEntity, Long>
interface ItemGenericInfoDao : JpaRepository<ItemGenericInfoEntity, Long>
interface ItemMagicInfoDao : JpaRepository<ItemMagicInfoEntity, Long>
interface ItemMiscInfoDao : JpaRepository<ItemMiscInfoEntity, Long>
interface ItemMountInfoDao : JpaRepository<ItemMountInfoEntity, Long>
interface ItemSourceInfoDao : JpaRepository<ItemSourceInfoEntity, Long>
interface ItemVehicleInfoDao : JpaRepository<ItemVehicleInfoEntity, Long>
interface ItemWeaponInfoDao : JpaRepository<ItemWeaponInfoEntity, Long>