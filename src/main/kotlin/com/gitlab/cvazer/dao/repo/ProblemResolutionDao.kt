package com.gitlab.cvazer.dao.repo

import com.gitlab.cvazer.dao.entity.system.ProblemResolutionEntity
import com.gitlab.cvazer.system.proxy.RemoteProxy
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
@RemoteProxy("problemResolutionDao")
interface ProblemResolutionDao: JpaRepository<ProblemResolutionEntity, Long> {

    @Query(value = "select count(e)>0 from ProblemResolutionEntity e where e.problem_id = :id")
    fun existsByProblemId(@Param("id") problemId: String): Boolean
}