package com.gitlab.cvazer.dao.repo

import com.gitlab.cvazer.dao.entity.InventoryEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface InventoryDao: JpaRepository<InventoryEntity, Long> {}