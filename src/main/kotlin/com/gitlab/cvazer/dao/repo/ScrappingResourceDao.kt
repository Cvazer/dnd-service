package com.gitlab.cvazer.dao.repo

import com.gitlab.cvazer.dao.entity.system.ScrappingResourceEntity
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ScrappingResourceDao: JpaRepository<ScrappingResourceEntity, Long> {
    fun findByLocator(locator: String): ScrappingResourceEntity?
    fun findAllByBake(bake: Boolean): List<ScrappingResourceEntity>
    fun findAllByBake(bake: Boolean, pageable: Pageable): Page<ScrappingResourceEntity>
}