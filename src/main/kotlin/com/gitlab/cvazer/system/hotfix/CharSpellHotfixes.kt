package com.gitlab.cvazer.system.hotfix

import com.gitlab.cvazer.dao.repo.CharDao
import com.gitlab.cvazer.dao.repo.SpellDao
import com.gitlab.cvazer.system.proxy.RemoteProxy
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
@RemoteProxy("charSpellHotfixes")
class CharSpellHotfixes(
    private val charDao: CharDao,
    private val spellDao: SpellDao
) {

    @Transactional
    fun fixCharSpellIds() { charDao.findAll().forEach { char ->
        char.miscInfo.magic = char.miscInfo.magic.apply {
            spells.forEach { spell -> spell.id = spellDao.findByName(spell.name).id!!}
        }
    } }

}