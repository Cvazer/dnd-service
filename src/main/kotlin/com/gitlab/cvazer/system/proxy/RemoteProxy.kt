package com.gitlab.cvazer.system.proxy

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class RemoteProxy(val name: String)