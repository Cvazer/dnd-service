package com.gitlab.cvazer.system.proxy.web

data class RemoteProxyEvalRq (
        val proxy: String,
        val expr: String,
        val lang: String,
        val additionalProxies: Map<String, String> = emptyMap(),
        val params: Map<String, Any> = emptyMap(),
        val objects: List<RemoteProxyObject> = emptyList()
){
        data class RemoteProxyObject(
                val name: String,
                val className: String,
                val json: String
        )
}