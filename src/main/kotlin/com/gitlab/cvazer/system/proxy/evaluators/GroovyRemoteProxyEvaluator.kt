package com.gitlab.cvazer.system.proxy.evaluators

import com.gitlab.cvazer.system.proxy.abstract.AbstractRemoteProxyEvaluator
import groovy.lang.Binding
import groovy.lang.GroovyShell
import org.springframework.stereotype.Service

@Service
class GroovyRemoteProxyEvaluator: AbstractRemoteProxyEvaluator<GroovyShell, Binding>() {
    override fun eval(exp: String): (shell: GroovyShell) -> Any? = { it.evaluate(exp) }
    override fun binding(values: Map<String, Any>): Binding = Binding(values)
    override fun shell(binding: Binding): GroovyShell = GroovyShell(binding)
    override fun lang(): String = "GROOVY"
}