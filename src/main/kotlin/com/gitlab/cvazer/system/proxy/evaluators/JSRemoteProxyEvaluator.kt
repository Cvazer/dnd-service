package com.gitlab.cvazer.system.proxy.evaluators

import com.gitlab.cvazer.system.proxy.abstract.AbstractRemoteProxyEvaluator
import org.mozilla.javascript.*
import org.springframework.stereotype.Service

@Service
class JSRemoteProxyEvaluator: AbstractRemoteProxyEvaluator<JSRemoteProxyEvaluator.JsShell, Map<String, Any>>() {
    override fun eval(exp: String): (shell: JsShell) -> Any? = { it.eval(exp) }
    override fun binding(values: Map<String, Any>): Map<String, Any> = values
    override fun shell(binding: Map<String, Any>): JsShell = JsShell(binding)
    override fun lang(): String = "JS"

    class JsShell(private val binding: Map<String, Any>) {
        private val context = Context.enter()
        private val scope: Scriptable = context.initStandardObjects()

        fun eval(exp: String): Any? {
            binding.forEach { (key, value) ->
                Context.javaToJS(value, scope)
                    .also { ScriptableObject.putProperty(scope, key, it) }
            }
            val res = context.evaluateString(scope, exp, "Js proxy rq", 1, null);
            return if (res is Wrapper) res.unwrap() else if (res is Undefined) null else res
        }
    }
}