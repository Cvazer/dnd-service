package com.gitlab.cvazer.system.proxy.impls

import com.gitlab.cvazer.model.icu.ICUPeriod
import com.gitlab.cvazer.model.icu.ICUTimeUnit
import com.gitlab.cvazer.system.proxy.RemoteProxy
import org.springframework.stereotype.Component

@Component
@RemoteProxy("icuPeriodProxy")
class ICUPeriodProxy {
    fun of(seconds: Long) = ICUPeriod.of(seconds)
    fun of(value: Long, unit: String) = ICUPeriod.of(value, ICUTimeUnit.valueOf(unit))
    fun of(src: String) = ICUPeriod.of(src)
}