package com.gitlab.cvazer.system.proxy

import com.gitlab.cvazer.system.proxy.`interface`.RemoteProxy
import com.gitlab.cvazer.system.proxy.abstract.AbstractRemoteProxyEvaluator
import com.gitlab.cvazer.system.proxy.web.RemoteProxyEvalRq
import org.springframework.stereotype.Service

@Service
class RemoteProxyService(
        private val proxyRegistry: ProxyRegistry,
        candidates: List<AbstractRemoteProxyEvaluator<*,*>>
){
    private final val evaluators: MutableMap<String, AbstractRemoteProxyEvaluator<*,*>> = mutableMapOf()
    init {evaluators.putAll(candidates.associateBy { it.lang() })}

    fun eval(proxyName: String, expr: String,
             lang: String, params: Map<String, Any>, additionalProxies: Map<String, String>,
             objects: List<RemoteProxyEvalRq.RemoteProxyObject>? = null): Any? =
             evaluators[lang]?.let { evaluator ->
                 (proxyRegistry[proxyName] ?: throw RuntimeException("no proxy with name [$proxyName]")).let { proxy ->
                     evaluator.eval(
                         if (proxy is RemoteProxy<*>) proxy.ref(params) else proxy, proxyRegistry,
                         expr, params, additionalProxies, objects ?: emptyList())
                 }
             }
}