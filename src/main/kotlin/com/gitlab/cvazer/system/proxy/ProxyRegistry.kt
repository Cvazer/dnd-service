package com.gitlab.cvazer.system.proxy

import com.gitlab.cvazer.Application
import com.gitlab.cvazer.system.proxy.`interface`.RemoteProxy
import org.springframework.core.annotation.AnnotationUtils
import com.gitlab.cvazer.system.proxy.RemoteProxy as RPAnnotation
import org.springframework.stereotype.Component

@Component
class ProxyRegistry(
        candidates: List<RemoteProxy<*>>
){
    val registry: MutableMap<String, Any> = mutableMapOf()

    fun reg(ref: Any, name: String) { registry[name] = ref }
    operator fun get(name: String) = registry[name]

    init {
        Application.context.getBeansWithAnnotation(RPAnnotation::class.java)
                .values.forEach { candidate ->
                    val proxyAnn: RPAnnotation = AnnotationUtils
                            .findAnnotation(candidate::class.java, RPAnnotation::class.java) ?: return@forEach
                    reg(candidate, proxyAnn.name)
                }
    }

    init { candidates.associateBy { it.name() }.toMutableMap().apply { registry.putAll(this) } }
}