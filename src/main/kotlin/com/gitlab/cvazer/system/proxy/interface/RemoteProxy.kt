package com.gitlab.cvazer.system.proxy.`interface`

interface RemoteProxy<T: Any> {
    fun ref(): T = ref(mapOf())
    fun ref(params: Map<String, Any>): T
    fun name(): String
}