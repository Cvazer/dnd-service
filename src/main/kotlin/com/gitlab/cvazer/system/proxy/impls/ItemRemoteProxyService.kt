package com.gitlab.cvazer.system.proxy.impls

import com.gitlab.cvazer.dao.entity.ItemEntity
import com.gitlab.cvazer.dao.repo.InventoryDao
import com.gitlab.cvazer.dao.repo.ItemDao
import com.gitlab.cvazer.dao.repo.generic.ItemTypeDictDao
import com.gitlab.cvazer.system.proxy.RemoteProxy
import com.gitlab.cvazer.util.business.getOrError
import com.gitlab.cvazer.util.unwrap
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@RemoteProxy(name = "itemRemoteProxyService")
class ItemRemoteProxyService(
        private val itemDao: ItemDao,
        private val itemTypeDictDao: ItemTypeDictDao,
        private val inventoryDao: InventoryDao
) {

    fun save(item: ItemEntity) = saveItemTrans(item
            .also { it.vehicleInfo.item = it }
            .also { it.armorInfo.item = it }
            .also { it.catalogueInfo.item = it }
            .also { it.containerInfo.item = it }
            .also { it.genericInfo.item = it }
            .also { it.magicInfo.item = it }
            .also { it.miscInfo.item = it }
            .also { it.mountInfo.item = it }
            .also { it.sourceInfo.item = it }
            .also { it.weaponInfo.item = it }
    )

    @Transactional
    fun saveItemTrans(item: ItemEntity): ItemEntity = itemDao.save(item).unwrap()

    @Transactional
    fun getItemTrans(itemId: Long): ItemEntity = itemDao.getOrError(itemId).unwrap()

    @Transactional
    fun delItemByIdTrans(itemId: Long) {
        inventoryDao.saveAll(inventoryDao.findAll()
                .onEach { it.items.removeIf { item -> item.item.id == itemId } })
        inventoryDao.flush()
        itemDao.deleteById(itemId)
    }

    @Transactional
    fun getBlankItemTrans(): ItemEntity = ItemEntity(
            name = "Новый предмет",
            type = itemTypeDictDao.getOrError("ADVENTURING_GEAR")
    )

    @Transactional
    fun getItemOrNullTrans(itemId: Long): ItemEntity? = itemDao.findByIdOrNull(itemId)?.unwrap()
}