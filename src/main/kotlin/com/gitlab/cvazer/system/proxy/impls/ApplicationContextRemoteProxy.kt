package com.gitlab.cvazer.system.proxy.impls

import com.gitlab.cvazer.Application
import com.gitlab.cvazer.system.proxy.`interface`.RemoteProxy
import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Component

@Component
class ApplicationContextRemoteProxy: RemoteProxy<ApplicationContext> {
    override fun ref(params: Map<String, Any>): ApplicationContext = Application.context
    override fun name(): String = "applicationContext"
}