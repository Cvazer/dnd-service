package com.gitlab.cvazer.system.proxy.impls

import com.gitlab.cvazer.dao.repo.CharDao
import com.gitlab.cvazer.system.proxy.`interface`.RemoteProxy
import org.springframework.stereotype.Component

@Component
class CharacterRemoteProxy(private val charDao: CharDao): RemoteProxy<Any> {
    override fun name(): String = "char"

    override fun ref(params: Map<String, Any>): Any {
        if (!params.containsKey("charId")) throw RuntimeException("charId param must be present")
        return charDao.findById(params["charId"].toString().toLong()).orElseThrow {
            RuntimeException("can't find char with id [${params["charId"]}]")
        }
    }
}