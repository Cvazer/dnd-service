package com.gitlab.cvazer.system.proxy.web

import com.gitlab.cvazer.system.proxy.RemoteProxyService
import com.gitlab.cvazer.util.serviceResponse
import com.gitlab.cvazer.web.comms.ServiceResponse
import com.gitlab.cvazer.system.proxy.web.RemoteProxyEvalRq
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin("*")
@RequestMapping(value = ["/api/proxy"],
        consumes = [MediaType.APPLICATION_JSON_VALUE],
        produces = [MediaType.APPLICATION_JSON_VALUE]
)
class RemoteProxyController(val service: RemoteProxyService) {

    @PostMapping(value = ["/eval/{proxyName}"])
    fun eval(@RequestBody rq: RemoteProxyEvalRq, @PathVariable proxyName: String): ResponseEntity<ServiceResponse<*>> =
            ResponseEntity((service.eval(
                    proxyName = proxyName,
                    expr = rq.expr,
                    lang = rq.lang,
                    params = rq.params,
                    objects = rq.objects,
                    additionalProxies = rq.additionalProxies
            )?:"OK").serviceResponse(), HttpStatus.OK)
}