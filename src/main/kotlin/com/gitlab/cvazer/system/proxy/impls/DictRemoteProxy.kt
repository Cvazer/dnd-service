package com.gitlab.cvazer.system.proxy.impls

import com.gitlab.cvazer.system.proxy.`interface`.RemoteProxy
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import kotlin.reflect.jvm.javaType

@Component
class DictRemoteProxy(private val repos: List<JpaRepository<*,*>>): RemoteProxy<Map<String, Any>> {

    @PersistenceContext
    private lateinit var entityManager: EntityManager

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    override fun ref(params: Map<String, Any>) = repos
            .filter { (it::class.supertypes[1].javaType as Class<*>).simpleName.contains("Dict") }
            .associate {
                Pair((it::class.supertypes[1].javaType as Class<*>).simpleName.replace("Dao", ""), it.findAll())
            }

    override fun name() = "dict"
}