package com.gitlab.cvazer.system.proxy.abstract

import com.gitlab.cvazer.system.proxy.ProxyRegistry
import com.gitlab.cvazer.system.proxy.`interface`.RemoteProxyEvaluator
import com.gitlab.cvazer.util.readAs
import com.gitlab.cvazer.system.proxy.web.RemoteProxyEvalRq

abstract class AbstractRemoteProxyEvaluator<Shell, Binding>: RemoteProxyEvaluator<Shell> {

    fun eval(proxy: Any,
             expr: String,
             params: Map<String, Any>,
             objects: List<RemoteProxyEvalRq.RemoteProxyObject>) = eval(proxy, null, expr, params, emptyMap(), objects)

    fun eval(proxy: Any,
             proxyRegistry: ProxyRegistry?,
             expr: String,
             params: Map<String, Any>,
             additionalProxies: Map<String, String>,
             objects: List<RemoteProxyEvalRq.RemoteProxyObject>) = mutableMapOf(Pair("target", proxy))
            .apply { this.putAll(additionalBindings(params)) }
            .let { bindObjects(objects, it) }
            .let { bindAdditionalProxies(proxyRegistry, additionalProxies, it) }
            .let { binding(it) }
            .let { shell(it) }
            .let { eval(expr).invoke(it) }

    private fun bindObjects(objects: List<RemoteProxyEvalRq.RemoteProxyObject>,
                            map: MutableMap<String, Any>): MutableMap<String, Any> {
        objects.forEach { objDescr -> map[objDescr.name] = objDescr.json.readAs(Class.forName(objDescr.className)) }
        return map
    }

    private fun bindAdditionalProxies(
        proxyRegistry: ProxyRegistry?,
        additionalProxies: Map<String, String>,
        map: MutableMap<String, Any>): MutableMap<String, Any> {
        proxyRegistry?.let { reg -> additionalProxies.forEach { (name, target) -> reg[target]?.let { map[name] = it }} }
        return map
    }

    @Suppress("MemberVisibilityCanBePrivate", "UNUSED_PARAMETER")
    protected fun additionalBindings(params: Map<String, Any>): Map<String, Any> = mapOf()
    protected abstract fun eval(exp: String): (shell: Shell) -> Any?
    protected abstract fun binding(values: Map<String, Any>): Binding
    protected abstract fun shell(binding: Binding): Shell
    abstract fun lang(): String
}