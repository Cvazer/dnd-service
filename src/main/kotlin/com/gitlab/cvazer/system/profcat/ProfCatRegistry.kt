package com.gitlab.cvazer.system.profcat

import com.gitlab.cvazer.dao.repo.generic.*
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

@Service
class ProfCatRegistry(candidates: List<ProfValueSource>) {
    val registry: MutableMap<String, ProfValueSource> = mutableMapOf()
    init { candidates.forEach { registry[it.catId()] = it } }

    operator fun get(value: String) = registry[value]

    @Component class ArmorPVS(dao: BaseArmorDictDao): AbstractProfValueSource("ARMOR", dao)
    @Component class ArtisanToolsPVS(dao: ArtisanToolsDictDao): AbstractProfValueSource("ARTISAN_TOOLS", dao)
    @Component class GamingSetPVS(dao: GamingSetDictDao): AbstractProfValueSource("GAMING_SET", dao)
    @Component class LangPVS(dao: LangDictDao): AbstractProfValueSource("LANGUAGE", dao)
    @Component class MusicalPVS(dao: MusicalInstrumentDictDao): AbstractProfValueSource("MUSICAL_INSTRUMENT", dao)
    @Component class ToolsPVS(dao: ToolsDictDao): AbstractProfValueSource("TOOLS", dao)
    @Component class WeaponPVS(dao: BaseWeaponDictDao): AbstractProfValueSource("WEAPON", dao)
}