package com.gitlab.cvazer.system.profcat

import com.gitlab.cvazer.dao.entity.dict.ProfCatDictEntity

interface ProfValueSource {
    fun catId(): String
    fun cat(): ProfCatDictEntity
    fun <R> get(id: String): R
    fun <R> all(): List<R>
}