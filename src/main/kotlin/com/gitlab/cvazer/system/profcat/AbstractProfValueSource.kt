package com.gitlab.cvazer.system.profcat

import com.gitlab.cvazer.dao.repo.generic.ProfCatDictDao
import com.gitlab.cvazer.util.getBean
import org.springframework.data.jpa.repository.JpaRepository

@Suppress("UNCHECKED_CAST")
abstract class AbstractProfValueSource(
        private val cat: String,
        val dao: JpaRepository<*, String>
    ): ProfValueSource
{
    override fun catId() = cat
    override fun cat() = getBean<ProfCatDictDao>().getById(cat)
    override fun <R> get(id: String) = dao.getById(id) as R
    override fun <R> all(): List<R> = dao.findAll() as List<R>
}