package com.gitlab.cvazer.system.itemgen.abstract

import com.gitlab.cvazer.system.itemgen.interfaces.SpecificItemGenerationService

abstract class AbstractSpecificItemGenerationService<
        Generic, Target, CriteriaDescriptor
        >: SpecificItemGenerationService<Generic, Target> {

    override fun fromGeneric(generic: Generic): List<Target> {
        val criteria = getCriteriaDescriptor(generic)
        return getAllPossibleBases()
                .filter { testBaseCandidate(it, criteria) }
                .map { createSpecificItem(it, generic) }
                .flatten()
    }

    override fun forBase(base: Target): List<Target> {
        return getAllGenerics()
                .filter { testBaseCandidate(base, getCriteriaDescriptor(it)) }
                .map { createSpecificItem(base, it) }
                .flatten()
    }

    internal abstract fun getCriteriaDescriptor(generic: Generic): CriteriaDescriptor
    internal abstract fun getAllPossibleBases(): List<Target>
    internal abstract fun getAllGenerics(): List<Generic>
    internal abstract fun testBaseCandidate(candidate: Target, criteria: CriteriaDescriptor): Boolean
    internal abstract fun createSpecificItem(base: Target, generic: Generic): List<Target>
}