package com.gitlab.cvazer.system.itemgen

import com.gitlab.cvazer.dao.entity.ItemEntity
import com.gitlab.cvazer.dao.entity.info.ItemGenericInfoEntity
import com.gitlab.cvazer.dao.entity.misc.ItemGenerationIndexEntity
import com.gitlab.cvazer.dao.repo.ItemDao
import com.gitlab.cvazer.dao.repo.generic.ItemGenerationIndexDao
import com.gitlab.cvazer.system.itemgen.abstract.AbstractSpecificItemGenerationService
import com.gitlab.cvazer.util.unwrap
import org.hibernate.Hibernate
import org.springframework.expression.spel.standard.SpelExpressionParser
import org.springframework.expression.spel.support.StandardEvaluationContext
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class ItemEntitySpecificItemGenerationService(
        private val itemDao: ItemDao,
        private val itemGenerationIndexDao: ItemGenerationIndexDao
): AbstractSpecificItemGenerationService<ItemEntity, ItemEntity, ItemGenericInfoEntity>() {

    private val spelParser: SpelExpressionParser = SpelExpressionParser()

    override fun createSpecificItem(base: ItemEntity, generic: ItemEntity): List<ItemEntity> {
        val info = generic.genericInfo
        var target = base.copy(id = null, base = false, concrete = true)
        val index: ItemGenerationIndexEntity? = itemGenerationIndexDao
                .findBySourceIdAndAndGenericId(base.id!!, generic.id!!)
        if (index != null) return listOf()

        // eval all expr
        val ctx = StandardEvaluationContext().apply {
            this.setBeanResolver { _, name ->
                when (name) {
                    "generic" -> generic
                    "base" -> base
                    "target" -> target
                    "helper" -> Helper()
                    else -> "null"
                }
            }
        }
        info.inherits
                .map { SpelExpressionParser().parseExpression(it) }
                .forEach { it.getValue(ctx) }


        // set static data and save
        target.name = (info.namePrefix ?: "") + base.name + (info.nameSuffix ?: "")
        target.value = if (info.addedValue == 0L && base.value == null) null else ((base.value ?: 0) + info.addedValue)
        target = itemDao.save(target)

        //generate and save index
        itemGenerationIndexDao.save(
                ItemGenerationIndexEntity(
                        sourceId = base.id!!,
                        genericId = generic.id!!,
                        specificId = target.id!!
                )
        )

        return listOf(target)
    }


    override fun testBaseCandidate(candidate: ItemEntity, criteria: ItemGenericInfoEntity): Boolean {
        return criteria.requires
                .map { spelParser.parseExpression(it).getValue(candidate) }
                .filterIsInstance<Boolean>().any { it } &&
                !criteria.excludeByNames.contains(candidate.name) &&
                !candidate.weaponInfo.props.let { candidate.weaponInfo.props.any {
                    criteria.excludeByProperties.contains(it.id)
                }} &&
                !criteria.excludeByPages.contains(candidate.sourceInfo.page)
    }

    @Transactional
    override fun fromGeneric(generic: ItemEntity): List<ItemEntity> {
        return if (!generic.generic) listOf() else generic
                .apply { Hibernate.initialize(this) }
                .apply { Hibernate.unproxy(this) as ItemEntity }
                .let { super.fromGeneric(it) }
    }

    @Transactional
    override fun forBase(base: ItemEntity): List<ItemEntity> {
        return if (!base.base) listOf() else base
                .apply { Hibernate.initialize(this) }
                .apply { Hibernate.unproxy(this) as ItemEntity }
                .let { super.forBase(base) }
    }

    private class Helper {
        fun getString(str: String?): String { return str ?: "" }
        fun getLong(num: Long?): Long { return num ?: 0 }
    }

    override fun getCriteriaDescriptor(generic: ItemEntity): ItemGenericInfoEntity { return generic.genericInfo }
    override fun getAllGenerics(): List<ItemEntity> { return itemDao.findAllGenericItems() }
    override fun getAllPossibleBases(): List<ItemEntity> { return itemDao.findAllBaseItems().onEach { it.unwrap() } }

}