package com.gitlab.cvazer.system.itemgen.interfaces

interface SpecificItemGenerationService<Input, Output> {
    fun fromGeneric(generic: Input): List<Output>
    fun forBase(base: Output): List<Output>
}