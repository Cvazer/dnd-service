package com.gitlab.cvazer.system.scrapping.abstraction

import com.gitlab.cvazer.system.scrapping.model.ScrappingProblem
import com.gitlab.cvazer.util.etc.ScrapFetchingException
import kotlin.jvm.Throws

interface ResourceProvider<Resource> {
    /**
     * Checks if provider needs initialization and initializes it if it does
     * @return true if already initialized, false otherwise
     */
    fun init(): Boolean

    /**
     * Says whether provider is ready to provide another resource batch
     * @return true if ready for the next batch, false otherwise
     */
    fun isReady(): Boolean

    /**
     * Says whether there is still something to provide
     * @return true if no more resources available, false otherwise
     */
    fun isDone(): Boolean

    /**
     * Provides batch of resources. It may not provide ALL the resources at once
     * @return the list of fetched Resources
     */
    @Throws(ScrapFetchingException::class)
    fun getBatch(): List<Resource>

    fun reset()

    /**
     * Tries to close provider when called
     * @return true if closing is finished, false otherwise
     */
    fun close(): Boolean

    fun status(): String

    fun problems(): List<ScrappingProblem>
}