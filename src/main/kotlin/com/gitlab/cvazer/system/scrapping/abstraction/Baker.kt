package com.gitlab.cvazer.system.scrapping.abstraction

interface Baker {
    fun bake()
}