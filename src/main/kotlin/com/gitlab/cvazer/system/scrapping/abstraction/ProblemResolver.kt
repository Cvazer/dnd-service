package com.gitlab.cvazer.system.scrapping.abstraction

import com.gitlab.cvazer.system.scrapping.model.ScrappingProblem
import com.gitlab.cvazer.util.log

@Suppress("UNCHECKED_CAST")
interface ProblemResolver<ConflictResolution> {

    fun resolve(resolution: ProblemResolution): Boolean = try {
        if (resolution.type == ScrappingProblem.Type.RESOURCE_CONFLICT)
            resolveResourceConflict(resolution as ConflictResolution)
        else false
    } catch (e: Exception) {
        log().error(e.message, e)
        false
    }

    fun resolveResourceConflict(resolution: ConflictResolution): Boolean
    fun name(): String
}