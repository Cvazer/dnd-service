package com.gitlab.cvazer.system.scrapping.abstraction

import com.gitlab.cvazer.system.scrapping.model.ScrapperStatus

interface Scrapper {
    fun scrap(): Boolean
    fun reset()
    fun name(): String
    fun status(): ScrapperStatus
    fun problemResolved(id: String): Boolean
}