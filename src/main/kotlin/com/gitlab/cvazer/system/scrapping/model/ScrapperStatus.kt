package com.gitlab.cvazer.system.scrapping.model

data class ScrapperStatus(
    val status: String,
    val providersStatus: Map<String, String>,
    val problems: MutableList<ScrappingProblem> = mutableListOf()
)