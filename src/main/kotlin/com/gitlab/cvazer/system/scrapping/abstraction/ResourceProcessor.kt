package com.gitlab.cvazer.system.scrapping.abstraction

import com.gitlab.cvazer.util.etc.ScrapProcessingException

interface ResourceProcessor<Resource, Result> {

    @Throws(ScrapProcessingException::class)
    fun process(resource: Resource): Result

    fun isCompatible(resource: Resource): Boolean
}