package com.gitlab.cvazer.system.scrapping.abstraction

import com.gitlab.cvazer.util.etc.ScrapProcessingException
import com.gitlab.cvazer.util.log
import org.springframework.transaction.annotation.Transactional

abstract class AbstractResourceProcessor<Resource: Scrap<*, *>, Result>: ResourceProcessor<Resource, Result> {

    @Transactional
    override fun process(resource: Resource): Result {
        try {
            val res = map(resource) ?: throw ScrapProcessingException("No processing result!")
            if (doPersist(res, resource)) {
                persist(res)
                markAsBaked(resource, res)
            } else {
                log().info("[{}] hash has not changed, skipping persist", resource.locator)
            }
            return res
        } catch (e: Exception) {
            log().error(e.message, e)
            throw ScrapProcessingException(e)
        }
    }

    abstract fun doPersist(res: Result, resource: Resource): Boolean
    abstract fun markAsBaked(resource: Resource, result: Result)
    abstract fun persist(result: Result): Result
    abstract fun map(resource: Resource): Result?
}