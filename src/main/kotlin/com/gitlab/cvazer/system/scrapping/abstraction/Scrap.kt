package com.gitlab.cvazer.system.scrapping.abstraction

interface Scrap<
        Locator: Comparable<Locator>,
        Hash: Comparable<Hash>
        > {
    val locator: Locator
    var rawHash: Hash?
    var bake: Boolean
}