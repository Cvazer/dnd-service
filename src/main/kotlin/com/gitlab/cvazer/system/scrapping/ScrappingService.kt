package com.gitlab.cvazer.system.scrapping

import com.gitlab.cvazer.dao.entity.system.ProblemResolutionEntity
import com.gitlab.cvazer.dao.repo.ProblemResolutionDao
import com.gitlab.cvazer.system.proxy.RemoteProxy
import com.gitlab.cvazer.system.scrapping.abstraction.ProblemResolution
import com.gitlab.cvazer.system.scrapping.abstraction.ProblemResolver
import com.gitlab.cvazer.system.scrapping.abstraction.Scrapper
import com.gitlab.cvazer.util.async
import com.gitlab.cvazer.util.jsonString
import com.gitlab.cvazer.util.log
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap
import java.util.concurrent.Future

@Service
@RemoteProxy("scrappingService")
class ScrappingService(
    private val scrappers: List<Scrapper>,
    private val resolvers: List<ProblemResolver<*>>,
    private val problemResolutionDao: ProblemResolutionDao
) {
    private val jobsMap: ConcurrentMap<Class<*>, Future<*>> = ConcurrentHashMap()

    fun startScrapping(name: String) {
        val scrapper = findScrapper(name)
        @Suppress("NullableBooleanElvis")
        if (jobsMap[scrapper::class.java]?.isDone ?: false) return
        jobsMap[scrapper::class.java] = async {
            @Suppress("ControlFlowWithEmptyBody")
            while (true){
                try {
                    if (!scrapper.scrap()) break
                } catch (e: Exception){
                    log().error(e.message, e)
                    break
                }
            }
            jobsMap.remove(scrapper::class.java)
            return@async
        }
    }

    @Transactional
    fun resolve(resolverName: String, resolution: ProblemResolution) {
        val res = findResolver(resolverName).resolve(resolution)
        if (!res) throw RuntimeException("Can't resolve problem. See server logs")
        problemResolutionDao.save(
            ProblemResolutionEntity(resolution.problemId, resolution.jsonString())
        )
    }

    fun status(name: String) = findScrapper(name).status()
    fun isDone(scrapperClass: Class<*>): Boolean = jobsMap[scrapperClass]?.isDone ?: true
    fun reset(name: String) = findScrapper(name).reset()
    fun listScrappers(): List<String> = scrappers.map { it.name() }

    private fun findScrapper(name: String) = scrappers
        .find { it.name() == name } ?: throw RuntimeException("No scrapper with name $name")

    private fun findResolver(name: String) = resolvers
        .find { it.name() == name } ?: throw RuntimeException("No resolver with name $name")
}