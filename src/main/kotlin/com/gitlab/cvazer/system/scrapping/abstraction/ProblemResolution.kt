package com.gitlab.cvazer.system.scrapping.abstraction

import com.gitlab.cvazer.system.scrapping.model.ScrappingProblem

interface ProblemResolution{
    val problemId: String
    val type: ScrappingProblem.Type
}