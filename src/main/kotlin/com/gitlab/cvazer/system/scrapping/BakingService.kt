package com.gitlab.cvazer.system.scrapping

import com.gitlab.cvazer.dao.repo.ScrappingResourceDao
import com.gitlab.cvazer.system.proxy.RemoteProxy
import com.gitlab.cvazer.system.scrapping.abstraction.Baker
import com.gitlab.cvazer.util.async
import com.gitlab.cvazer.web.comms.paging.PagingRs
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import java.util.concurrent.Future

@Service
@RemoteProxy("bakingService")
class BakingService(
    private val bakers: List<Baker>,
    private val scrappingResourceDao: ScrappingResourceDao,
) {
    private val jobs: MutableMap<Class<*>, Future<Unit>> = mutableMapOf()

    fun bake() {
        if (jobs.values.any { !it.isDone }) return
        bakers.forEach {
            jobs[it::class.java] = async { it.bake() }
        }
    }

    fun status(): String = "${
        jobs.values.asSequence()
            .filter { it.isDone }
            .count()
    }/${jobs.size}"

    fun listRaw(page: Int, size: Int) = scrappingResourceDao
        .findAllByBake(true, PageRequest.of(page, size))
        .toList()
        .let { PagingRs.get(it, page, size) }
}