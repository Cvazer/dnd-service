package com.gitlab.cvazer.system.scrapping.abstraction

import com.gitlab.cvazer.util.log

abstract class AbstractBaker<
        Resource: Scrap<Locator, Hash>,
        Hash: Comparable<Hash>,
        Locator: Comparable<Locator>
        >
(
    private val processors: List<ResourceProcessor<Resource, *>>
): Baker {

    override fun bake() = findAllRaw().forEach { scrap ->
        val result = processors.asSequence()
            .filter { it.isCompatible(scrap) }
            .mapNotNull { runCatching { it.process(scrap) }.getOrElse {
                log().error(it.message, it)
                null
            } }
            .toList()
        if (result.isNotEmpty()) updateResource(scrap.apply { bake = false })
    }

    abstract fun updateResource(resource: Resource)
    abstract fun findAllRaw(): List<Resource>
}