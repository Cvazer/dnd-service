package com.gitlab.cvazer.system.scrapping.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.system.scrapping.abstraction.Scrap
import com.gitlab.cvazer.util.jsonString
import com.gitlab.cvazer.util.readAs
import java.util.UUID

data class ScrappingProblem(
    val level: Level = Level.INFO,
    val type: Type = Type.GENERAL,
    val descr: String,
    val id: String = UUID.randomUUID().toString(),
) {
    @JsonIgnore private var _meta: String = "{}"
    var meta: MutableMap<String, Any>
        get() = _meta.readAs()
        set(value) { _meta = value.jsonString() }

    @JsonIgnore
    fun meta(block: () -> MutableMap<String, Any>): ScrappingProblem {
        meta = block.invoke()
        return this
    }

    enum class Level {
        WARNING, ERROR, INFO
    }

    enum class Type {
        GENERAL, RESOURCE_CONFLICT, RESOURCE_NOT_UPDATED, SCRAPPED
    }

    companion object {

        @JvmStatic fun <L, T: Scrap<L, *>> resourceAlreadyExists(
            new: T,
            persisted: T,
            id: String,
        ): ScrappingProblem = ScrappingProblem(
            Level.ERROR,
            Type.RESOURCE_CONFLICT,
            "Resource with locator [${new.locator}] already exists",
            id = "$id|RESOURCE_CONFLICT"
        ).meta { mutableMapOf(
            Pair("new", new),
            Pair("persisted", persisted)
        ) }

        @JvmStatic fun<L, T: Scrap<L, *>> resourceNotUpdated(
            resource: T,
            id: String
        ): ScrappingProblem = ScrappingProblem(
            Level.WARNING,
            Type.RESOURCE_NOT_UPDATED,
            "Resource with locator [${resource.locator}] was scrapped " +
                    "but not updated due to it being already persisted," +
                    " and no changes being detected",
            id = "$id|RESOURCE_NOT_UPDATED"
        ).meta { mutableMapOf(
            Pair("resource", resource)
        ) }

        @JvmStatic fun<L, T: Scrap<L, *>> resourceAdded(
            resource: T,
            id: String
        ): ScrappingProblem = ScrappingProblem(
            Level.INFO,
            Type.SCRAPPED,
            "Resource with locator [${resource.locator}] was scrapped",
            id = "$id|SCRAPPED"
        ).meta { mutableMapOf(
            Pair("resource", resource)
        ) }

    }
}