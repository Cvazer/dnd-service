package com.gitlab.cvazer.system.scrapping.model

import com.gitlab.cvazer.dao.entity.system.ScrappingResourceEntity
import com.gitlab.cvazer.system.scrapping.abstraction.ProblemResolution

class ResourceConflictProblemResolution(
    override val problemId: String,
    override val type: ScrappingProblem.Type,
    val resolution: Resolution,
    val newRes: ScrappingResourceEntity? = null,
    val persisted: ScrappingResourceEntity? = null,
    val consensus: ScrappingResourceEntity? = null
): ProblemResolution {

    enum class Resolution {
        DISCARD_NEW, USE_NEW, USE_CONSENSUS, KEEP_BOTH
    }

}