package com.gitlab.cvazer.system.scrapping.fivetools

import com.gitlab.cvazer.util.async
import com.gitlab.cvazer.util.log
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.api.errors.JGitInternalException
import org.eclipse.jgit.lib.Constants.HEAD
import org.eclipse.jgit.lib.ObjectLoader
import org.eclipse.jgit.lib.TextProgressMonitor
import org.eclipse.jgit.revwalk.RevWalk
import org.eclipse.jgit.treewalk.TreeWalk
import org.eclipse.jgit.treewalk.filter.PathFilter
import org.springframework.stereotype.Component
import java.io.File
import java.io.PrintWriter
import java.io.StringWriter
import java.util.concurrent.Future
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicReference

@Component
class FiveEToolsMiddleware {
    private val _status: AtomicReference<String?> = AtomicReference(null)
    private val _stringWriter: AtomicReference<StringWriter> = AtomicReference(StringWriter())
    private val _downloading: AtomicBoolean = AtomicBoolean(false)

    val status: String
        get() = if (_downloading.get() && !gitDownload.isDone) {
            val res = _stringWriter.get().toString()
            val buf = _stringWriter.get().buffer
            buf.delete(0, buf.length)
            res
        } else {
            _status.get() ?: "No status info"
        }

    private val gitDownload by lazy<Future<Git>> {
        async {
            try {
                _status.set("downloading git files...")
                Git.cloneRepository()
                    .setURI("https://github.com/5etools-mirror-1/5etools-mirror-1.github.io.git")
                    .setDirectory(File("tmp/5etools"))
                    .setNoTags()
                    .setProgressMonitor(TextProgressMonitor(PrintWriter(_stringWriter.get())))
                    .setCloneSubmodules(false)
                    .setCloneAllBranches(false)
                    .call()
            } catch (e: JGitInternalException) {
                log().info(e.message)
                Git.open(File("tmp/5etools"))!!
            }.also {
                try {
                    it.pull().call()
                } catch (e: Exception) {
                    log().warn(e.message)
                }
            }
        }
    }

    fun init(
        filterSupplier: () -> String,
        fileConsumer: (loader: ObjectLoader, treeWalk: TreeWalk) -> Unit
    ): Boolean {
        _downloading.set(true)
        if (!gitDownload.isDone) return false
        _status.set("downloading git files finished")
        gitDownload.get().apply {
            RevWalk(repository).use { revWalk ->
                TreeWalk(repository).use { treeWalk ->
                    treeWalk.addTree(revWalk.parseCommit(repository.resolve(HEAD)).tree)
                    treeWalk.isRecursive = true
                    treeWalk.filter = PathFilter.create(filterSupplier.invoke())
                    while (treeWalk.next()){
                        val loader: ObjectLoader = repository.open(treeWalk.getObjectId(0))
                        fileConsumer(loader, treeWalk)
                    }
                }
            }
        }
        return true
    }

}