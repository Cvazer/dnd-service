package com.gitlab.cvazer.system.scrapping.fivetools

import com.fasterxml.jackson.annotation.JsonProperty
import com.gitlab.cvazer.dao.entity.system.ScrappingResourceEntity
import com.gitlab.cvazer.dao.entity.SpellEntity
import com.gitlab.cvazer.dao.entity.link.SpellTagLinkEntity
import com.gitlab.cvazer.dao.entity.link.identity.SpellTagLinkIdentity
import com.gitlab.cvazer.dao.entity.system.TagEntity
import com.gitlab.cvazer.dao.repo.SpellDao
import com.gitlab.cvazer.dao.repo.TagDao
import com.gitlab.cvazer.dao.repo.generic.*
import com.gitlab.cvazer.dao.repo.link.SpellTagLinkDao
import com.gitlab.cvazer.system.scrapping.abstraction.AbstractResourceProcessor
import com.gitlab.cvazer.util.*
import com.gitlab.cvazer.util.business.getOrError
import org.apache.commons.codec.digest.DigestUtils
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
class FiveEToolsSpellsResourceProcessor(
    private val spellDao: SpellDao,
    private val tagDao: TagDao,
    private val spellTagLinkDao: SpellTagLinkDao,
    private val tagDomainDictDao: TagDomainDictDao,
    private val magicSchoolDictDao: MagicSchoolDictDao,
    private val spellTagLinkTypeDictDao: SpellTagLinkTypeDictDao,
    private val sourceDictDao: SourceDictDao,
    private val magicTimeUnitDictDao: MagicTimeUnitDictDao,
    private val magicRangeTypeDictDao: MagicRangeTypeDictDao,
    private val magicDistanceTypeDictDao: MagicDistanceTypeDictDao,
    private val magicDurationTypeDictDao: MagicDurationTypeDictDao,
): AbstractResourceProcessor<ScrappingResourceEntity, SpellEntity>() {
    private final val schools: Map<String, String> = mutableMapOf(
        Pair("I", "ILLUSION"),
        Pair("E", "ENCHANTMENT"),
        Pair("V", "EVOCATION"),
        Pair("A", "ABJURATION"),
        Pair("D", "DIVINATION"),
        Pair("T", "TRANSMUTATION"),
        Pair("C", "CONJURATION"),
        Pair("N", "NECROMANCY"),
    )

    private final val sources = mutableMapOf(
        Pair("AI", "AI"),
        Pair("AitFR-AVT", "AFRAVT"),
        Pair("EGW", "EGW"),
        Pair("FTD", "FTD"),
        Pair("GGR", "GGR"),
        Pair("IDRotF", "IDRF"),
        Pair("LLK", "LLK"),
        Pair("PHB", "PHB"),
        Pair("TCE", "TCE"),
        Pair("SCC", "SCC"),
        Pair("UA2020PsionicOptionsRevisited", "UA2POR"),
        Pair("UA2020SpellsAndMagicTattoos", "UA2SMT"),
        Pair("UA2021DraconicOptions", "UA21DO"),
        Pair("UAArtificerRevisited", "UAAR"),
        Pair("UAFighterRogueWizard", "UAFRW"),
        Pair("UAModernMagic", "UAMM"),
        Pair("UAStarterSpells", "UASS"),
        Pair("UAThatOldBlackMagic", "UAOBM"),
        Pair("XGE", "XGE"),
        Pair("UASorcererAndWarlock", "UASAW"),
        Pair("SCAG", "SCAG"),
        Pair("AitFR-FCD", "AFRFCD"),
        Pair("EEPC", "EEPC"),
        Pair("ERLW", "ERLW"),
        Pair("UAWGE", "WGE"),
        Pair("VGM", "VGM"),
        Pair("MOT", "MOT"),
    )
    
    private final val miscTags = mutableMapOf(
        Pair("HL", "Healing"),
        Pair("THP", "Grants Temporary Hit Points"),
        Pair("SGT", "Requires Sight"),
        Pair("PRM", "Permanent Effects"),
        Pair("SCL", "Scaling Effects"),
        Pair("SMN", "Summons Creature"),
        Pair("MAC", "Modifies AC"),
        Pair("TP", "Teleportation"),
        Pair("FMV", "Forced Movement"),
        Pair("RO", "Rollable Effects"),
        Pair("LGTS", "Creates Sunlight"),
        Pair("LGT", "Creates Light"),
    )
    
    private final val spellArea = mutableMapOf(
        Pair("ST", "Single Target"),
        Pair("MT", "Multiple Targets"),
        Pair("C", "Cube"),
        Pair("N", "Cone"),
        Pair("Y", "Cylinder"),
        Pair("S", "Sphere"),
        Pair("R", "Circle"),
        Pair("Q", "Square"),
        Pair("L", "Line"),
        Pair("H", "Hemisphere"),
        Pair("W", "Wall")
    )

    @Transactional
    override fun map(resource: ScrappingResourceEntity): SpellEntity? {
        log().info("Start baking [{}]", resource.locator)
        val data: Data = resource.data.readAs()
        val res = getSpellEntity(resource, data)
        val tags = mutableListOf<SpellTagLinkEntity>()

        //SHOOL
        res.school = magicSchoolDictDao.findById(schools[data.school]!!).get()
        tags.add(getTag(res.school.id, "SCHOOL", "MAGIC_SCHOOL", res))

        //SOURCE
        res.source = sourceDictDao.findById(sources[data.source]!!).get()
        tags.add(getTag(res.source.id, "SOURCE", "SOURCE", res, res.source.id))

        data.otherSources?.forEach {
            val key = it["source"].toString()
            tags.add(getTag(key, "SOURCE", "SOURCE", res))
        }
        data.additionalSources?.forEach {
            val key = it["source"].toString()
            tags.add(getTag(key, "SOURCE", "SOURCE", res))
        }

        //LEVEL
        res.level = data.level
        tags.add(getTag("""SPELL_SLOT_${res.level}""", "SPELL_SLOT_LEVEL", "MAGIC_LEVEL", res, "${
            when(res.level) {
                1 -> "1st"
                2 -> "2nd"
                3 -> "3rd"
                else -> res.level.toString()+"th"
            }
        } level"))

        //CASTING
        if (data.time.size != 1) {
            data.time.forEachIndexed f@{ i, it ->
                if (i == 0) return@f
                val unit = magicTimeUnitDictDao.findById(it.unit.uppercase()).get()
                tags.add(getTag(
                    "${it.number}_${unit.id}", "CASTING_TIME",
                    "TIME_FRAME", res,
                    "${it.number} ${
                        when (it.number) {
                            1 -> unit.name
                            else -> unit.plural
                        }
                    }"
                ))
            }
        }
        val castingTime = data.time[0]
        res.castingInfo.castCondition = castingTime.condition
        res.castingInfo.timeAmount = castingTime.number
        res.castingInfo.timeUnit = magicTimeUnitDictDao.findById(castingTime.unit.uppercase()).get()
        tags.add(getTag(
            "${res.castingInfo.timeAmount}_${res.castingInfo.timeUnit!!.id}",
            "CASTING_TIME", "TIME_FRAME", res,
            "${res.castingInfo.timeAmount} ${
                when (res.castingInfo.timeAmount) {
                    1 -> res.castingInfo.timeUnit!!.name
                    else -> res.castingInfo.timeUnit!!.plural
                }
            }"
        ))

        res.castingInfo.verbal = data.components.v
        res.castingInfo.somatic = data.components.s
        res.castingInfo.ritual = data.components.r
        data.meta?.get("ritual")
            ?.let { it as Boolean }
            ?.takeIf { it }
            ?.also { res.castingInfo.ritual = it }
        res.castingInfo.material = data.components.m != null
        res.castingInfo.materialFlavor = data.components.m?.let {
            when (data.components.m) {
                is String -> data.components.m
                is Map<*, *> -> {
                    @Suppress("UNCHECKED_CAST") val m = data.components.m as Map<String, *>
                    m["text"]?.let { it as String }
                }
                else -> null
            }
        }
        res.castingInfo.materialCost = data.components.m?.let {
            when (data.components.m) {
                is Map<*, *> -> {
                    @Suppress("UNCHECKED_CAST") val m = data.components.m as Map<String, *>
                    m["cost"]?.toString()?.toInt()
                }
                else -> null
            }
        }
        res.castingInfo.materialConsume = data.components.m?.let {
            when (data.components.m) {
                is Map<*, *> -> {
                    @Suppress("UNCHECKED_CAST") val m = data.components.m as Map<String, *>
                    m["consume"]?.let { when (it) {
                        is Boolean -> it
                        else -> false
                    } } ?: false
                }
                else -> false
            }
        } ?: false

        res.castingInfo.materialCost?.also { tags.add(getTag(
            "MATERIAL_WITH_COST", "MISC", "SPELL_MISC",
            res, "Material with cost"
        )) }
        res.castingInfo.takeIf { it.materialConsume }?.also { tags.add(getTag(
            "MATERIAL_IS_CONSUMED", "MISC", "SPELL_MISC",
            res, "Material is consumed"
        )) }
        res.castingInfo.takeIf { it.verbal }?.also {
            tags.add(getTag(
                "VERBAL", "COMPONENT",
                "SPELL_CASTING_COMPONENT", res, "Verbal"
            ))
        }
        res.castingInfo.takeIf { it.somatic }?.also {
            tags.add(getTag(
                "SOMATIC", "COMPONENT",
                "SPELL_CASTING_COMPONENT", res, "Somatic"
            ))
        }
        res.castingInfo.takeIf { it.material }?.also {
            tags.add(getTag(
                "MATERIAL", "COMPONENT",
                "SPELL_CASTING_COMPONENT", res, "Material"
            ))
        }
        res.castingInfo.takeIf { it.ritual }?.also {
            tags.add(getTag(
                "RITUAL", "MISC",
                "SPELL_MISC", res, "Ritual"
            ))
        }

        //RANGE
        res.rangeInfo.type = data.range.type.let { magicRangeTypeDictDao.findById(it.uppercase()).get() }
        data.range.distance
            ?.let { magicDistanceTypeDictDao.findById(it.type.uppercase()).get() }
            ?.also { res.rangeInfo.distance = it }
        data.range.distance
            ?.let { if (it.amount == 0) null else it.amount }
            ?.also { res.rangeInfo.amount = it }
        tags.add(getTag(
            res.rangeInfo.type!!.id, "RANGE", "SPELL_RANGE", res
        ))
        res.rangeInfo.distance?.also { distance ->
            tags.add(getTag(
                "${ if (res.rangeInfo.amount == null) "" else "${res.rangeInfo.amount}_"}${distance.id}",
                "DISTANCE", "DISTANCE", res,
                "${ if (res.rangeInfo.amount == null) "" else "${res.rangeInfo.amount} "}${distance.name}",
            ))
        }

        //DURATION
        if (data.duration.size != 1) {
            data.duration.forEachIndexed f@{ i, it ->
                if (i == 0) return@f
                val typeUnit = magicDurationTypeDictDao.findById(it.type.uppercase()).get()
                tags.add(getTag(
                    typeUnit.id, "DURATION",
                    "TIME_FRAME_TYPE", res,
                    typeUnit.name
                ))
                it.duration?.let { dur ->
                    val unit = magicTimeUnitDictDao.findById(dur.type.uppercase()).get()
                    tags.add(getTag(
                        "${if (dur.amount==0) "" else "${dur.amount}_"}${unit.id}",
                        "DURATION_TIME", "TIME_FRAME", res,
                        "${if (dur.amount==0) "" else "${dur.amount}_"}${
                            when (dur.amount) {
                                1 -> unit.name
                                else -> unit.plural
                            }
                        }",
                    ))
                }
                if (it.concentration) {
                    tags.add(getTag(
                        "CONCENTRATION", "MISC",
                        "SPELL_MISC", res,
                        "Concentration"
                    ))
                }
                it.ends?.apply { processSimpleStringTags(
                    this, { "END_CONDITION" }, { "SPELL_END_CONDITION" },
                    res, tags
                ) }
            }
        }
        val duration = data.duration[0]
        res.durationInfo.type = duration.type
            .let { magicDurationTypeDictDao.findById(it.uppercase()).get() }
        res.durationInfo.concentration = duration.concentration
        duration.duration
            ?.let { magicTimeUnitDictDao.findById(it.type.uppercase()).get() }
            ?.also { res.durationInfo.timeUnit = it }
        duration.duration
            ?.let { if (it.amount == 0) null else it.amount }
            ?.also { res.durationInfo.timeAmount = it }
        tags.add(getTag(
            res.durationInfo.type!!.id, "DURATION",
            "TIME_FRAME_TYPE", res,
            res.durationInfo.type!!.name
        ))
        if (res.durationInfo.concentration) {
            tags.add(getTag(
                "CONCENTRATION", "MISC",
                "SPELL_MISC", res,
                "Concentration"
            ))
        }
        res.durationInfo.timeUnit?.also { timeUnit ->
            tags.add(getTag(
                "${ if (res.durationInfo.timeAmount == null) "" else "${res.durationInfo.timeAmount}_"}${timeUnit.id}",
                "DURATION_TIME", "TIME_FRAME", res,
                "${ if (res.durationInfo.timeAmount == null) "" else "${res.durationInfo.timeAmount} "}${
                    when (res.durationInfo.timeAmount) {
                        1 -> timeUnit.name
                        else -> timeUnit.plural
                    }
                }",
            ))
        }
        duration.ends?.apply { processSimpleStringTags(
            this, { "END_CONDITION" }, { "SPELL_END_CONDITION" },
            res, tags
        ) }

        //ENTRIES
        res.entries = mutableListOf<Any>()
            .apply { addAll(data.entries) }
            .apply { data.entriesHigherLevel?.also { addAll(it) } }

        //MISC TAGS
        data.damageInflict?.also { processSimpleStringTags(
            it, { "DAMAGE_INFLICT" }, { "DAMAGE_TYPE" },
            res, tags
        ) }
        data.damageVulnerable?.also { processSimpleStringTags(
            it, { "DAMAGE_VULNERABLE" }, { "DAMAGE_TYPE" },
            res, tags
        ) }
        data.damageImmune?.also { processSimpleStringTags(
            it, { "DAMAGE_IMMUNE" }, { "DAMAGE_TYPE" },
            res, tags
        ) }
        mutableListOf<String>()
            .apply { data.damageResist?.also { addAll(it) } }
            .apply { data.damageResists?.also { addAll(it) } }
            .also { processSimpleStringTags(
                it, { "DAMAGE_RESIST" }, { "DAMAGE_TYPE" },
                res, tags
            ) }
        data.conditionInflict?.also { processSimpleStringTags(
            it, { "CONDITION_INFLICT" }, { "CONDITION" },
            res, tags
        ) }
        data.conditionImmune?.also { processSimpleStringTags(
            it, { "CONDITION_IMMUNE" }, { "CONDITION" },
            res, tags
        ) }
        data.affectsCreatureType?.also { processSimpleStringTags(
            it, { "AFFECTS_CREATURE_TYPE" }, { "CREATURE_TYPE" },
            res, tags
        ) }
        data.spellAttack?.map { when(it) {
            "M" -> "MELEE"
            "R" -> "RANGE"
            else -> error("Unknown spell attack")
        } }?.forEach { tags.add(getTag(it, "SPELL_ATTACK_TYPE", "SPELL_ATTACK_TYPE", res)) }
        data.savingThrow?.also { processSimpleStringTags(
            it, { "REQUIRES_SAVING_THROW" }, { "CHARACTERISTIC" },
            res, tags
        ) }
        data.miscTags?.also { processSimpleStringTags(
            it, { "MISC" }, { "SPELL_MISC" }, res, tags,
            { s -> miscTags[s]!! }, { s -> fullNormTagName(miscTags[s]!!) }
        ) }
        data.areaTags?.also { processSimpleStringTags(
            it, { "AREA" }, { "SPELL_AREA" }, res, tags,
            { s -> spellArea[s]!! }, { s -> fullNormTagName(spellArea[s]!!) }
        ) }
        data.abilityCheck?.also { processSimpleStringTags(
            it, { "INVOLVES_ABILITY_CHECK" }, { "CHARACTERISTIC" }, res, tags
        ) }
        data.classes?.also { clsData ->
            mutableListOf<String>()
                .apply { clsData.fromClassList?.map { it.name }?.also { addAll(it) } }
                .apply { clsData.fromClassListVariant?.map { it.name }?.also { addAll(it) } }
                .also { l -> processSimpleStringTags(
                    l, { "CLASS_LIST" }, { "CLASS" }, res, tags,
                    { it }, { fullNormTagName(it) }
                ) }
            clsData.fromSubclass?.map {
                "${it.cls.name} - ${it.subclass.name}${
                    it.subclass.subSubclass?.let { s -> " ($s)" } ?: ""
                }"
            }?.also { list ->
                processSimpleStringTags(
                    list, { "SUBCLASS_LIST" }, { "SUBCLASS" }, res, tags,
                    { it }, { fullNormTagName(it) }
                )
            }
        }
        data.backgrounds?.map { it.name }?.also { list -> processSimpleStringTags(
            list, { "BACKGROUND" }, { "BACKGROUND" }, res, tags,
            { it }, { fullNormTagName(it) }
        ) }
        data.races?.forEach {
            if (it.baseName != null) {
                tags.add(getTag(
                    fullNormTagName(it.baseName), "RACE", "RACE",
                    res, it.baseName
                ))
                tags.add(getTag(
                    fullNormTagName(it.name), "SUBRACE", "SUBRACE",
                    res, it.name
                ))
            } else {
                tags.add(getTag(
                    fullNormTagName(it.name), "RACE", "RACE",
                    res, it.name
                ))
            }
        }
        data.eldritchInvocations?.map { it.name }?.also { list -> processSimpleStringTags(
            list, { "ELDRITCH_INVOCATION" }, { "ELDRITCH_INVOCATION" }, res, tags,
            { it }, { fullNormTagName(it) }
        ) }

        res.tags = tags
            .distinctBy { it.tag.id+"|"+it.type.id }
            .toMutableList()
        return res
    }

    private fun getSpellEntity(resource: ScrappingResourceEntity, data: Data): SpellEntity {
        return if (resource.resId != null) spellDao.getOrError(resource.resId!!.toLong())
        else SpellEntity(data.name)
    }

    private fun fullNormTagName(name: String) =
        name.uppercase()
            .replace(" - ", "_")
            .replace(" ", "_")
            .replace("\\W".toRegex(), "")

    private fun getTag(tag: String,
                       type: String,
                       domain: String,
                       spell: SpellEntity,
                       name: String? = null): SpellTagLinkEntity {
        return spellTagLinkDao.findById(
            SpellTagLinkIdentity(spell.id, tag, type)
        ).orElse(SpellTagLinkEntity(
            tag = tagDao.findById(tag).orElse(TagEntity(
                tag, name ?: tag.lowercase().capitalize(),
                tagDomainDictDao.getReferenceById(domain)
            ).let { tagDao.save(it) }),
            spell = spell,
            type = spellTagLinkTypeDictDao.getReferenceById(type)
        ))
    }

    private fun processSimpleStringTags(list: Collection<String>,
                                        type: (s: String) -> String,
                                        domain: (s: String) -> String,
                                        spell: SpellEntity,
                                        tags: MutableList<SpellTagLinkEntity>,
                                        name: ((s: String) -> String)? = null,
                                        tag: ((s: String) -> String)? = null,) {
        list.forEach { string ->
            tags.add(getTag(
                tag?.let { it(string) } ?: fullNormTagName(string),
                type(string), domain(string), spell,
                name?.let { it(string) } ?: string.lowercase().capitalize()
            ))
        }
    }

    @Transactional
    override fun persist(result: SpellEntity) = spellDao.saveAndFlush(result)

    override fun isCompatible(resource: ScrappingResourceEntity) =
        resource.source == "5ETOOLS" && resource.type.id == "SPELL"

    @Transactional
    override fun markAsBaked(resource: ScrappingResourceEntity, result: SpellEntity) {
        resource.baked_hash = hash(result)
        resource.resId = result.id.toString()
    }

    @Transactional
    override fun doPersist(res: SpellEntity, resource: ScrappingResourceEntity) = resource.baked_hash != hash(res)

    @Suppress("UNCHECKED_CAST")
    private fun hash(spell: SpellEntity) = DigestUtils.md5Hex(
        spell.dtoMap()
            .cut("id").cut("entries")
            .transform<MutableMap<String, Any?>>("durationInfo") { it.cut("id") }
            .transform<MutableMap<String, Any?>>("rangeInfo") { it.cut("id") }
            .transform<MutableMap<String, Any?>>("castingInfo") { it.cut("id") }
            .transform<List<MutableMap<String, Any?>>>("tags") {list -> list
                .map { it["tag"] as MutableMap<String, Any?> }
                .map { it["id"] }
            }.jsonString().toByteArray()
    )

    private class Data(
        val name: String,
        val school: String,
        val source: String,
        val otherSources: List<Map<String, Any>>?,
        val additionalSources: List<Map<String, Any>>?,
        val page: Int, //Not interested
        val level: Int,
        val srd: Any?, //Not interested
        val basicRules: Boolean?, //Not interested
        val time: List<CastingTime>,
        val range: Range,
        val components: Components,
        val duration: List<Duration>,
        val entries: List<Any>,
        val entriesHigherLevel: List<Any>?,
        val scalingLevelDice: Any?,
        val damageInflict: List<String>?,
        val damageVulnerable: List<String>?,
        val damageImmune: List<String>?,
        val damageResist: List<String>?,
        val damageResists: List<String>?,
        val conditionInflict: List<String>?,
        val conditionImmune: List<String>?,
        val affectsCreatureType: List<String>?,
        val spellAttack: List<String>?,
        val savingThrow: List<String>?,
        val miscTags: List<String>?,
        val areaTags: List<String>?,
        val abilityCheck: List<String>?,
        val classes: Classes?,
        val backgrounds: List<Background>?,
        val races: List<Race>?,
        val eldritchInvocations: List<Invocation>?,
        val hasFluffImages: Boolean?,
        val hasFluff: Boolean?,
        val meta: Map<String, Any>?,
    )
    {
        class CastingTime(val number: Int, val unit: String, val condition: String?)
        class Range(val type: String, val distance: Distance?){
            class Distance(val type: String, val amount: Int)
        }
        class Components(val v: Boolean = false, val s: Boolean = false, val m: Any?, val r: Boolean)
        class Duration(val type: String, val duration: Dur?, val concentration: Boolean, val ends: List<String>?, val condition: String?){
            class Dur(val type: String, val amount: Int, val upTo: Boolean?)
        }
        class Scaling(val label: String, val scaling: Map<String, String>)
        class Classes(
            val fromClassList: List<GameCls>?,
            val fromSubclass: List<GameSubCls>?,
            val fromClassListVariant: List<GameClsVariant>?
            ){
            class GameCls(val name: String, val source: String, val definedInSource: String?)
            class GameClsVariant(val name: String, val source: String, val definedInSource: String)
            class GameSubCls(@JsonProperty("class") val cls: GameSubClsRecord, val subclass: GameSubClsRecord){
                class GameSubClsRecord(val name: String, val source: String, val subSubclass: String?)
            }
        }
        class Background(val name: String, val source: String)
        class Invocation(val name: String, val source: String)
        class Race(val name: String, val source: String, val baseName: String?, val baseSource: String?)
    }
}