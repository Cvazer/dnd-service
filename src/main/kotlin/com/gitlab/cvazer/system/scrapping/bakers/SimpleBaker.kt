package com.gitlab.cvazer.system.scrapping.bakers

import com.gitlab.cvazer.dao.entity.system.ScrappingResourceEntity
import com.gitlab.cvazer.dao.entity.SpellEntity
import com.gitlab.cvazer.dao.repo.ScrappingResourceDao
import com.gitlab.cvazer.system.scrapping.abstraction.AbstractBaker
import com.gitlab.cvazer.system.scrapping.abstraction.ResourceProcessor
import org.springframework.stereotype.Component

@Component
class SimpleBaker(
    processors: List<ResourceProcessor<ScrappingResourceEntity, SpellEntity>>,
    private val scrappingResourceDao: ScrappingResourceDao
): AbstractBaker<ScrappingResourceEntity, String, String>(processors) {

    override fun updateResource(resource: ScrappingResourceEntity) {
        scrappingResourceDao.save(resource)
    }

    override fun findAllRaw() = scrappingResourceDao.findAllByBake(true)
}