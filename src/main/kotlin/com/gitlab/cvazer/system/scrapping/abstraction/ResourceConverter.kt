package com.gitlab.cvazer.system.scrapping.abstraction

import com.gitlab.cvazer.util.etc.ScrapConversionException

interface ResourceConverter<Resource, Entity> {

    @Throws(ScrapConversionException::class)
    fun convertResource(resource: Resource): Entity

    @Throws(ScrapConversionException::class)
    fun convertEntity(entity: Entity): Resource

    fun getResourceType(): String
}