package com.gitlab.cvazer.system.scrapping.fivetools

import com.fasterxml.jackson.databind.ObjectMapper
import com.gitlab.cvazer.dao.entity.system.ScrappingResourceEntity
import com.gitlab.cvazer.dao.repo.generic.ScrappingResourceTypeDictDao
import com.gitlab.cvazer.system.scrapping.abstraction.ResourceProvider
import com.gitlab.cvazer.system.scrapping.model.ScrappingProblem
import org.eclipse.jgit.lib.ObjectLoader
import org.springframework.stereotype.Component
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue

@Component
class FiveEToolsSpellsResourceProvider(
    private val middleware: FiveEToolsMiddleware,
    private val objectMapper: ObjectMapper,
    private val scrappingResourceTypeDictDao: ScrappingResourceTypeDictDao
): ResourceProvider<ScrappingResourceEntity> {
    private var fileLoaders: BlockingQueue<Pair<ObjectLoader, String>> = LinkedBlockingQueue()
    private val resourceTypeDictEntity by lazy { scrappingResourceTypeDictDao.findById("SPELL").get() }
    private var inited: Boolean = false
    private var state: String = ""
    private val problems = mutableListOf<ScrappingProblem>()

    override fun init(): Boolean = if (!inited) middleware.init({ "data/spells/" }, consumer@{ loader, walk ->
        if (!walk.nameString.matches("spells[-a-zA-Z\\d]+[.]json".toRegex())) return@consumer
        fileLoaders.put(Pair(loader, walk.pathString))
        inited = true
    }) else true

    override fun isReady(): Boolean = true

    override fun isDone(): Boolean = fileLoaders.isEmpty() && inited

    override fun getBatch(): List<ScrappingResourceEntity> {
        val pair = fileLoaders.take()
        state = "parsing file ${pair.second}"
        return objectMapper.readTree(pair.first.bytes)["spell"].asSequence()
            .map { ScrappingResourceEntity(
                locator = "SPELL ${it["name"].asText()}",
                source = "5ETOOLS",
                data = it.toString(),
                type = resourceTypeDictEntity
            ) }
            .onEach { it.meta = mutableMapOf(
                Pair("sourceFile", pair.second),
                Pair("provider", this::class.java.simpleName)
            ) }
            .toList()
    }

    override fun reset() {
        inited = false
        state = ""
        problems.clear()
    }

    override fun close(): Boolean {
        return true
    }

    override fun status(): String =
        if (!inited) middleware.status
        else if (fileLoaders.isEmpty()) "Done"
        else state

    override fun problems(): List<ScrappingProblem> {
        val res = ArrayList(problems)
        problems.clear()
        return res
    }
}