package com.gitlab.cvazer.system.scrapping.abstraction

import com.gitlab.cvazer.system.scrapping.model.ScrapperStatus
import com.gitlab.cvazer.system.scrapping.model.ScrappingProblem
import java.util.concurrent.atomic.AtomicReference
import javax.transaction.Transactional

abstract class AbstractScrapper<
        Resource: Scrap<Locator, Hash>,
        Hash: Comparable<Hash>,
        Locator: Comparable<Locator>
        > (
    private val providers: Collection<ResourceProvider<Resource>>,
): Scrapper {
    private val status: AtomicReference<String> = AtomicReference("Ready")
    private val problems: AtomicReference<MutableList<ScrappingProblem>> = AtomicReference(mutableListOf())


    override fun scrap(): Boolean {
        try {
            val initializing = providers.asSequence()
                .filter { !it.init() }
                .any()

            val scrapList = providers.asSequence()
                .filter { it.init() } // filter already initialised only!
                .filter { it.isReady() } // filter ones, that ready to fetch
                .filter { !it.isDone() } // filter only thous that have anything to fetch
                .flatMap { it.getBatch().asSequence() }
                .toList()
            val fetching = scrapList.isNotEmpty() // if something was fetched => still fetching

            val closing = providers.asSequence()
                .filter { it.isDone() } // find ones that not done
                .filter { !it.close() } // try to close them and filter NOT closed
                .any() // if any => true (still closing)

            scrapList.forEach { persistScrap(it) }

            providers.forEach { problems.get().addAll(it.problems()) }

            val working = fetching || closing || initializing
            if (!working) status.set("Done")

            return working
        } catch (e: Exception) {
            status.set("Error: ${e.message}")
            throw e
        }
    }

    override fun status() = ScrapperStatus(
        status = status.get(),
        providersStatus = providers
            .associateBy(
                { it::class.java.simpleName },
                { it.status() }
            ),
        problems = problems.get()
    )

    override fun reset() {
        status.set("Ready")
        problems.get().clear()
        providers.forEach { it.reset() }
    }

    @Transactional
    protected open fun persistScrap(scrap: Resource) {
        scrap.rawHash = calculateHash(scrap)
        val persisted = persisted(scrap.locator)

        if (persisted == null) {
            persist(scrap.apply { bake = true })
            return
        }

        val scrapHash = scrap.rawHash!!
        val persistedHash = persisted.rawHash ?: calculateHash(persisted)

        if (scrapHash.compareTo(persistedHash) == 0) {
            ScrappingProblem.resourceNotUpdated(scrap, idSalt(listOf(scrap)))
                .takeIf { !problemAlreadyResolved(it) }
                ?.also { problems.get().add(it) }
        } else {
            ScrappingProblem.resourceAlreadyExists(scrap, persisted, idSalt(listOf(scrap, persisted)))
                .takeIf { !problemAlreadyResolved(it) }
                ?.also { problems.get().add(it) }
        }
    }



    override fun problemResolved(id: String): Boolean = problems.get().removeIf { it.id == id }

    protected abstract fun problemAlreadyResolved(problem: ScrappingProblem): Boolean
    protected abstract fun idSalt(resources: List<Resource>): String
    protected abstract fun calculateHash(scrap: Resource): Hash
    protected abstract fun persisted(locator: Locator): Resource?
    protected abstract fun persist(resource: Resource): Resource

}