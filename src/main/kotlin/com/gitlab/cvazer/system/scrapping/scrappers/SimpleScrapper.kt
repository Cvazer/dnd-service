package com.gitlab.cvazer.system.scrapping.scrappers

import com.gitlab.cvazer.dao.entity.system.ScrappingResourceEntity
import com.gitlab.cvazer.dao.repo.ProblemResolutionDao
import com.gitlab.cvazer.dao.repo.ScrappingResourceDao
import com.gitlab.cvazer.system.scrapping.abstraction.AbstractScrapper
import com.gitlab.cvazer.system.scrapping.abstraction.ProblemResolver
import com.gitlab.cvazer.system.scrapping.abstraction.ResourceProvider
import com.gitlab.cvazer.system.scrapping.model.ResourceConflictProblemResolution
import com.gitlab.cvazer.system.scrapping.model.ResourceConflictProblemResolution.Resolution.*
import com.gitlab.cvazer.system.scrapping.model.ScrappingProblem
import org.apache.commons.codec.digest.DigestUtils
import org.springframework.stereotype.Component
import java.util.*

@Component
class SimpleScrapper(
    providers: List<ResourceProvider<ScrappingResourceEntity>>,
    private val scrappingResourceDao: ScrappingResourceDao,
    private val problemResolutionDao: ProblemResolutionDao
):
    AbstractScrapper<ScrappingResourceEntity, String, String>(providers),
    ProblemResolver<ResourceConflictProblemResolution>
{
    override fun problemAlreadyResolved(problem: ScrappingProblem): Boolean =
        problemResolutionDao.existsByProblemId(problem.id)

    override fun name(): String = "SimpleCrapper"
    override fun calculateHash(scrap: ScrappingResourceEntity): String = DigestUtils.md5Hex(scrap.data)
    override fun persisted(locator: String) = scrappingResourceDao.findByLocator(locator)

    override fun persist(resource: ScrappingResourceEntity): ScrappingResourceEntity =
        scrappingResourceDao.save(resource)

    override fun idSalt(resources: List<ScrappingResourceEntity>): String = resources
        .joinToString("|") { it.rawHash ?: calculateHash(it) }
        .let { UUID.nameUUIDFromBytes(it.toByteArray()).toString() }

    override fun resolveResourceConflict(resolution: ResourceConflictProblemResolution):Boolean {
        when(resolution.resolution){
            DISCARD_NEW -> {}
            USE_NEW -> {
                persist(resolution.newRes!!.apply {
                    id = resolution.persisted!!.id
                    bake = true
                })
            }
            USE_CONSENSUS -> {
                persist(resolution.consensus!!.apply {
                    id = resolution.persisted!!.id
                    bake = true
                })
            }
            KEEP_BOTH -> {
                persist(resolution.newRes!!.apply {
                    bake = true
                })
            }
        }
        return problemResolved(resolution.problemId)
    }

}