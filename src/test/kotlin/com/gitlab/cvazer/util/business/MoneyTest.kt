package com.gitlab.cvazer.util.business

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

internal class MoneyTest {
    @Test
    @DisplayName("Check units to Money sanity")
    fun unitsToMoney() {
        var l = (200_000+400+80+1).toLong()
        var money = Money(l) //2p4g8s1c
        assertEquals(2, money.pp)
        assertEquals(4, money.gp)
        assertEquals(8, money.sp)
        assertEquals(1, money.cp)
        assertEquals(l, money.getUnits())

        l = (0+34_200+10+9).toLong()
        money = Money(l) //0p342g1s9c
        assertEquals(0, money.pp)
        assertEquals(342, money.gp)
        assertEquals(1, money.sp)
        assertEquals(9, money.cp)
        assertEquals(l, money.getUnits())
    }

    @Test
    @DisplayName("Check conversion")
    fun conversion() {
        assertEquals(1, Money.parse("1c").cp)
        assertEquals(1, Money.parse("1s").sp)
        assertEquals(1, Money.parse("1g").gp)
        assertEquals(1, Money.parse("1p").pp)
    }

    @Test
    @DisplayName("Check parse sanity")
    fun parse() {
        val money = Money.parse("1g2s")
        assertEquals(1, money.gp)
        assertEquals(2, money.sp)
    }

    @Test
    @DisplayName("Check operators sanity")
    fun operators() {
        val money = Money.parse("1g")
        val money2 = Money.parse("1g")
        assertEquals(2, (money + money2).gp)
        money += money2
        assertEquals(2, money.gp)
        money -= money2
        assertEquals(1, money.gp)
    }
}