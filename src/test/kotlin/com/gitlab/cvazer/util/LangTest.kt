package com.gitlab.cvazer.util

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

class LangTest {

    @Test
    @DisplayName("Dto map sanity test")
    fun dtoMapSanity(){
        val res = TestClass(10, 20, 30).dtoMap()
            .cut("x", "y")
            .paste(
                Pair("a", 1),
                Pair("b", 2)
            ).jsonString()
        assertEquals("{\"z\":30,\"a\":1,\"b\":2}", res)
    }

    data class TestClass(val x: Int, val y: Int, val z: Int)

}