package com.gitlab.cvazer.model.icu

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

import com.gitlab.cvazer.model.icu.ICUTimeUnit.*
import org.apache.commons.lang3.RandomUtils

internal class ICUPeriodTest {

    @Test
    @DisplayName("General check")
    fun general(){
        val seconds = RandomUtils.nextLong(1, 9999999999)
        assertEquals(seconds, ICUPeriod.of(seconds).long())
    }

    @Test
    @DisplayName("Parsing check")
    fun parsing(){
        val secs = RandomUtils.nextInt(1, 60).toString()
        val min = RandomUtils.nextInt(1, 60).toString()
        val hour = RandomUtils.nextInt(1, 24).toString()
        val day = RandomUtils.nextInt(1, 29).toString()
        val month = RandomUtils.nextInt(1, 14).toString()
        val year = RandomUtils.nextInt(1, 2000).toString()
        val time = listOf(
            secs.toInt() * SECOND.factor,
            min.toInt() * MINUTE.factor,
            hour.toInt() * HOUR.factor,
            day.toInt() * DAY.factor,
            month.toInt() * MONTH.factor,
            year.toInt() * YEAR.factor
        ).sum()
        val period = ICUPeriod.of("${year}y${month}M${day}d${hour}h${min}m${secs}s")
        assertEquals(time, period.long())
    }

    @Test
    @DisplayName("Test methods of period creation")
    fun ofTest(){
        assertEquals(
            "0 лет 0 дней 0 часов 0 минут 59 секунд",
            ICUPeriod.of(59).getString()
        )
        assertEquals(
            "0 лет 0 дней 0 часов 59 минут 0 секунд",
            ICUPeriod.of(59, MINUTE).getString()
        )
        assertEquals(
            "0 лет 0 дней 23 часов 0 минут 0 секунд",
            ICUPeriod.of(23, HOUR).getString()
        )
        assertEquals(
            "0 лет 28 дней 0 часов 0 минут 0 секунд",
            ICUPeriod.of(28, DAY).getString()
        )
        assertEquals(
            "1000 лет 0 дней 0 часов 0 минут 0 секунд",
            ICUPeriod.of(1000, YEAR).getString()
        )
    }
}