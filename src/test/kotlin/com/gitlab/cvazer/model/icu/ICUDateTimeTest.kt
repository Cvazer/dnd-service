package com.gitlab.cvazer.model.icu

import org.apache.commons.lang3.RandomUtils
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

internal class ICUDateTimeTest {

    @Test
    @DisplayName("Period conversion test")
    fun period(){
        val seconds = RandomUtils.nextLong(1, 9999999999)
        assertEquals(seconds, ICUDateTime(ICUPeriod.of(seconds)).long())
        assertEquals(seconds, ICUDateTime(ICUPeriod.of(seconds)).period().long())
    }

    @Test
    @DisplayName("Constructors")
    fun constructors(){
        val month = ICUMonth.getByNum(RandomUtils.nextInt(1, 14))
        val dbm = (month.num-1)*28
        val wom = RandomUtils.nextInt(1, 5)
        val dow = ICUDay.getByNum(RandomUtils.nextInt(0, 7))
        val dom = (wom-1)*7+dow.pos
        val year = RandomUtils.nextLong(1, Long.MAX_VALUE)

        val check = { date: ICUDateTime ->
            assertEquals(year, date.year)
            assertEquals(dbm+dom, date.dayOfYear)
            assertEquals(wom, date.weekOfMonth)
            assertEquals(dow, date.dayOfWeek)
            assertEquals(dom, date.dayOfMonth)
            assertEquals(month, date.month)
        }

        check(ICUDateTime(ICUAge.POWER, year, month, dom, 0, 0, 0))
        check(ICUDateTime(year, dbm+dom, 0, 0, 0))
        check(ICUDateTime(ICUAge.POWER, ICUPeriod(year, dbm+dom, 0, 0, 0)))
        check(ICUDateTime(ICUPeriod(year, dbm+dom, 0, 0, 0)))
        check(ICUDateTime(age= ICUAge.DRAGON, year=year, dayOfYear=dbm+dom, hour=1, minute=1, second=1))
    }

    @Test
    @DisplayName("Day of week sanity")
    fun dayOfWeekSanity() {
        val day = ICUDay.getByNum(RandomUtils.nextInt(0, 1))
        val year = RandomUtils.nextLong(1, Long.MAX_VALUE)
        val doy = (RandomUtils.nextInt(1, 53)-1) * 7 + day.pos
        val date = ICUDateTime(year, doy, 0, 0, 0)
        assertEquals(day, date.dayOfWeek)
    }

    @Test
    @DisplayName("Week of month sanity")
    fun weekOfMonthSanity() {
        val week = RandomUtils.nextInt(1, 5)
        val month = ICUMonth.getByNum(RandomUtils.nextInt(1, 14))
        val year = RandomUtils.nextLong(1, Long.MAX_VALUE)
        val doy = (month.num-1) * 28 + (week-1) * 7 + RandomUtils.nextInt(1, 8)
        val date = ICUDateTime(year, doy, 0, 0, 0)
        assertEquals(week, date.weekOfMonth)
    }

    @Test
    @DisplayName("Day of month sanity")
    fun dayOfMonthSanity() {
        val dom = RandomUtils.nextInt(1, 28)
        val month = ICUMonth.getByNum(RandomUtils.nextInt(1, 14))
        val year = RandomUtils.nextLong(1, Long.MAX_VALUE)
        val doy = (month.num-1) * 28 + dom
        val date = ICUDateTime(year, doy, 0, 0, 0)
        assertEquals(dom, date.dayOfMonth)
    }

    @Test
    @DisplayName("Month sanity")
    fun monthSanity() {
        val month = ICUMonth.getByNum(RandomUtils.nextInt(1, 14))
        val year = RandomUtils.nextLong(1, Long.MAX_VALUE)
        val doy = (month.num-1) * 28 + RandomUtils.nextInt(1, 29)
        val date = ICUDateTime(year, doy, 0, 0, 0)
        assertEquals(month, date.month)
    }

    @Test
    @DisplayName("Add Sanity")
    fun addSanity(){
        val getSecs = secs@{
            return@secs listOf(
                RandomUtils.nextInt(1, 60) * ICUTimeUnit.SECOND.factor,
                RandomUtils.nextInt(1, 60) * ICUTimeUnit.MINUTE.factor,
                RandomUtils.nextInt(1, 24) * ICUTimeUnit.HOUR.factor,
                RandomUtils.nextInt(1, 29) * ICUTimeUnit.DAY.factor,
                RandomUtils.nextInt(1, 14) * ICUTimeUnit.MONTH.factor,
                RandomUtils.nextInt(1, 2000) * ICUTimeUnit.YEAR.factor
            ).sum()
        }
        val initialDate = ICUDateTime(getSecs())
        val period = ICUPeriod.of(getSecs())
        assertEquals(initialDate.add(period).long(), period.long()+initialDate.long())
        assertEquals(initialDate.add(period.long()).long(), period.long()+initialDate.long())
        assertEquals(initialDate.add(period.long(), ICUTimeUnit.SECOND).long(), period.long()+initialDate.long())
    }

    @Test
    @DisplayName("Leap sanity")
    fun leap(){
        assertEquals(
            ICUDateTime(1, 1, 0, 1, 0),
            ICUDateTime(1, 1, 0, 0, 59).add(1)
        )
        assertEquals(
            ICUDateTime(1, 1, 1, 0, 0),
            ICUDateTime(1, 1, 0, 59, 59).add(1)
        )
        assertEquals(
            ICUDateTime(1, 2, 0, 0, 0),
            ICUDateTime(1, 1, 23, 59, 59).add(1)
        )
        assertEquals(
            ICUDateTime(2, 1, 0, 0, 0),
            ICUDateTime(1, 364, 23, 59, 59).add(1)
        )
    }

    @Test
    @DisplayName("Add test")
    fun add(){
        val res = ICUDateTime(ICUAge.REBIRTH, 235, 180, 0, 0, 0).add("1w1h30m")
        assertEquals(187, res.dayOfYear)
        assertEquals(1, res.hour)
        assertEquals(30, res.minute)
    }

    @Test
    @DisplayName("Full test")
    fun full(){
        ICUMonth.values().forEach { month -> for (week in 1..4) { ICUDay.values().forEach { day ->
            assertEquals(week, ICUDateTime(ICUAge.POWER, 1, month, (week-1)*7+day.pos, 0, 0, 0).weekOfMonth)
            assertEquals(day, ICUDateTime(ICUAge.POWER, 1, month, (week-1)*7+day.pos, 0, 0, 0).dayOfWeek)
            assertEquals((week-1)*7+day.pos,ICUDateTime(ICUAge.POWER,1,month,(week-1)*7+day.pos,0,0,0).dayOfMonth)
            assertEquals(month, ICUDateTime(ICUAge.POWER, 1, month, (week-1)*7+day.pos, 0, 0, 0).month)
        } } }
    }
}