package com.gitlab.cvazer.service

import com.gitlab.cvazer.dao.entity.BackgroundEntity
import com.gitlab.cvazer.dao.entity.CharEntity
import com.gitlab.cvazer.dao.entity.RaceEntity
import com.gitlab.cvazer.dao.repo.CharDao
import com.gitlab.cvazer.dao.repo.generic.SkillDictDao
import com.gitlab.cvazer.dao.repo.generic.StatDictDao
import com.gitlab.cvazer.dao.repo.system.AccountDao
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.mockito.Mockito.*
import java.util.*

internal class CharServiceTest {
    private lateinit var dao: CharDao

    @BeforeEach fun setUp() { dao = mock(CharDao::class.java) }

    @Test
    @DisplayName("Check addExp sanity")
    fun addExp() {
        val char = CharEntity(
                background = mock(BackgroundEntity::class.java),
                name = "TEST", playerName = "TEST PLAYER",
                race = mock(RaceEntity::class.java))
        char.genericInfo.exp = 290
        char.genericInfo.lvl = 1
        `when`(dao.findById(anyLong())).thenReturn(Optional.of(char))
        `when`(dao.save(char)).thenReturn(char)
        val service = CharService(dao, mock(StatDictDao::class.java), mock(SkillDictDao::class.java), mock(AccountDao::class.java))
        val res = service.addExp(50, 1L)
        assertEquals(2, char.genericInfo.lvl)
        assertEquals(340, char.genericInfo.exp)
        assertEquals(900, res["next"])
        assertEquals(300, res["prev"])
    }

    @Test
    @DisplayName("Check addExp lvl switch edge case sanity")
    fun addExpEdge() {
        val char = CharEntity(
                background = mock(BackgroundEntity::class.java),
                name = "TEST", playerName = "TEST PLAYER",
                race = mock(RaceEntity::class.java))
        char.genericInfo.exp = 300
        char.genericInfo.lvl = 1
        `when`(dao.findById(anyLong())).thenReturn(Optional.of(char))
        `when`(dao.save(char)).thenReturn(char)
        val service = CharService(dao, mock(StatDictDao::class.java), mock(SkillDictDao::class.java), mock(AccountDao::class.java))
        val res = service.addExp(600, 1L)
        assertEquals(3, char.genericInfo.lvl)
        assertEquals(900, char.genericInfo.exp)
        assertEquals(2_700, res["next"])
        assertEquals(900, res["prev"])
    }

}