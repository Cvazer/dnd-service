package com.gitlab.cvazer.service

import com.gitlab.cvazer.dao.entity.system.AccountEntity
import com.gitlab.cvazer.dao.repo.CharDao
import com.gitlab.cvazer.dao.repo.GameEventLogDao
import com.gitlab.cvazer.dao.repo.link.AccountCampaignLinkDao
import com.gitlab.cvazer.dao.repo.system.AccountDao
import com.gitlab.cvazer.service.system.AccountService
import com.gitlab.cvazer.util.etc.AccountNotFoundException
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.mockito.Mockito.*

internal class AccountServiceTest {
    private lateinit var dao: AccountDao
    private lateinit var charDao: CharDao
    private lateinit var accountCampaignLinkDao: AccountCampaignLinkDao
    private lateinit var gameEventLogDao: GameEventLogDao

    @BeforeEach fun setUp() {
        dao = mock(AccountDao::class.java)
        charDao = mock(CharDao::class.java)
        accountCampaignLinkDao = mock(AccountCampaignLinkDao::class.java)
        gameEventLogDao = mock(GameEventLogDao::class.java)
    }

    @Test
    @DisplayName("Check getAccount sanity positive case")
    fun getAccountPositiveCase() {
        val target = AccountEntity("test", "test", "admin")
        `when`(dao.findByKeyCaseSensitive("test")).thenReturn(target)
        val service = AccountService(dao, accountCampaignLinkDao, gameEventLogDao, charDao)
        val res = service.getAccount("test")
        assertEquals(target, res)
    }

    @Test
    @DisplayName("Check getAccount sanity negative case")
    fun getAccountNegativeCase() {
        `when`(dao.findByKeyCaseSensitive(anyString())).thenReturn(null)
        assertThrows(
                AccountNotFoundException::class.java,
                { AccountService(dao, accountCampaignLinkDao, gameEventLogDao, charDao).getAccount("test") },
                "Нет аккаунта с ключем [test]")
    }
}