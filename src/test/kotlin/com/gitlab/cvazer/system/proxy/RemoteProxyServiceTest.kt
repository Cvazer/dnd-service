package com.gitlab.cvazer.system.proxy

import com.gitlab.cvazer.system.proxy.`interface`.RemoteProxy
import com.gitlab.cvazer.system.proxy.abstract.AbstractRemoteProxyEvaluator
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.mockito.Mockito.*

internal class RemoteProxyServiceTest{
    private lateinit var registry: ProxyRegistry
    private lateinit var evaluator: AbstractRemoteProxyEvaluator<*, *>

    @BeforeEach
    fun setUp() {
        registry = mock(ProxyRegistry::class.java)
    }

    @Test
    @DisplayName("Test if evaluator is fetched correctly")
    fun testFetchCorrectEvaluator(){
        val service = RemoteProxyService(registry, listOf(
                object: AbstractRemoteProxyEvaluator<Any, Any>(){
                    override fun eval(exp: String): (shell: Any) -> Any? = { shell -> shell }
                    override fun binding(values: Map<String, Any>): Any = Any()
                    override fun shell(binding: Any): Any = Any()
                    override fun lang(): String = "TEST_LANG1"
                },
                object: AbstractRemoteProxyEvaluator<Any, Any>(){
                    override fun eval(exp: String): (shell: Any) -> Any? = { _ ->
                        throw IllegalArgumentException("CALLED TEST_LANG2")
                    }
                    override fun binding(values: Map<String, Any>): Any = Any()
                    override fun shell(binding: Any): Any = Any()
                    override fun lang(): String = "TEST_LANG2"
                }
        ))
        `when`(registry[anyString()]).then { Any() }

        val ex = assertThrows(IllegalArgumentException::class.java)
        { service.eval("NAME", "EXPR", "TEST_LANG2", emptyMap(), emptyMap(), emptyList()) }

        assertTrue(ex.message == "CALLED TEST_LANG2")
    }

    @Test
    @DisplayName("Test if proxy is fetched correctly")
    fun testFetchCorrectProxy(){
        val evaluator = mock(AbstractRemoteProxyEvaluator::class.java)
        `when`(evaluator.lang()).then { return@then "TEST_LANG" }

        val service = RemoteProxyService(registry, listOf(evaluator))
        `when`(registry["NAME"]).then { throw IllegalArgumentException("CALLED ME!") }

        val ex = assertThrows(IllegalArgumentException::class.java)
        { service.eval("NAME", "EXPR", "TEST_LANG", emptyMap(), emptyMap(), emptyList()) }

        assertTrue(ex.message == "CALLED ME!")
    }

    @Test
    @DisplayName("Test if proxy is unwarped if instance of interface")
    fun testProxyUnwrap(){
        val proxy = mock(RemoteProxy::class.java)
        val params = mapOf<String, Any>()
        val evaluator = object: AbstractRemoteProxyEvaluator<Any, Any>() {
            override fun eval(exp: String): (shell: Any) -> Any? = { it }
            override fun binding(values: Map<String, Any>): Any = Any()
            override fun shell(binding: Any): Any = binding
            override fun lang(): String = "TEST_LANG"

        }

        `when`(registry[anyString()]).then { proxy }
        `when`(proxy.ref(anyMap())).then { "OK" }

        val service = RemoteProxyService(registry, listOf(evaluator))

        service.eval("NAME", "EXPR", "TEST_LANG", params, emptyMap(), emptyList())

        verify(proxy, times(1)).ref(anyMap())
    }

//    private fun <T> any(type: Class<T>): T = Mockito.any<T>(type)
}