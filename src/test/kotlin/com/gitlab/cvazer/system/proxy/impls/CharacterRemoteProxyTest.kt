package com.gitlab.cvazer.system.proxy.impls

import com.gitlab.cvazer.dao.entity.CharEntity
import com.gitlab.cvazer.dao.repo.CharDao
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.mockito.Mockito.*
import java.util.*

internal class CharacterRemoteProxyTest {

    @Test
    @DisplayName("Test for correct chad - id correlation")
    fun checkCorrectCharFetch(){
        val dao = mock(CharDao::class.java)
        val char = Optional.of(mock(CharEntity::class.java))
        `when`(char.get().id).then { -1L }
        `when`(dao.findById(anyLong())).then { char }
        val proxy = CharacterRemoteProxy(dao)
        assertEquals(-1L, (proxy.ref(mapOf(Pair("charId", "-1"))) as CharEntity).id)
    }
}