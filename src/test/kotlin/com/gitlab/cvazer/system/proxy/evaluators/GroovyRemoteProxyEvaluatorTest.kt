package com.gitlab.cvazer.system.proxy.evaluators

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

internal class GroovyRemoteProxyEvaluatorTest {

    @Test
    @DisplayName("Test GROOVY evaluator sanity")
    fun testFetchCorrectEvaluator(){
        val evaluator = GroovyRemoteProxyEvaluator()
        val proxy = mapOf(Pair("name", "TEST"))

        //language=Groovy
        val res = evaluator.eval(proxy, """target.get("name")""", emptyMap(), emptyList())
        assertEquals("TEST", res)
        assertEquals("GROOVY", evaluator.lang())
    }

}