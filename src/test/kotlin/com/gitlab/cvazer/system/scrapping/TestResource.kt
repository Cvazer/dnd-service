package com.gitlab.cvazer.system.scrapping

import com.gitlab.cvazer.system.scrapping.abstraction.Scrap
import java.util.*

class TestResource(
    override val locator: String,
    val data: String = UUID.nameUUIDFromBytes(locator.toByteArray()).toString(),
    override var rawHash: String? = null,
    override var bake: Boolean = false,
): Scrap<String, String>