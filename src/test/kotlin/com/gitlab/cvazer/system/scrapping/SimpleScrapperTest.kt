package com.gitlab.cvazer.system.scrapping

import com.gitlab.cvazer.dao.entity.system.ScrappingResourceEntity
import com.gitlab.cvazer.dao.entity.dict.ScrappingResourceTypeDictEntity
import com.gitlab.cvazer.dao.repo.ProblemResolutionDao
import com.gitlab.cvazer.dao.repo.ScrappingResourceDao
import com.gitlab.cvazer.system.scrapping.abstraction.ResourceProvider
import com.gitlab.cvazer.system.scrapping.model.ResourceConflictProblemResolution
import com.gitlab.cvazer.system.scrapping.model.ResourceConflictProblemResolution.Resolution
import com.gitlab.cvazer.system.scrapping.model.ScrappingProblem
import com.gitlab.cvazer.system.scrapping.scrappers.SimpleScrapper
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource
import org.mockito.Mockito.*

internal class SimpleScrapperTest {

    @ParameterizedTest
    @EnumSource(value = Resolution::class)
    fun resourceConflictResolutionTest(resolution: Resolution){
        val typeStub = ScrappingResourceTypeDictEntity("SPELL", "Spell")

        val new = ScrappingResourceEntity(locator = "A", data = "Y", type = typeStub, source = "")
        val persisted = ScrappingResourceEntity(locator = "A", data = "X", type = typeStub, id = 1, source = "")
        val consensus = ScrappingResourceEntity(locator = "A", data = "Z", type = typeStub, source = "")

        val providerStub = object: ResourceProvider<ScrappingResourceEntity> {
            override fun init(): Boolean = true
            override fun isReady(): Boolean = true
            override fun isDone(): Boolean = false
            override fun reset() {}
            override fun close(): Boolean = false
            override fun status(): String = ""
            override fun problems(): List<ScrappingProblem> = emptyList()

            override fun getBatch(): List<ScrappingResourceEntity> =
                listOf(new)
        }

        var savedRef: ScrappingResourceEntity? = null
        val daoMock = mock(ScrappingResourceDao::class.java)
        `when`(daoMock.findByLocator(persisted.locator)).thenReturn(persisted)
        doAnswer {
            savedRef = it.arguments[0] as ScrappingResourceEntity
            return@doAnswer savedRef
        }.`when`(daoMock).save(any())

        val resolutionDaoMock = mock(ProblemResolutionDao::class.java);
        `when`(resolutionDaoMock.existsByProblemId(anyString())).thenReturn(false)
        val scrapper = SimpleScrapper(listOf(providerStub), daoMock, resolutionDaoMock)

        var status = scrapper.apply { scrap() }.status()
        assertEquals(1, status.problems.size)
        val problem = status.problems[0]
        assertEquals(ScrappingProblem.Type.RESOURCE_CONFLICT, problem.type)

        val res = ResourceConflictProblemResolution(
            problem.id, problem.type, resolution, new, persisted, consensus
        )
        scrapper.resolve(res)
        when(resolution) {
            Resolution.USE_NEW -> {
                assertEquals(new, savedRef)
                assertEquals(persisted.id, new.id)
            }
            Resolution.USE_CONSENSUS -> { assertEquals(consensus, savedRef) }
            Resolution.DISCARD_NEW -> { assertNull(savedRef) }
            Resolution.KEEP_BOTH -> {
                assertEquals(new, savedRef)
                assertNull(new.id)
            }
        }
        status = scrapper.status()
        assertEquals(0, status.problems.size)
    }

}