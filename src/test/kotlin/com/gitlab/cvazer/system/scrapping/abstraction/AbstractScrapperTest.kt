package com.gitlab.cvazer.system.scrapping.abstraction

import com.gitlab.cvazer.system.scrapping.TestProvider
import com.gitlab.cvazer.system.scrapping.TestResource
import com.gitlab.cvazer.system.scrapping.TestScrapper
import com.gitlab.cvazer.system.scrapping.model.ScrappingProblem
import com.gitlab.cvazer.util.getAs
import com.gitlab.cvazer.util.jsonString
import com.gitlab.cvazer.util.readAs
import org.apache.commons.codec.digest.DigestUtils
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.junit.jupiter.params.provider.ValueSource
import java.util.*

internal class AbstractScrapperTest {

    @Test
    @DisplayName("Sanity check")
    fun checkSanity(){
        val provider = TestProvider {
            listOf(TestResource(UUID.randomUUID().toString()))
        }
        val scrapper = TestScrapper(provider)

        assertEquals(true, scrapper.scrap())
        assertEquals(true, provider.onlyTrue(
            TestProvider.Signal.INIT,
            TestProvider.Signal.READY,
            TestProvider.Signal.DONE,
            TestProvider.Signal.CLOSE,
        ))
        assertEquals(false, scrapper.scrap())
        assertEquals(1, scrapper.data.size)
    }

    @ParameterizedTest
    @DisplayName("Test reset sanity")
    @CsvSource("A|B|C,A|B|C", "A|A|C,A|B|C", "A|A,A|A")
    fun testReset(locators: String, data: String){
        val provider = TestProvider { locators.split("|").mapIndexed { i, l ->
                TestResource(l, data.split("|")[i])
        } }
        val scrapper = TestScrapper(provider)

        assertEquals(true, provider.onlyFalse(*TestProvider.Signal.values()))
        val status1 = scrapper
            .apply { scrap() }
            .apply { scrap() }
            .status()
        assertEquals(true, provider.onlyTrue(*TestProvider.Signal.values()))

        scrapper.reset()

        assertEquals(true, provider.onlyFalse(*TestProvider.Signal.values()))
        val status2 =scrapper
            .apply { scrap() }
            .apply { scrap() }
            .status()
        assertEquals(true, provider.onlyTrue(*TestProvider.Signal.values()))

        assertEquals(
            DigestUtils.md5Hex(status1.jsonString()),
            DigestUtils.md5Hex(status2.jsonString()),
        )
    }

    @ParameterizedTest
    @ValueSource(strings = [
        """{"data":[{"l":"A","d":"A"}, {"l":"B","d":"B"}, {"l":"C","d":"C"}],"conflicts":"0"}""",
        """{"data":[{"l":"A","d":"A"}, {"l":"A","d":"A"}, {"l":"C","d":"C"}],"conflicts":"1"}""",
        """{"data":[{"l":"A","d":"A"}, {"l":"A","d":"B"}, {"l":"C","d":"C"}],"conflicts":"1"}""",
    ])
    fun testProblems(arg: String){
        val argMap = arg.readAs<Map<String, Any>>()
        val expectedConflicts: Int = (argMap["conflicts"]!! as String).toInt()
        val data: List<Map<String, String>> = argMap["data"]!!.getAs()

        val provider = TestProvider { data.map { TestResource(it["l"]!!, it["d"]!!) } }
        val scrapper = TestScrapper(provider)

        val status = scrapper
            .apply { scrap() }
            .apply { scrap() }
            .status()
        assertEquals(expectedConflicts, status.problems.size)
    }

    @ParameterizedTest
    @ValueSource(strings = [
        """{"data":[{"l":"A","d":"A"}, {"l":"B","d":"B"}, {"l":"C","d":"C"}],"e":"0","w":"3"}""",
        """{"data":[{"l":"A","d":"A"}, {"l":"A","d":"A"}, {"l":"C","d":"C"}],"e":"0","w":"3"}""",
        """{"data":[{"l":"A","d":"A"}, {"l":"A","d":"B"}, {"l":"C","d":"C"}],"e":"1","w":"2"}""",
    ])
    fun testProblemDoubleScrap(arg: String){
        val argMap = arg.readAs<Map<String, Any>>()
        val errorsCount: Int = (argMap["e"]!! as String).toInt()
        val warningsCount: Int = (argMap["w"]!! as String).toInt()
        val data: List<Map<String, String>> = argMap["data"]!!.getAs()

        val provider = TestProvider { data.map { TestResource(it["l"]!!, it["d"]!!) } }
        val scrapper = TestScrapper(provider)

        val status = scrapper
            .apply { scrap() }.apply { scrap() }
            .apply { reset() }
            .apply { scrap() }.apply { scrap() }
            .status()
        assertEquals(errorsCount, status.problems.count { it.level == ScrappingProblem.Level.ERROR })
        assertEquals(warningsCount, status.problems.count { it.level == ScrappingProblem.Level.WARNING })
    }

    @Test
    fun testResolveProblem(){
        val provider = TestProvider { listOf(
            TestResource("A", "X"),
            TestResource("A", "Y"),
        ) }
        val scrapper = TestScrapper(provider)

        var status = scrapper.apply { scrap() }.status()
        assertEquals(1, status.problems.size)

        scrapper.problemResolved(status.problems[0].id)
        status = scrapper.status()
        assertEquals(0, status.problems.size)
    }

    @Test
    fun testAlreadyResolvedProblem(){
        val provider = TestProvider { listOf(
            TestResource("A", "X"),
            TestResource("A", "Y"),
        ) }
        val scrapper = TestScrapper(provider)
        var status = scrapper.apply { scrap() }.status()

        assertEquals(1, status.problems
            .filter { it.level == ScrappingProblem.Level.ERROR }
            .size)

        val problem = status.problems[0]
        scrapper.resolved.add(problem.id)
        scrapper.reset()
        status = scrapper.apply { scrap() }.status()
        
        assertEquals(0, status.problems
            .filter { it.level == ScrappingProblem.Level.ERROR }
            .size)
    }
}