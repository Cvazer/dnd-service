package com.gitlab.cvazer.system.scrapping

import com.gitlab.cvazer.dao.repo.ProblemResolutionDao
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock

internal class ScrappingServiceTest {

    @Test
    fun checkSanity(){
        val scrapper = TestScrapper(TestProvider { listOf(TestResource("A")) })
        val service = ScrappingService(listOf(scrapper), emptyList(), listOf(), mock(ProblemResolutionDao::class.java))

        service.startScrapping(scrapper.name())
        while (true){ if (service.isDone(TestScrapper::class.java)) break }
        assertEquals(1, scrapper.data.size)
    }

    @Test
    fun testBreaking() {
        val scrapper = TestScrapper(TestProvider { throw RuntimeException("TEST") })
        val service = ScrappingService(listOf(scrapper), emptyList(), listOf(), mock(ProblemResolutionDao::class.java))
        service.startScrapping(scrapper.name())
        while (true){ if (service.isDone(TestScrapper::class.java)) break }
        assertEquals(0, scrapper.data.size)
    }

}