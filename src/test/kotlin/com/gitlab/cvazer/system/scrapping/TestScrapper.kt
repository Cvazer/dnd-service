package com.gitlab.cvazer.system.scrapping

import com.gitlab.cvazer.system.scrapping.abstraction.AbstractScrapper
import com.gitlab.cvazer.system.scrapping.abstraction.ResourceProvider
import com.gitlab.cvazer.system.scrapping.model.ScrappingProblem
import java.util.*

class TestScrapper(
    provider: ResourceProvider<TestResource>,
    val data: MutableMap<String, TestResource> = mutableMapOf(),
    val resolved: MutableSet<String> = mutableSetOf(),
): AbstractScrapper<TestResource, String, String>(listOf(provider)) {

    override fun problemAlreadyResolved(problem: ScrappingProblem): Boolean =
        resolved.contains(problem.id)

    override fun idSalt(resources: List<TestResource>): String = resources
            .joinToString("|") { it.rawHash ?: calculateHash(it) }
            .let { UUID.nameUUIDFromBytes(it.toByteArray()).toString() }

    fun publicIdSalt(resources: List<TestResource>): String = idSalt(resources)

    override fun name() = NAME
    override fun calculateHash(scrap: TestResource): String = scrap.data
    override fun persisted(locator: String): TestResource? = data[locator]
    override fun persist(resource: TestResource): TestResource = resource
        .also { data[it.locator] = it }

    companion object {
        @JvmStatic val NAME: String = "TestScrapper"
    }

    fun waitDone() {
        while (true) {
            try {
                if (status().status == "Done") break
            } catch (e: Exception) {
                throw e
            }
        }
    }
}