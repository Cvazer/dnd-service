package com.gitlab.cvazer.system.scrapping

import com.gitlab.cvazer.system.scrapping.abstraction.ResourceProvider
import com.gitlab.cvazer.system.scrapping.model.ScrappingProblem

class TestProvider(
    val state: MutableMap<Signal, Boolean> = mutableMapOf(
        Pair(Signal.INIT, false),
        Pair(Signal.READY, false),
        Pair(Signal.DONE, false),
        Pair(Signal.CLOSE, false),
    ),
    val resSupplier: () -> List<TestResource>
): ResourceProvider<TestResource> {

    override fun init(): Boolean {
        state[Signal.INIT] = true
        state[Signal.READY] = true
        return state[Signal.INIT]!!
    }

    override fun isReady(): Boolean {
        return state[Signal.READY]!!
    }

    override fun isDone(): Boolean {
        return state[Signal.DONE]!!
    }

    override fun getBatch(): List<TestResource> {
        state[Signal.DONE] = true
        return resSupplier()
    }

    override fun close(): Boolean {
        state[Signal.CLOSE] = true
        return state[Signal.CLOSE]!!
    }

    override fun status() =
        if (!state[Signal.INIT]!!) "NOT INITED"
        else "DONE"

    override fun reset() {
        state.putAll(TestProvider { emptyList() }.state)
    }
    override fun problems(): List<ScrappingProblem> = emptyList()

    fun onlyTrue(vararg signals: Signal): Boolean {
        val allTrue = state.entries.asSequence()
            .filter { signals.contains(it.key) }
            .all { it.value }
        val othersList = state.entries
            .filter { !signals.contains(it.key) }
        val others = if (othersList.isEmpty()) false
        else othersList.all { it.value }
        return allTrue && !others
    }

    fun onlyFalse(vararg signals: Signal): Boolean {
        val allFalse = state.entries.asSequence()
            .filter { signals.contains(it.key) }
            .all { !it.value }
        val othersList = state.entries
            .filter { !signals.contains(it.key) }
        val others = if (othersList.isEmpty()) true
        else othersList.all { !it.value }
        return allFalse && others
    }

    fun trueExcept(vararg signals: Signal) = onlyTrue(
        *Signal.values()
            .filter { !signals.contains(it) }
            .toTypedArray()
    )

    fun falseExcept(vararg signals: Signal) = onlyFalse(
        *Signal.values()
            .filter { !signals.contains(it) }
            .toTypedArray()
    )

    enum class Signal {
        INIT, READY, DONE, CLOSE
    }
}