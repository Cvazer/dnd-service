package com.gitlab.cvazer.system.itemgen

import com.gitlab.cvazer.dao.entity.ItemEntity
import com.gitlab.cvazer.dao.entity.dict.ItemTypeDictEntity
import com.gitlab.cvazer.dao.entity.info.ItemGenericInfoEntity
import com.gitlab.cvazer.dao.repo.ItemDao
import com.gitlab.cvazer.dao.repo.generic.ItemGenerationIndexDao
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.mockito.Mockito.*

internal class ItemEntitySpecificItemGenerationServiceTest {
    private lateinit var itemDao: ItemDao
    private lateinit var indexDao: ItemGenerationIndexDao
    private lateinit var service: ItemEntitySpecificItemGenerationService

    @BeforeEach
    fun setUp() {
        itemDao = mock(ItemDao::class.java)
        indexDao = mock(ItemGenerationIndexDao::class.java)
        service = ItemEntitySpecificItemGenerationService(itemDao, indexDao)

        `when`<ItemEntity>(itemDao.save(any(ItemEntity::class.java))).then { i ->
            i.getArgument(0, ItemEntity::class.java).copy(id = 0)
        }
    }

    @Test
    @DisplayName("Test generate from generic given multiple base")
    fun testGenerateFromGeneric(){
        val generic = ItemEntity(
                id = 0,
                name = "Test generic",
                type = mock(ItemTypeDictEntity::class.java),
                generic = true
        ).apply {
            this.genericInfo = ItemGenericInfoEntity(
                    _requires = "[\"name == 'Base 1'\"]",
                    namePrefix = "From test generic "
            )
        }

        val bases = listOf(
                ItemEntity(
                        id = 0,
                        name = "Base 1",
                        type = mock(ItemTypeDictEntity::class.java),
                        base = true
                ),
                ItemEntity(
                        id = 0,
                        name = "Base 2",
                        type = mock(ItemTypeDictEntity::class.java),
                        base = true
                )
        )

        `when`(itemDao.findAllBaseItems()).then { bases }
        `when`(indexDao.findBySourceIdAndAndGenericId(anyLong(), anyLong())).then { null }

        val res = service.fromGeneric(generic)
        assertTrue(res.size == 1)
        assertEquals("From test generic Base 1", res.first().name)
    }

    @Test
    @DisplayName("Test generate fro base given multiple generics")
    fun testGenerateFroBase(){
        val base = ItemEntity(
                id = 0,
                name = "Test Base",
                type = mock(ItemTypeDictEntity::class.java),
                base = true
        )

        val generics = listOf(
                ItemEntity(
                        id = 0,
                        name = "Generic 1",
                        type = mock(ItemTypeDictEntity::class.java),
                        generic = true
                ).apply {
                    this.genericInfo = ItemGenericInfoEntity(
                            _requires = "[\"name == 'Test Base'\"]",
                            _inherits = "[\"@target.value = @generic.value\"]",
                            namePrefix = "From Generic 1 ",
                            addedValue = 100
                    )
                },
                ItemEntity(
                        id = 0,
                        name = "Generic 2",
                        type = mock(ItemTypeDictEntity::class.java),
                        generic = true
                ).apply {
                    this.genericInfo = ItemGenericInfoEntity(
                            _requires = "[\"name == 'Test Base 2'\"]",
                            _inherits = "[\"@target.value = @generic.value\"]",
                            namePrefix = "From Generic 1 ",
                            addedValue = 200
                    )
                }
        )

        `when`(itemDao.findAllGenericItems()).then { generics }
        `when`(indexDao.findBySourceIdAndAndGenericId(anyLong(), anyLong())).then { null }

        val res = service.forBase(base)
        assertTrue(res.size == 1)
        assertEquals("From Generic 1 Test Base", res.first().name)
        assertEquals(100, res.first().value)
    }

}